package com.cp.locationsync.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {
	private int errorCode;
	private String message;
	private List<FieldErrorResource> fieldErrors =new ArrayList<FieldErrorResource>();
	
	
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<FieldErrorResource> getFieldErrors() {
		return fieldErrors;
	}
	public void setFieldErrors(List<FieldErrorResource> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}