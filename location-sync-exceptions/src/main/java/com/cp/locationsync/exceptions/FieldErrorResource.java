package com.cp.locationsync.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldErrorResource {
	@JsonIgnore
    private String resource;
    private String field;
    @JsonIgnore
    private String code;
    private String message;

    public String getResource() { return resource; }

    public void setResource(String resource) { this.resource = resource; }

    public String getField() { return field; }

    public void setField(String field) { this.field = field; }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}