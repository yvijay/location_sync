package com.cp.locationsync.exceptions;

public class InvalidAPIKeyException extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;
 
	public String getErrorMessage() {
		return errorMessage;
	}
	public InvalidAPIKeyException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public InvalidAPIKeyException() {
		super();
	}
}
