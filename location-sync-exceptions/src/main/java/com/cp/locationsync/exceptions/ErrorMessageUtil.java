package com.cp.locationsync.exceptions;

import org.springframework.http.HttpStatus;

public class ErrorMessageUtil {
	
	public static ErrorResponse getErrorMessage (HttpStatus httpStatus, Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(httpStatus.value());
		if(httpStatus.value() == 401 ) {
			error.setMessage("UNAUTHORIZED Request - Invalid Client Credentials Or Access Token");
		}
		else if(httpStatus.value() == 403 ) {
			error.setMessage("Access Denied");
		}
		else if(httpStatus.value() == 500 ) {
			error.setMessage("Internal Server Error while processing your request, Please try later");
		}
		else if(httpStatus.value() == 400) {
			error.setMessage("Your request was improperly formatted. You should verify that your request conforms to this specification and re-issue the request in a properly formatted manner");
		}
		else if(httpStatus.value() == 404) {
			error.setMessage("The requested resource does not exist");
		}
		else if(httpStatus.value() == 409) {
			error.setMessage("Resource already exist");
		}
		else if(httpStatus.value() == 503) {
			error.setMessage("System undergoing maintenance or is otherwise temporarily unavailable for API queries.");
		}
		else {
			if(ex != null) {
				error.setMessage(ex.getMessage());
			}
			
		}
		
		return error;
	}

}
