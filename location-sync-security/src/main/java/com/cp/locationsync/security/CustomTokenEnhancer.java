/**
 * 
 */
package com.cp.locationsync.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.web.context.request.RequestContextHolder;

public class CustomTokenEnhancer implements TokenEnhancer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.oauth2.provider.token.TokenEnhancer#enhance(
	 * org.springframework.security.oauth2.common.OAuth2AccessToken,
	 * org.springframework.security.oauth2.provider.OAuth2Authentication)
	 */
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		OAuth2Request client = (OAuth2Request) authentication.getOAuth2Request();
		final Map<String, Object> additionalInfo = new HashMap<String, Object>();
		additionalInfo.put("client_id", client.getClientId());
		additionalInfo.put("scope", client.getScope());
		additionalInfo.put("session_id", RequestContextHolder.currentRequestAttributes().getSessionId());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

		return accessToken;
	}

}
