package com.cp.persistence.dto;

import java.io.Serializable;

public class PropertyAddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String address1;

	private String address2;

	private String state;

	private String city;

	private String zip;

	private String country;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	@Override
	public String toString() {
		return "PropertyAddressDTO [id=" + id + ", address1=" + address1
				+ ", address2=" + address2 + ", state=" + state + ", city="
				+ city + ", zip=" + zip + ", country=" + country + "]";
	}
}
