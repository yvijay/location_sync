package com.cp.persistence.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.joda.time.LocalDateTime;

import com.cp.persistence.dto.enums.StatusEnum;

public class RevenueAndSourceDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;

	private LocationDTO locationDTO;

	private StatusEnum status;

	private LocalDateTime revenueDate;

	private BigDecimal amount;

	private Integer numberOfDeposits;

	private UserDTO submittedBy;
	
	private LocalDateTime submittedDate;

	private Set<RevenueNotesDTO> notesDTO = new HashSet<>(0);
	
	private Set<SourceDTO> sources = new HashSet<>(0);
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocationDTO getLocationDTO() {
		return locationDTO;
	}

	public void setLocationDTO(LocationDTO locationDTO) {
		this.locationDTO = locationDTO;
	}





	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public LocalDateTime getRevenueDate() {
		return revenueDate;
	}

	public void setRevenueDate(LocalDateTime revenueDate) {
		this.revenueDate = revenueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getNumberOfDeposits() {
		return numberOfDeposits;
	}

	public void setNumberOfDeposits(Integer numberOfDeposits) {
		this.numberOfDeposits = numberOfDeposits;
	}



	public UserDTO getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(UserDTO submittedBy) {
		this.submittedBy = submittedBy;
	}

	public LocalDateTime getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(LocalDateTime submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Set<RevenueNotesDTO> getNotesDTO() {
		return notesDTO;
	}

	public void setNotesDTO(Set<RevenueNotesDTO> notesDTO) {
		this.notesDTO = notesDTO;
	}

	public Set<SourceDTO> getSources() {
		return sources;
	}

	public void setSources(Set<SourceDTO> sources) {
		this.sources = sources;
	}

	@Override
	public String toString() {
		return "RevenueAndSourceDTO [id=" + id + ", locationDTO=" + locationDTO + ", status=" + status
				+ ", revenueDate=" + revenueDate + ", amount=" + amount + ", numberOfDeposits=" + numberOfDeposits
				+ ", submittedBy=" + submittedBy + ", submittedDate=" + submittedDate + ", notesDTO=" + notesDTO
				+ ", sources=" + sources + "]";
	}




	
	
	
	
	
	
	
	
	
}
