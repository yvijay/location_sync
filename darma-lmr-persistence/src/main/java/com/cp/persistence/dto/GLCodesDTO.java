package com.cp.persistence.dto;

import java.io.Serializable;

import com.cp.persistence.dto.enums.StatusEnum;

public class GLCodesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private String code;

	private String description;

	private String type;

	private BrandDTO brandDTO;
	private StatusEnum status;

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BrandDTO getBrandDTO() {
		return brandDTO;
	}

	public void setBrandDTO(BrandDTO brandDTO) {
		this.brandDTO = brandDTO;
	}

	@Override
	public String toString() {
		return "GLCodesDTO [id=" + id + ", code=" + code + ", description="
				+ description + ", type=" + type + ", brandDTO=" + brandDTO
				+ "]";
	}

}
