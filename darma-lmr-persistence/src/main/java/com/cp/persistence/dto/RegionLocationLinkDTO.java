package com.cp.persistence.dto;

import java.io.Serializable;

import com.cp.persistence.dto.enums.StatusEnum;

public class RegionLocationLinkDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private RegionDTO regionDTO;
	private LocationDTO locationDTO;
	
	private StatusEnum status;

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RegionDTO getRegionDTO() {
		return regionDTO;
	}

	public void setRegionDTO(RegionDTO regionDTO) {
		this.regionDTO = regionDTO;
	}

	public LocationDTO getLocationDTO() {
		return locationDTO;
	}

	public void setLocationDTO(LocationDTO locationDTO) {
		this.locationDTO = locationDTO;
	}

}
