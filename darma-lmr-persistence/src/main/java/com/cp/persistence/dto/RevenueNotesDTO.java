package com.cp.persistence.dto;

import java.io.Serializable;
import java.util.Objects;

import com.cp.persistence.dto.enums.StatusEnum;


public class RevenueNotesDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String noteHeading;

	private String notes;

	private SourceDTO sourceDTO;
	
	private StatusEnum status;

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNoteHeading() {
		return noteHeading;
	}

	public void setNoteHeading(String noteHeading) {
		this.noteHeading = noteHeading;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public SourceDTO getSourceDTO() {
		return sourceDTO;
	}

	public void setSourceDTO(SourceDTO sourceDTO) {
		this.sourceDTO = sourceDTO;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RevenueNotesDTO notes = (RevenueNotesDTO) o;
		if (notes.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, notes.id);
	}

	@Override
	public String toString() {
		return "RevenueNotesDTO [id=" + id + ", noteHeading=" + noteHeading
				+ ", notes=" + notes + ", sourceDTO=" + sourceDTO + "]";
	}

}
