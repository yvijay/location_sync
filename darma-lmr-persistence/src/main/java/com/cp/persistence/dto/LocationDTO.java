package com.cp.persistence.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.cp.persistence.dto.enums.StatusEnum;

public class LocationDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;

	private String locationID;

	private String name;

	private String clientLegalName;

	private String address1;

	private String address2;

	private String city;

	private String county;

	private String state;

	private String zipCode;

	private StatusEnum status;

	private String phone;

	private String cell;

	private String fax;

	private String ext;

	private String companyName;

	private String executive;

	private String serviceType;

	private String contractType;
	
	private Boolean applicableForDarma;

	private Integer noofSpaces;

	private String subsidyType;
	private RegionDTO regionDTO;
	private String parentLocationID;
	private String parentLocationName;
	private Set<LocationNotesDTO> notesDTO = new HashSet<LocationNotesDTO>();
	private Set<TenderTypesLocationLinkDTO> tenderTypesLnkDTO = new HashSet<TenderTypesLocationLinkDTO>();
	private Set<RevenueTypesLocationLinkDTO> revenueTypesLnkDTO = new HashSet<RevenueTypesLocationLinkDTO>();
	private Set<BankAccountsLocationLinkDTO> bankAccountsLocationLinkDTO = new HashSet<>();
	private Set<BurdensDTO> burdensDTO = new HashSet<BurdensDTO>();
	private Set<ExclusionBurdensDTO> exclusionBurdensDTO = new HashSet<ExclusionBurdensDTO>();
	private Set<PositionsAndBillingRatesDTO> rolesAndBillingRatesDTO = new HashSet<PositionsAndBillingRatesDTO>();
	private Set<LocationContactDTO> locationContactDTO = new HashSet<LocationContactDTO>();
	private BurdenInfoDTO burdenInfoDTO;
	private LocationMoreInfoDTO locationMoreInfoDTO;
	private UserDTO darmaUser;
	private String brandCode;
	private BrandDTO brandDTO;

	
	
	public Boolean getApplicableForDarma() {
		return applicableForDarma;
	}

	public void setApplicableForDarma(Boolean applicableForDarma) {
		this.applicableForDarma = applicableForDarma;
	}

	public BrandDTO getBrandDTO() {
		return brandDTO;
	}

	public void setBrandDTO(BrandDTO brandDTO) {
		this.brandDTO = brandDTO;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public UserDTO getDarmaUser() {
		return darmaUser;
	}

	public void setDarmaUser(UserDTO darmaUser) {
		this.darmaUser = darmaUser;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocationID() {
		return locationID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<RevenueTypesLocationLinkDTO> getRevenueTypesLnkDTO() {
		return revenueTypesLnkDTO;
	}

	public void setRevenueTypesLnkDTO(
			Set<RevenueTypesLocationLinkDTO> revenueTypesLnkDTO) {
		this.revenueTypesLnkDTO = revenueTypesLnkDTO;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getSubsidyType() {
		return subsidyType;
	}

	public void setSubsidyType(String subsidyType) {
		this.subsidyType = subsidyType;
	}

	public RegionDTO getRegionDTO() {
		return regionDTO;
	}

	public void setRegionDTO(RegionDTO regionDTO) {
		this.regionDTO = regionDTO;
	}

	public String getParentLocationID() {
		return parentLocationID;
	}

	public void setParentLocationID(String parentLocationID) {
		this.parentLocationID = parentLocationID;
	}

	public String getParentLocationName() {
		return parentLocationName;
	}

	public void setParentLocationName(String parentLocationName) {
		this.parentLocationName = parentLocationName;
	}

	public Set<LocationNotesDTO> getNotesDTO() {
		return notesDTO;
	}

	public void setNotesDTO(Set<LocationNotesDTO> notesDTO) {
		this.notesDTO = notesDTO;
	}

	public Set<TenderTypesLocationLinkDTO> getTenderTypesLnkDTO() {
		return tenderTypesLnkDTO;
	}

	public void setTenderTypesLnkDTO(
			Set<TenderTypesLocationLinkDTO> tenderTypesLnkDTO) {
		this.tenderTypesLnkDTO = tenderTypesLnkDTO;
	}

	public Set<BankAccountsLocationLinkDTO> getBankAccountsLocationLinkDTO() {
		return bankAccountsLocationLinkDTO;
	}

	public void setBankAccountsLocationLinkDTO(
			Set<BankAccountsLocationLinkDTO> bankAccountsLocationLinkDTO) {
		this.bankAccountsLocationLinkDTO = bankAccountsLocationLinkDTO;
	}

	public Set<BurdensDTO> getBurdensDTO() {
		return burdensDTO;
	}

	public void setBurdensDTO(Set<BurdensDTO> set) {
		this.burdensDTO = set;
	}

	public Set<PositionsAndBillingRatesDTO> getRolesAndBillingRatesDTO() {
		return rolesAndBillingRatesDTO;
	}

	public void setRolesAndBillingRatesDTO(
			Set<PositionsAndBillingRatesDTO> rolesAndBillingRatesDTO) {
		this.rolesAndBillingRatesDTO = rolesAndBillingRatesDTO;
	}

	public Set<LocationContactDTO> getLocationContactDTO() {
		return locationContactDTO;
	}

	public void setLocationContactDTO(Set<LocationContactDTO> locationContactDTO) {
		this.locationContactDTO = locationContactDTO;
	}

	public LocationMoreInfoDTO getLocationMoreInfoDTO() {
		return locationMoreInfoDTO;
	}

	public void setLocationMoreInfoDTO(LocationMoreInfoDTO locationMoreInfoDTO) {
		this.locationMoreInfoDTO = locationMoreInfoDTO;
	}

	public BurdenInfoDTO getBurdenInfoDTO() {
		return burdenInfoDTO;
	}

	public void setBurdenInfoDTO(BurdenInfoDTO burdenInfoDTO) {
		this.burdenInfoDTO = burdenInfoDTO;
	}

	public String getClientLegalName() {
		return clientLegalName;
	}

	public void setClientLegalName(String clientLegalName) {
		this.clientLegalName = clientLegalName;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getExecutive() {
		return executive;
	}

	public void setExecutive(String executive) {
		this.executive = executive;
	}

	public Integer getNoofSpaces() {
		return noofSpaces;
	}

	public void setNoofSpaces(Integer noofSpaces) {
		this.noofSpaces = noofSpaces;
	}

	public Set<ExclusionBurdensDTO> getExclusionBurdensDTO() {
		return exclusionBurdensDTO;
	}

	public void setExclusionBurdensDTO(
			Set<ExclusionBurdensDTO> exclusionBurdensDTO) {
		this.exclusionBurdensDTO = exclusionBurdensDTO;
	}

	@Override
	public String toString() {
		return "LocationDTO [id=" + id + ", locationID=" + locationID
				+ ", name=" + name + ", clientLegalName=" + clientLegalName
				+ ", address1=" + address1 + ", address2=" + address2
				+ ", city=" + city + ", county=" + county + ", state=" + state
				+ ", zipCode=" + zipCode + ", status=" + status + ", phone="
				+ phone + ", cell=" + cell + ", fax=" + fax + ", ext=" + ext
				+ ", companyName=" + companyName + ", executive=" + executive
				+ ", serviceType=" + serviceType + ", contractType="
				+ contractType + ", applicableForDarma=" + applicableForDarma
				+ ", noofSpaces=" + noofSpaces + ", subsidyType=" + subsidyType
				+ ", regionDTO=" + regionDTO + ", parentLocationID="
				+ parentLocationID + ", parentLocationName="
				+ parentLocationName + ", notesDTO=" + notesDTO
				+ ", tenderTypesLnkDTO=" + tenderTypesLnkDTO
				+ ", revenueTypesLnkDTO=" + revenueTypesLnkDTO
				+ ", bankAccountsLocationLinkDTO="
				+ bankAccountsLocationLinkDTO + ", burdensDTO=" + burdensDTO
				+ ", exclusionBurdensDTO=" + exclusionBurdensDTO
				+ ", rolesAndBillingRatesDTO=" + rolesAndBillingRatesDTO
				+ ", locationContactDTO=" + locationContactDTO
				+ ", burdenInfoDTO=" + burdenInfoDTO + ", locationMoreInfoDTO="
				+ locationMoreInfoDTO + ", darmaUser=" + darmaUser
				+ ", brandCode=" + brandCode + ", brandDTO=" + brandDTO + "]";
	}

	

}
