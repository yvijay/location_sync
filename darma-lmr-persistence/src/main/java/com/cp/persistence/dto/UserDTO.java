package com.cp.persistence.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.cp.persistence.dto.enums.StatusEnum;
import com.cp.persistence.dto.enums.UserType;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String firstName;

	private String lastName;

	private String emailAddress;

	private String userName;

	private String phone;

	private StatusEnum status;

	private UserType usertype;

	private Set<RoleDTO> rolesDTO = new HashSet<>();

	private Set<UserLocationLinkDTO> usersLocationLinkDTO = new HashSet<>();
	private BrandDTO brandDTO;
	private String brandCode;
	
	private String displayName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public Set<RoleDTO> getRolesDTO() {
		return rolesDTO;
	}

	public void setRolesDTO(Set<RoleDTO> rolesDTO) {
		this.rolesDTO = rolesDTO;
	}

	public BrandDTO getBrandDTO() {
		return brandDTO;
	}

	public void setBrandDTO(BrandDTO brandDTO) {
		this.brandDTO = brandDTO;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserType getUsertype() {
		return usertype;
	}

	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}

	public Set<UserLocationLinkDTO> getUsersLocationLinkDTO() {
		return usersLocationLinkDTO;
	}

	public void setUsersLocationLinkDTO(
			Set<UserLocationLinkDTO> usersLocationLinkDTO) {
		this.usersLocationLinkDTO = usersLocationLinkDTO;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}



}
