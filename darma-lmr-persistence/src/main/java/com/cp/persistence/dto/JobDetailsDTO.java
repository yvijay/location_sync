package com.cp.persistence.dto;

import java.io.Serializable;

public class JobDetailsDTO implements Serializable {

	private static final long serialVersionUID = 1124869316564110072L;

	private String id;

	private String jobName;
	
	private String jobCode;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getJobName() {
		return jobName;
	}


	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	public String getJobCode() {
		return jobCode;
	}


	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}


	@Override
	public String toString() {
		return "JobDetailsDTO [id=" + id + ", jobName=" + jobName + ", jobCode=" + jobCode + "]";
	}

}
