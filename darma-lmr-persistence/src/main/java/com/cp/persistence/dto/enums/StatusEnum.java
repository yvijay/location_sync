package com.cp.persistence.dto.enums;

public enum StatusEnum {
	I("Inactive"), A("Active"), X("Deleted"), S("Submitted"), C("Created"), E("Entered"), O("Open"), CL("Closed");
	
	private final String value;

	StatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
