package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Positions;
@Repository
public interface PositionsRepository extends JpaRepository<Positions, String> , JpaSpecificationExecutor<Positions>{

}
