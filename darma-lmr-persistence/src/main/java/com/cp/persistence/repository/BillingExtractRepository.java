package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.BillingExtract;

@Repository
public interface BillingExtractRepository extends JpaRepository<BillingExtract, String>{

	@Query("SELECT billingExtract FROM BillingExtract billingExtract "
			+ "INNER JOIN billingExtract.billingExtractFile file "
			+ "WHERE file.id=:billingExtractFileId AND file.isProcessed=:isProcessed")
	List<BillingExtract> getBillingExtractOfFile(@Param(value = "billingExtractFileId") String billingExtractFileId, @Param(value = "isProcessed") char isProcessed);
	
}
