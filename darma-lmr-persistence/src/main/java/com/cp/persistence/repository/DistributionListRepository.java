package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cp.persistence.entity.DistributionList;

public interface DistributionListRepository extends JpaRepository<DistributionList, String> {

	DistributionList findByCode(String code);

	}


