package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cp.persistence.dto.enums.StatusEnum;
import com.cp.persistence.dto.enums.UserType;
import com.cp.persistence.entity.User;
@Repository
public interface UserRepository extends JpaRepository<User, String> , JpaSpecificationExecutor<User>{

	User findByEmailAddress(String login);
	
	User findByUserName(String login);
	
	Page<User> findAllByUsertype(UserType usertype, Pageable generatePageRequest);
	
	//get all users based on brandId and usertype 
	Page<User> findAll(Specification<User> specs, Pageable generatePageRequest);

	User findByUserNameAndUsertype(String substring, UserType valueOf);

	public String GET_USER = "select u from User u " 
			+ "inner join u.brand b "
			+ "where u.userName=:userName and u.usertype=:userType and b.code=:brandCode";
	@Query(GET_USER)
	User findByUserNameAndUsertypeAndBrandCode(@Param(value = "userName") String userName,
			@Param(value = "userType")UserType userType, @Param(value = "brandCode") String brandCode);

	User findByUserNameAndUsertypeAndBrandCodeAndStatus(String userName,
			UserType usertype, String code, StatusEnum a);

	


}
