package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Role;
@Repository
public interface RolesRepository extends JpaRepository<Role, String> , JpaSpecificationExecutor<Role>{

	Role findByCode(String string);

	List<Role> findAll(Specification<Role> buildFilterSpecification);
	
	

}
