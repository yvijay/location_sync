package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.BankAccounts;
@Repository
public interface BankAccountsRepository extends JpaRepository<BankAccounts, String> , JpaSpecificationExecutor<BankAccounts>{
	
	Page<BankAccounts> findAll(Specification<BankAccounts> specs, Pageable generatePageRequest);

	BankAccounts findByBankNameAndAccountNumber(String bankName,
			String accountNumber);


}
