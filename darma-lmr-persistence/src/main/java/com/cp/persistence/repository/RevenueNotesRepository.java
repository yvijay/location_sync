package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.RevenueNotes;

@Repository
public interface RevenueNotesRepository
		extends JpaRepository<RevenueNotes, String>, JpaSpecificationExecutor<RevenueNotes> {

}
