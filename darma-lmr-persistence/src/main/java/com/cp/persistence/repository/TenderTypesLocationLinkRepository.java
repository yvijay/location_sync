package com.cp.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.TenderTypesLocationLink;

@Repository
public interface TenderTypesLocationLinkRepository
		extends JpaRepository<TenderTypesLocationLink, String>, JpaSpecificationExecutor<TenderTypesLocationLink> {

	Page<TenderTypesLocationLink> findByLocationId(String location_id, Pageable generatePageRequest);

	Set<TenderTypesLocationLink> findByLocationId(String id);

}
