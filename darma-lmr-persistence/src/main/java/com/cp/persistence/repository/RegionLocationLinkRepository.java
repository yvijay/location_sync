package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.RegionLocationLink;

@Repository
public interface RegionLocationLinkRepository extends
		JpaRepository<RegionLocationLink, String>,
		JpaSpecificationExecutor<RegionLocationLink> {

	Page<RegionLocationLink> findByLocationId(String location_id,
			Pageable generatePageRequest);

}
