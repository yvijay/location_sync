package com.cp.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Burdens;

@Repository
public interface BurdensRepository extends JpaRepository<Burdens, String>, JpaSpecificationExecutor<Burdens> {
	
	Page<Burdens> findByLocationId(String locationId, Pageable generatePageRequest);

	Set<Burdens> findByLocationId(String id);
}
