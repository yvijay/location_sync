package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, String>,
		JpaSpecificationExecutor<Location> {


	

	/**
	 * Query to get all locations associated to logged in user and also belonging to same brand.
	 * @param specs 
	 * @param generatePageRequest
	 * @return
	 */
	Page<Location> findAll(Specification<Location> specs, Pageable generatePageRequest);
	
	//get all locations based on contractType, subsidyType and brand_code
	public String GET_ALL_LOCATIONS = "select l from Location l " 
			+ "inner join l.brand b "
			+ "where l.contractType='Subsidy' and l.subsidyType='Bi-weekly' and b.code='PK1'";

	@Query(GET_ALL_LOCATIONS)
	List<Location> findAllLocationsForBillingConv();
}
