package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.BillingExtractFile;

@Repository
public interface BillingExtractFileRepository extends JpaRepository<BillingExtractFile, String> {

	List<BillingExtractFile> findByIsProcessed(char isProcessed);

	List<BillingExtractFile> findByIsProcessedAndOutboundFileNameIsNull(char isProcessed);

	@Modifying(clearAutomatically = true)
	@Query("UPDATE BillingExtractFile blf SET blf.isProcessed=:newState WHERE blf.id=:billingExtractFileId")
	int updateBillingExtractFileState(@Param("billingExtractFileId") String billingExtractFileId, @Param("newState") char newState);

	@Modifying(clearAutomatically = true)
	@Query("UPDATE BillingExtractFile blf SET blf.outboundFileName=:exportFileName WHERE blf.id=:billingExtractFileId")
	int updateBillingExtractExportFile(@Param("billingExtractFileId") String billingExtractFileId,
			@Param("exportFileName") String exportFileName);

}
