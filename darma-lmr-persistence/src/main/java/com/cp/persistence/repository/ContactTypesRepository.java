package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.ContactTypes;
@Repository
public interface ContactTypesRepository extends JpaRepository<ContactTypes, String> , JpaSpecificationExecutor<ContactTypes>{

	ContactTypes findByCode(String code);

}
