package com.cp.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.BankAccountsLocationLink;

@Repository
public interface BankAccountsLocationLinkRepository
		extends JpaRepository<BankAccountsLocationLink, String>, JpaSpecificationExecutor<BankAccountsLocationLink> {

	Page<BankAccountsLocationLink> findByLocationId(String location_id, Pageable generatePageRequest);

	Set<BankAccountsLocationLink> findByLocationId(String id);

}
