package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.SourceNotes;

@Repository
public interface SourceNotesRepository
		extends JpaRepository<SourceNotes, String>, JpaSpecificationExecutor<SourceNotes> {

}
