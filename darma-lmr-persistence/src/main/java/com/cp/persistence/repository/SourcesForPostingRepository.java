package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cp.persistence.entity.vwSourcesForPosting;

public interface SourcesForPostingRepository extends
		JpaRepository<vwSourcesForPosting, String> , JpaSpecificationExecutor<vwSourcesForPosting>{
	
	
}