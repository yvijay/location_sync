package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.MerchantLocationLink;

@Repository
public interface MerchantLocationLinkRepository
		extends JpaRepository<MerchantLocationLink, String> {

}
