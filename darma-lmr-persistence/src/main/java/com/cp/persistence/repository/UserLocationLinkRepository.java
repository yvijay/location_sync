package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cp.persistence.entity.UserLocationLink;

public interface UserLocationLinkRepository extends
		JpaRepository<UserLocationLink, String>,
		JpaSpecificationExecutor<UserLocationLink> {

	Page<UserLocationLink> findByLocationId(String locationId,
			Pageable generatePageRequest);

	UserLocationLink findOneByUserAndLocation(String userId, String locationId);

	Page<UserLocationLink> findByUserId(String user_id,
			Pageable generatePageRequest);

}
