package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Brand;
@Repository
public interface BrandRepository extends JpaRepository<Brand, String> , JpaSpecificationExecutor<Brand>{

	Brand findByCodeIgnoreCase(String lanier);

}
