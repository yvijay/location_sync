package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Region;
@Repository
public interface RegionRepository extends JpaRepository<Region, String> , JpaSpecificationExecutor<Region>{

}
