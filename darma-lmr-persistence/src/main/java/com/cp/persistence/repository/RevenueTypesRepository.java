package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.RevenueTypes;
@Repository
public interface RevenueTypesRepository extends JpaRepository<RevenueTypes, String> , JpaSpecificationExecutor<RevenueTypes>{

	RevenueTypes findByCode(String code);

}
