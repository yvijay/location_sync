package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cp.persistence.entity.vwAllocations;

public interface vwAllocationsRepository extends JpaRepository<vwAllocations, String>, JpaSpecificationExecutor<vwAllocations> {


}