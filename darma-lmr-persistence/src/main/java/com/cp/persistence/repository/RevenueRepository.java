package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.Revenue;

@Repository
public interface RevenueRepository extends JpaRepository<Revenue, String>, JpaSpecificationExecutor<Revenue> {

	Page<Revenue> findAll(Specification<Revenue> specs, Pageable generatePageRequest);

	Revenue findOneByIdAndLocation(String revenueId, String locationId);

	Revenue findByDepositIdentifier(String depIdentifier);

}
