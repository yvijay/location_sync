package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.TenderTypes;
@Repository
public interface TenderTypesRepository extends JpaRepository<TenderTypes, String> , JpaSpecificationExecutor<TenderTypes>{

	TenderTypes findByCode(String code);

}
