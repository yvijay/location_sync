package com.cp.persistence.repository;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cp.persistence.dto.enums.StatusEnum;
import com.cp.persistence.entity.Source;

@Repository
public interface SourceRepository extends JpaRepository<Source, String>, JpaSpecificationExecutor<Source> {

	@Query("select source from Source source " + "where source.revenue.id=:id "
			+ "and(source.status=:submittedStatus or source.status=:enteredStatus)")
	public Page<Source> findByRevenueIdAndStatus(@Param(value = "id") String id,
			@Param(value = "submittedStatus") StatusEnum submittedStatus,
			@Param(value = "enteredStatus") StatusEnum enteredStatus, Pageable generatePageRequest);
	


	@Query("select source from Source source where source.correctiveYn=:correctiveYn "
			+ "AND source.submittedDate between :submittedDatefrom AND :submittedDateTo ")
	public Page<Source> findBySubmittedDateAndPostingDate(
			@Param(value = "submittedDatefrom") LocalDateTime submittedDatefrom,
			@Param(value = "submittedDateTo") LocalDateTime submittedDateTo,
			@Param(value = "correctiveYn") char correctiveYn, Pageable generatePageRequest);

	@Query("select source from Source source " 
			+ "where source.status=:status")
	public Page<Source> findAllByStatus(@Param(value = "status") StatusEnum status,
			Pageable generatePageRequest);
	
	@Query("select source from Source source "
			+ "where source.transactionDate=:transactionDate and "
			//+ "source.postingDate is null and "
			+ "source.location.id=:location and "
			+ "source.tenderTypes.id=:tenderType")
	List<Source> getSourceForFirstData(@Param(value = "location") String location,
			@Param(value = "transactionDate") LocalDateTime transactionDate,@Param(value = "tenderType") String tenderType);

}
