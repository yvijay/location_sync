package com.cp.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.ExclusionBurdens;

@Repository
public interface ExclusionBurdensRepository extends
		JpaRepository<ExclusionBurdens, String>,
		JpaSpecificationExecutor<ExclusionBurdens> {

	Page<ExclusionBurdens> findByLocationId(String locationId,
			Pageable generatePageRequest);

	Set<ExclusionBurdens> findByLocationId(String id);

}
