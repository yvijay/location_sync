package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.JobDetails;

@Repository
public interface JobDetailsRepository extends JpaRepository<JobDetails, String> {

}
