package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.LocationContact;
@Repository
public interface LocationContactRepository extends JpaRepository<LocationContact, String> , JpaSpecificationExecutor<LocationContact>{

	Page<LocationContact> findByLocationId(String locationId, Pageable generatePageRequest);

	Page<LocationContact> findByFirstName(String contactName, Pageable generatePageRequest);

}
