package com.cp.persistence.repository;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cp.persistence.entity.RevenueTypesLocationLink;

public interface RevenueTypesLocationLinkRepository  extends JpaRepository<RevenueTypesLocationLink, String> , JpaSpecificationExecutor<RevenueTypesLocationLink>{

	Page<RevenueTypesLocationLink> findByLocationId(String locationId, Pageable generatePageRequest);

	Set<RevenueTypesLocationLink> findByLocationId(String id);

}
