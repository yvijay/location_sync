package com.cp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.FinancialYear;

@Repository
public interface FinancialYearRepository extends
		JpaRepository<FinancialYear, String>,
		JpaSpecificationExecutor<FinancialYear> {

}
