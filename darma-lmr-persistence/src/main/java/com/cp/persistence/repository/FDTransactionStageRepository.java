package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.FDTransactionStage;

@Repository
public interface FDTransactionStageRepository extends JpaRepository<FDTransactionStage, String> {

	@Query("select xLocation,transactionDate,xCardType,sum(processedTransactionAmount) from FDTransactionStage group by xLocation,transactionDate,xCardType order by xLocation,transactionDate,xCardType")
	List<Object[]> aggregateFDTransactionStage();
	
	

}
