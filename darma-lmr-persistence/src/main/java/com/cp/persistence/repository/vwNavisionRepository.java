package com.cp.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cp.persistence.entity.vwNavision;

public interface vwNavisionRepository extends JpaRepository<vwNavision, String>, JpaSpecificationExecutor<vwNavision> {

	public List<vwNavision> findAll(Specification<vwNavision> spec);

}