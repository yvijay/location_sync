package com.cp.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cp.persistence.entity.LocationNotes;
@Repository
public interface LocationNotesRepository extends JpaRepository<LocationNotes, String> , JpaSpecificationExecutor<LocationNotes>{

	Page<LocationNotes> findByLocationId(String locationId, Pageable generatePageRequest);

}
