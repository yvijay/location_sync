package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

/**
 * A BILLING_EXTRACT_FILE.
 */
@Entity
@Table(name = "BILLING_EXTRACT_FILE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BillingExtractFile extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "pay_period")
	private String payPeriod;
	
	@Column(name = "pay_group")
	private String payGroup;
	
	@Column(name = "brand")
	private String brand;

	@Column(name = "file_timestamp")
	private Date fileTimestamp;
	
	@Column(name = "inbound_file_name")
	private String inboundFileName;
	
	@Column(name = "outbound_file_name")
	private String outboundFileName;
	
	@Column(name = "is_processed")
	private char isProcessed;

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, cascade = CascadeType.ALL)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@JoinColumn(name="billing_extract_file_id")
	private List<BillingExtract> billingExtracts = new ArrayList<BillingExtract>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(String payPeriod) {
		this.payPeriod = payPeriod;
	}

	public String getPayGroup() {
		return payGroup;
	}

	public void setPayGroup(String payGroup) {
		this.payGroup = payGroup;
	}

	public String getInboundFileName() {
		return inboundFileName;
	}

	public void setInboundFileName(String inboundFileName) {
		this.inboundFileName = inboundFileName;
	}

	public String getOutboundFileName() {
		return outboundFileName;
	}

	public void setOutboundFileName(String outboundFileName) {
		this.outboundFileName = outboundFileName;
	}

	public char getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(char isProcessed) {
		this.isProcessed = isProcessed;
	}

	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Date getFileTimestamp() {
		return fileTimestamp;
	}

	public void setFileTimestamp(Date fileTimestamp) {
		this.fileTimestamp = fileTimestamp;
	}

	public List<BillingExtract> getBillingExtracts() {
		return billingExtracts;
	}

	public void setBillingExtracts(List<BillingExtract> billingExtracts) {
		this.billingExtracts = billingExtracts;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BillingExtractFile billingExtractFile = (BillingExtractFile) o;

		if (!Objects.equals(id, billingExtractFile.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "BillingExtractFile [id=" + id + ", payPeriod=" + payPeriod + ", payGroup=" + payGroup + ", brand=" + brand
				+ ", fileTimestamp=" + fileTimestamp + ", inboundFileName=" + inboundFileName + ", outboundFileName="
				+ outboundFileName + ", isProcessed=" + isProcessed + "]";
	}

}
