package com.cp.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@NamedStoredProcedureQuery(
		name = "updateNavisionPCI", 
		procedureName = "uspNavisionPCI_MarkExported", 
		resultClasses = NavisionPCI.class, 
		parameters = {
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = Boolean.class)
		}
	)
public class NavisionPCIUpdate {

	@Id
	private String id;
}
