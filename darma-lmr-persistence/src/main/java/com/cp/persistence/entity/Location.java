package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "LOCATION")
@SQLDelete(sql = "UPDATE LOCATION SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Location extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "location_id")
	private String locationID;

	@Column(name = "name")
	private String name;

	@Column(name = "client_legal_name")
	private String clientLegalName;

	@Column(name = "address_1")
	private String address1;

	@Column(name = "address_2")
	private String address2;

	@Column(name = "city")
	private String city;

	@Column(name = "county")
	private String county;

	@Column(name = "state")
	private String state;

	@Column(name = "zipCode")
	private String zipCode;

	@Column(name = "phone")
	private String phone;

	@Column(name = "cell")
	private String cell;

	@Column(name = "fax")
	private String fax;

	@Column(name = "ext")
	private String ext;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "executive")
	private String executive;

	@Column(name = "service_type")
	private String serviceType;

	@Column(name = "contract_type")
	private String contractType;

	@Column(name = "noof_spaces")
	private Integer noofSpaces;

	@Column(name = "subsidy_type")
	private String subsidyType;

	@Column(name = "isapplicable_for_darma")
	private Boolean applicableForDarma;

	@Column(name = "parent_location_id")
	private String parentLocationID;

	@Column(name = "parent_location_name")
	private String parentLocationName;

	@Column(name = "brand_code")
	private String brandCode;

	@ManyToOne
	private Brand brand;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<LocationNotes> notes = new HashSet<LocationNotes>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<TenderTypesLocationLink> tenderTypesLnk = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<UserLocationLink> usersLocationLink = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Burdens> burdens = new HashSet<Burdens>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<ExclusionBurdens> exclusionBurdens = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<PositionsAndBillingRates> rolesAndBillingRates = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<LocationContact> locationContact = new HashSet<>();

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "location_more_info_id")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private LocationMoreInfo locationMoreInfo;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "burdenInfo_id")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private BurdenInfo burdenInfo;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<RevenueTypesLocationLink> revenueTypesLocationLink = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<BankAccountsLocationLink> bankAccountsLocationLink = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<RegionLocationLink> regionLocationLinks = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Revenue> revenues = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id", updatable = false, insertable = false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Source> sources = new HashSet<>();

	public Boolean getApplicableForDarma() {
		return applicableForDarma;
	}

	public void setApplicableForDarma(Boolean applicableForDarma) {
		this.applicableForDarma = applicableForDarma;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocationID() {
		return locationID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getSubsidyType() {
		return subsidyType;
	}

	public void setSubsidyType(String subsidyType) {
		this.subsidyType = subsidyType;
	}

	public String getParentLocationID() {
		return parentLocationID;
	}

	public void setParentLocationID(String parentLocationID) {
		this.parentLocationID = parentLocationID;
	}

	public String getParentLocationName() {
		return parentLocationName;
	}

	public void setParentLocationName(String parentLocationName) {
		this.parentLocationName = parentLocationName;
	}

	public Set<LocationNotes> getNotes() {
		return notes;
	}

	public void setNotes(Set<LocationNotes> list) {
		this.notes = list;
	}

	public Set<TenderTypesLocationLink> getTenderTypesLnk() {
		return tenderTypesLnk;
	}

	public void setTenderTypesLnk(Set<TenderTypesLocationLink> set) {
		this.tenderTypesLnk = set;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public Set<BankAccountsLocationLink> getBankAccountsLocationLink() {
		return bankAccountsLocationLink;
	}

	public void setBankAccountsLocationLink(
			Set<BankAccountsLocationLink> bankAccountsLocationLink) {
		this.bankAccountsLocationLink = bankAccountsLocationLink;
	}

	public Set<Burdens> getBurdens() {
		return burdens;
	}

	public void setBurdens(Set<Burdens> set) {
		this.burdens = set;
	}

	public Set<PositionsAndBillingRates> getRolesAndBillingRates() {
		return rolesAndBillingRates;
	}

	public void setRolesAndBillingRates(
			Set<PositionsAndBillingRates> rolesAndBillingRates) {
		this.rolesAndBillingRates = rolesAndBillingRates;
	}

	public Set<LocationContact> getLocationContact() {
		return locationContact;
	}

	public void setLocationContact(Set<LocationContact> set) {
		this.locationContact = set;
	}

	public LocationMoreInfo getLocationMoreInfo() {
		return locationMoreInfo;
	}

	public void setLocationMoreInfo(LocationMoreInfo locationMoreInfo) {
		this.locationMoreInfo = locationMoreInfo;
	}

	public BurdenInfo getBurdenInfo() {
		return burdenInfo;
	}

	public void setBurdenInfo(BurdenInfo burdenInfo) {
		this.burdenInfo = burdenInfo;
	}

	public String getClientLegalName() {
		return clientLegalName;
	}

	public void setClientLegalName(String clientLegalName) {
		this.clientLegalName = clientLegalName;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public Set<RevenueTypesLocationLink> getRevenueTypesLocationLink() {
		return revenueTypesLocationLink;
	}

	public void setRevenueTypesLocationLink(
			Set<RevenueTypesLocationLink> revenueTypesLocationLink) {
		this.revenueTypesLocationLink = revenueTypesLocationLink;
	}

	public Set<UserLocationLink> getUsersLocationLink() {
		return usersLocationLink;
	}

	public void setUsersLocationLink(Set<UserLocationLink> usersLocationLink) {
		this.usersLocationLink = usersLocationLink;
	}

	public String getExecutive() {
		return executive;
	}

	public void setExecutive(String executive) {
		this.executive = executive;
	}

	public Set<RegionLocationLink> getRegionLocationLinks() {
		return regionLocationLinks;
	}

	public void setRegionLocationLinks(
			Set<RegionLocationLink> regionLocationLinks) {
		this.regionLocationLinks = regionLocationLinks;
	}

	public Set<Revenue> getRevenues() {
		return revenues;
	}

	public void setRevenues(Set<Revenue> revenues) {
		this.revenues = revenues;
	}

	public Set<Source> getSources() {
		return sources;
	}

	public void setSources(Set<Source> sources) {
		this.sources = sources;
	}

	public Integer getNoofSpaces() {
		return noofSpaces;
	}

	public void setNoofSpaces(Integer noofSpaces) {
		this.noofSpaces = noofSpaces;
	}

	public Set<ExclusionBurdens> getExclusionBurdens() {
		return exclusionBurdens;
	}

	public void setExclusionBurdens(Set<ExclusionBurdens> exclusionBurdens) {
		this.exclusionBurdens = exclusionBurdens;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Location location = (Location) o;

		if (!Objects.equals(id, location.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Location [id=" + id + ", locationID=" + locationID + ", name="
				+ name + ", clientLegalName=" + clientLegalName + ", address1="
				+ address1 + ", address2=" + address2 + ", city=" + city
				+ ", county=" + county + ", state=" + state + ", zipCode="
				+ zipCode + ", phone=" + phone + ", cell=" + cell + ", fax="
				+ fax + ", ext=" + ext + ", companyName=" + companyName
				+ ", executive=" + executive + ", serviceType=" + serviceType
				+ ", contractType=" + contractType + ", noofSpaces="
				+ noofSpaces + ", subsidyType=" + subsidyType
				+ ", applicableForDarma=" + applicableForDarma
				+ ", parentLocationID=" + parentLocationID
				+ ", parentLocationName=" + parentLocationName + ", brandCode="
				+ brandCode + ", brand=" + brand + ", notes=" + notes
				+ ", tenderTypesLnk=" + tenderTypesLnk + ", usersLocationLink="
				+ usersLocationLink + ", burdens=" + burdens
				+ ", exclusionBurdens=" + exclusionBurdens
				+ ", rolesAndBillingRates=" + rolesAndBillingRates
				+ ", locationContact=" + locationContact
				+ ", locationMoreInfo=" + locationMoreInfo + ", burdenInfo="
				+ burdenInfo + ", revenueTypesLocationLink="
				+ revenueTypesLocationLink + ", bankAccountsLocationLink="
				+ bankAccountsLocationLink + ", regionLocationLinks="
				+ regionLocationLinks + ", revenues=" + revenues + ", sources="
				+ sources + "]";
	}

}
