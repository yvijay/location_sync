package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "LOCATION_MORE_INFO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LocationMoreInfo extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "vertical")
	private String vertical;

	@Column(name = "old_location_id")
	private String oldLocationID;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "open_date", nullable = false)
	private LocalDateTime openDate;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "close_date", nullable = false)
	private LocalDateTime closeDate;

	@Column(name = "svp")
	private String sVP;

	@Column(name = "rvp")
	private String rVP;

	@Column(name = "subsidy_monthly_rate")
	private String SubsidyMonthlyRate;

	@Column(name = "contract_hours")
	private String contractHours;

	@Column(name = "management_group")
	private String managementGroup;

	@Column(name = "expires")
	private String expires;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "revision_date")
	private LocalDateTime revisionDate;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "action_date")
	private LocalDateTime actionDate;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "contract_period_from")
	private LocalDateTime contractPeriodFrom;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "contract_period_to")
	private LocalDateTime contractPeriodTo;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "last_date_contract_revised")
	private LocalDateTime lastDateContractRevised;

	@Column(name = "contracted_annual_hours")
	private Double contractedAnnualHours;

	@Column(name = "po_number")
	private String pONumber;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContractHours() {
		return contractHours;
	}

	public void setContractHours(String contractHours) {
		this.contractHours = contractHours;
	}

	public String getManagementGroup() {
		return managementGroup;
	}

	public void setManagementGroup(String managementGroup) {
		this.managementGroup = managementGroup;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public LocalDateTime getRevisionDate() {
		return revisionDate;
	}

	public void setRevisionDate(LocalDateTime revisionDate) {
		this.revisionDate = revisionDate;
	}

	public LocalDateTime getActionDate() {
		return actionDate;
	}

	public void setActionDate(LocalDateTime actionDate) {
		this.actionDate = actionDate;
	}

	public LocalDateTime getContractPeriodFrom() {
		return contractPeriodFrom;
	}

	public void setContractPeriodFrom(LocalDateTime contractPeriodFrom) {
		this.contractPeriodFrom = contractPeriodFrom;
	}

	public LocalDateTime getContractPeriodTo() {
		return contractPeriodTo;
	}

	public void setContractPeriodTo(LocalDateTime contractPeriodTo) {
		this.contractPeriodTo = contractPeriodTo;
	}

	public LocalDateTime getLastDateContractRevised() {
		return lastDateContractRevised;
	}

	public void setLastDateContractRevised(LocalDateTime lastDateContractRevised) {
		this.lastDateContractRevised = lastDateContractRevised;
	}

	public Double getContractedAnnualHours() {
		return contractedAnnualHours;
	}

	public void setContractedAnnualHours(Double contractedAnnualHours) {
		this.contractedAnnualHours = contractedAnnualHours;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		this.vertical = vertical;
	}

	public String getOldLocationID() {
		return oldLocationID;
	}

	public void setOldLocationID(String oldLocationID) {
		this.oldLocationID = oldLocationID;
	}

	public String getsVP() {
		return sVP;
	}

	public void setsVP(String sVP) {
		this.sVP = sVP;
	}

	public String getrVP() {
		return rVP;
	}

	public void setrVP(String rVP) {
		this.rVP = rVP;
	}

	public String getSubsidyMonthlyRate() {
		return SubsidyMonthlyRate;
	}

	public void setSubsidyMonthlyRate(String subsidyMonthlyRate) {
		SubsidyMonthlyRate = subsidyMonthlyRate;
	}

	public String getpONumber() {
		return pONumber;
	}

	public void setpONumber(String pONumber) {
		this.pONumber = pONumber;
	}

	public LocalDateTime getOpenDate() {
		return openDate;
	}

	public void setOpenDate(LocalDateTime openDate) {
		this.openDate = openDate;
	}

	public LocalDateTime getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(LocalDateTime closeDate) {
		this.closeDate = closeDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		LocationMoreInfo locationMoreInfo = (LocationMoreInfo) o;

		if (!Objects.equals(id, locationMoreInfo.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "LocationMoreInfo [id=" + id + ", vertical=" + vertical + ", oldLocationID=" + oldLocationID
				+ ", openDate=" + openDate + ", closeDate=" + closeDate + ", sVP=" + sVP + ", rVP=" + rVP
				+ ", SubsidyMonthlyRate=" + SubsidyMonthlyRate + ", contractHours=" + contractHours
				+ ", managementGroup=" + managementGroup + ", expires=" + expires + ", revisionDate=" + revisionDate
				+ ", actionDate=" + actionDate + ", contractPeriodFrom=" + contractPeriodFrom + ", contractPeriodTo="
				+ contractPeriodTo + ", lastDateContractRevised=" + lastDateContractRevised + ", contractedAnnualHours="
				+ contractedAnnualHours + ", pONumber=" + pONumber + "]";
	}

}
