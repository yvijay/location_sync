package com.cp.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.cp.persistence.entity.util.CustomLocalDateSerializer;
import com.cp.persistence.entity.util.ISO8601LocalDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * A NavisionPCI.
 */
@Entity
@NamedStoredProcedureQuery(
		name = "getNavisionPCI", 
		procedureName = "uspNavisionPCI", 
		resultClasses = NavisionPCI.class, 
		parameters = {
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, type = Boolean.class)
		}
	)
public class NavisionPCI implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id    
    @Column(name = "location_id")
    private String location_id;

    @Id
    @Column(name = "Company")
    private String company;

    @Id
    @Column(name = "Location")
    private String location;

    @Id
    @Column(name = "GLBaseAccount")
    private String glBaseAccount;

    @Id
    @Column(name = "CorE")
    private String cOrE;

    @Id
    @Column(name = "RevenueDate")
    private String revenueDate;

    @Id
    @Column(name = "BorG")
    private String bOrG;

    @Id
    @Column(name = "Description")
    private String description;

    @Id
    @Column(name = "Debits", precision=10, scale=2)
    private BigDecimal debits;

    @Id
    @Column(name = "Credits", precision=10, scale=2)
    private BigDecimal credits;

    @Id
    @Column(name = "PostingDate")
    private String postingDate;

    @Id
    @Column(name = "ReconciledDate")
    private String reconciledDate;

    @Id
    @Column(name = "deposit_identifier")
    private String depositIdentifier;

    @Id
    @Column(name = "FilterPostingDate")
    private String filterPostingDate;

    @Id
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	@JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "posted_navision_date")
    private LocalDate postedNavisionDate;

    @Id
    @Column(name = "Comment")
    private String comment;

    
    @Column(name = "SortBy")
    private Integer sortBy;
    
    public String getLocation_id() {
        return location_id;
    }

    public NavisionPCI location_id(String location_id) {
        this.location_id = location_id;
        return this;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getCompany() {
        return company;
    }

    public NavisionPCI company(String company) {
        this.company = company;
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public NavisionPCI location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGlBaseAccount() {
        return glBaseAccount;
    }

    public NavisionPCI glBaseAccount(String glBaseAccount) {
        this.glBaseAccount = glBaseAccount;
        return this;
    }

    public void setGlBaseAccount(String glBaseAccount) {
        this.glBaseAccount = glBaseAccount;
    }

    public String getcOrE() {
        return cOrE;
    }

    public NavisionPCI cOrE(String cOrE) {
        this.cOrE = cOrE;
        return this;
    }

    public void setcOrE(String cOrE) {
        this.cOrE = cOrE;
    }

    public String getRevenueDate() {
        return revenueDate;
    }

    public NavisionPCI revenueDate(String revenueDate) {
        this.revenueDate = revenueDate;
        return this;
    }

    public void setRevenueDate(String revenueDate) {
        this.revenueDate = revenueDate;
    }

    public String getbOrG() {
        return bOrG;
    }

    public NavisionPCI bOrG(String bOrG) {
        this.bOrG = bOrG;
        return this;
    }

    public void setbOrG(String bOrG) {
        this.bOrG = bOrG;
    }

    public String getDescription() {
        return description;
    }

    public NavisionPCI description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDebits() {
        return debits;
    }

    public NavisionPCI debits(BigDecimal debits) {
        this.debits = debits;
        return this;
    }

    public void setDebits(BigDecimal debits) {
        this.debits = debits;
    }

    public BigDecimal getCredits() {
        return credits;
    }

    public NavisionPCI credits(BigDecimal credits) {
        this.credits = credits;
        return this;
    }

    public void setCredits(BigDecimal credits) {
        this.credits = credits;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public NavisionPCI postingDate(String postingDate) {
        this.postingDate = postingDate;
        return this;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getReconciledDate() {
        return reconciledDate;
    }

    public NavisionPCI reconciledDate(String reconciledDate) {
        this.reconciledDate = reconciledDate;
        return this;
    }

    public void setReconciledDate(String reconciledDate) {
        this.reconciledDate = reconciledDate;
    }

    public String getDepositIdentifier() {
        return depositIdentifier;
    }

    public NavisionPCI depositIdentifier(String depositIdentifier) {
        this.depositIdentifier = depositIdentifier;
        return this;
    }

    public void setDepositIdentifier(String depositIdentifier) {
        this.depositIdentifier = depositIdentifier;
    }

    public String getFilterPostingDate() {
        return filterPostingDate;
    }

    public NavisionPCI filterPostingDate(String filterPostingDate) {
        this.filterPostingDate = filterPostingDate;
        return this;
    }

    public void setFilterPostingDate(String filterPostingDate) {
        this.filterPostingDate = filterPostingDate;
    }

    public LocalDate getPostedNavisionDate() {
        return postedNavisionDate;
    }

    public NavisionPCI postedNavisionDate(LocalDate postedNavisionDate) {
        this.postedNavisionDate = postedNavisionDate;
        return this;
    }

    public void setPostedNavisionDate(LocalDate postedNavisionDate) {
        this.postedNavisionDate = postedNavisionDate;
    }

    public String getComment() {
        return comment;
    }

    public NavisionPCI comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getSortBy() {
        return sortBy;
    }

    public NavisionPCI sortBy(Integer sortBy) {
        this.sortBy = sortBy;
        return this;
    }

    public void setSortBy(Integer sortBy) {
        this.sortBy = sortBy;
    }

	@Override
    public String toString() {
        return "NavisionPCI{" +
            ", location_id='" + location_id + "'" +
            ", company='" + company + "'" +
            ", location='" + location + "'" +
            ", glBaseAccount='" + glBaseAccount + "'" +
            ", cOrE='" + cOrE + "'" +
            ", revenueDate='" + revenueDate + "'" +
            ", bOrG='" + bOrG + "'" +
            ", description='" + description + "'" +
            ", debits='" + debits + "'" +
            ", credits='" + credits + "'" +
            ", postingDate='" + postingDate + "'" +
            ", reconciledDate='" + reconciledDate + "'" +
            ", depositIdentifier='" + depositIdentifier + "'" +
            ", filterPostingDate='" + filterPostingDate + "'" +
            ", postedNavisionDate='" + postedNavisionDate + "'" +
            ", comment='" + comment + "'" +
            ", sortBy='" + sortBy + "'" +
            '}';
    }
}
