package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "BURDENS")
@SQLDelete(sql = "UPDATE BURDENS SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Burdens extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "job_description")
	private String jobDescription;

	@Column(name = "type")
	private String type;

	@Column(name = "d_percentage")
	private Double dPercentage;

	@Column(name = "d_gL")
	private String dGL;

	@Column(name = "d_eorC")
	private String dEorC;

	@Column(name = "d_offsetEorC")
	private String dOffsetEorC;

	@Column(name = "c_percentage")
	private Double cPercentage;

	@Column(name = "c_gl")
	private String cGL;

	@Column(name = "c_EorC")
	private String cEorC;

	@Column(name = "c_offsetEorC")
	private String cOffsetEorC;

	@Column(name = "offsetting_account")
	private String offsettingAccount;

	@Column(name = "including_tips")
	private String includingTips;
	
	
	@ManyToOne
	private Location location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getdGL() {
		return dGL;
	}

	public void setdGL(String dGL) {
		this.dGL = dGL;
	}

	public String getdEorC() {
		return dEorC;
	}

	public void setdEorC(String dEorC) {
		this.dEorC = dEorC;
	}

	public String getdOffsetEorC() {
		return dOffsetEorC;
	}

	public void setdOffsetEorC(String dOffsetEorC) {
		this.dOffsetEorC = dOffsetEorC;
	}

	public Double getdPercentage() {
		return dPercentage;
	}

	public void setdPercentage(Double dPercentage) {
		this.dPercentage = dPercentage;
	}

	public Double getcPercentage() {
		return cPercentage;
	}

	public void setcPercentage(Double cPercentage) {
		this.cPercentage = cPercentage;
	}

	public String getcGL() {
		return cGL;
	}

	public void setcGL(String cGL) {
		this.cGL = cGL;
	}

	public String getcEorC() {
		return cEorC;
	}

	public void setcEorC(String cEorC) {
		this.cEorC = cEorC;
	}

	public String getcOffsetEorC() {
		return cOffsetEorC;
	}

	public void setcOffsetEorC(String cOffsetEorC) {
		this.cOffsetEorC = cOffsetEorC;
	}

	public String getOffsettingAccount() {
		return offsettingAccount;
	}

	public void setOffsettingAccount(String offsettingAccount) {
		this.offsettingAccount = offsettingAccount;
	}


	
	public String getIncludingTips() {
		return includingTips;
	}

	public void setIncludingTips(String includingTips) {
		this.includingTips = includingTips;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Burdens burdens = (Burdens) o;

		if (!Objects.equals(id, burdens.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Burdens [id=" + id + ", jobDescription=" + jobDescription + ", type=" + type + ", dPercentage="
				+ dPercentage + ", dGL=" + dGL + ", dEorC=" + dEorC + ", dOffsetEorC=" + dOffsetEorC + ", cPercentage="
				+ cPercentage + ", cGL=" + cGL + ", cEorC=" + cEorC + ", cOffsetEorC=" + cOffsetEorC
				+ ", offsettingAccount=" + offsettingAccount + ", includingTips=" + includingTips + ", location="
				+ location + "]";
	}

	

	
}
