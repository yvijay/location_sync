package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "BURDENS_INFO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BurdenInfo extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "exclusion")
	private Boolean exclusion;

	@Column(name = "use_actuals")
	private Boolean useActuals;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getExclusion() {
		return exclusion;
	}

	public void setExclusion(Boolean exclusion) {
		this.exclusion = exclusion;
	}

	public Boolean getUseActuals() {
		return useActuals;
	}

	public void setUseActuals(Boolean useActuals) {
		this.useActuals = useActuals;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BurdenInfo burdenInfo = (BurdenInfo) o;

		if (!Objects.equals(id, burdenInfo.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "BurdenInfo [id=" + id + ", exclusion=" + exclusion
				+ ", useActuals=" + useActuals + "]";
	}

}
