package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "REVENUE_TYPES")
@SQLDelete(sql = "UPDATE REVENUE_TYPES SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RevenueTypes extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "gl_number")
	private String glNumber;

	@Column(name = "oneGL")
	private String oneGL;

	@Column(name = "company_name")
	private String companyName;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "revenuetypes_id",updatable=false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<RevenueTypesLocationLink> revenueTypesLocationLink = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<RevenueTypesLocationLink> getRevenueTypesLocationLink() {
		return revenueTypesLocationLink;
	}

	public void setRevenueTypesLocationLink(
			Set<RevenueTypesLocationLink> revenueTypesLocationLink) {
		this.revenueTypesLocationLink = revenueTypesLocationLink;
	}

	public String getGlNumber() {
		return glNumber;
	}

	public void setGlNumber(String glNumber) {
		this.glNumber = glNumber;
	}

	public String getOneGL() {
		return oneGL;
	}

	public void setOneGL(String oneGL) {
		this.oneGL = oneGL;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		RevenueTypes revenueTypes = (RevenueTypes) o;

		if (!Objects.equals(id, revenueTypes.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "RevenueTypes [id=" + id + ", code=" + code + ", description="
				+ description + ", glNumber=" + glNumber + ", oneGL=" + oneGL
				+ ", companyName=" + companyName
				+ ", revenueTypesLocationLink=" + revenueTypesLocationLink
				+ "]";
	}

}
