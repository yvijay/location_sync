package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

/**
 * A BILLING_EXTRACT.
 */
@Entity
@Table(name = "BILLING_EXTRACT")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BillingExtract extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@ManyToOne
	private BillingExtractFile billingExtractFile;
	
	@Column(name = "pay_period")
	private String payPeriod;

	@Column(name = "pay_date")
	private Date payDate;

	@Column(name = "brand_code")
	private String brandCode;
	
	@Column(name = "location_name")
	private String locationName;

	@Column(name = "gl_string")
	private String glString;
	
	@Column(name = "parent_org_unit_code")
	private String parentOrgUnitCode;

	@Column(name = "onsite_dept_name")
	private String onsiteDeptName;
	
	@Column(name = "onsite_dept_desc")
	private String onsiteDeptDesc;
	
	@Column(name = "was_org_code")
	private String wasOrgCode;
	
	@Column(name = "was_dept_code")
	private String wasDeptCode;
	
	@Column(name = "was_job_code")
	private String wasJobCode;

	@Column(name = "job_name")
	private String jobName;

	@Column(name = "billable")
	private String billable;

	@Column(name = "position_code")
	private String positionCode;

	@Column(name = "territory_code")
	private String territoryCode;

	@Column(name = "regular_hrs")
	private Double regularHrs;

	@Column(name = "ot_holiday_hrs")
	private Double otHolidayHrs;

	@Column(name = "total_hrs")
	private Double totalHrs;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BillingExtractFile getBillingExtractFile() {
		return billingExtractFile;
	}

	public void setBillingExtractFile(BillingExtractFile billingExtractFile) {
		this.billingExtractFile = billingExtractFile;
	}

	public String getPayPeriod() {
		return payPeriod;
	}

	public void setPayPeriod(String payPeriod) {
		this.payPeriod = payPeriod;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getGlString() {
		return glString;
	}

	public void setGlString(String glString) {
		this.glString = glString;
	}

	public String getParentOrgUnitCode() {
		return parentOrgUnitCode;
	}

	public void setParentOrgUnitCode(String parentOrgUnitCode) {
		this.parentOrgUnitCode = parentOrgUnitCode;
	}

	public String getOnsiteDeptName() {
		return onsiteDeptName;
	}

	public void setOnsiteDeptName(String onsiteDeptName) {
		this.onsiteDeptName = onsiteDeptName;
	}

	public String getOnsiteDeptDesc() {
		return onsiteDeptDesc;
	}

	public void setOnsiteDeptDesc(String onsiteDeptDesc) {
		this.onsiteDeptDesc = onsiteDeptDesc;
	}

	public String getWasOrgCode() {
		return wasOrgCode;
	}

	public void setWasOrgCode(String wasOrgCode) {
		this.wasOrgCode = wasOrgCode;
	}

	public String getWasDeptCode() {
		return wasDeptCode;
	}

	public void setWasDeptCode(String wasDeptCode) {
		this.wasDeptCode = wasDeptCode;
	}

	public String getWasJobCode() {
		return wasJobCode;
	}

	public void setWasJobCode(String wasJobCode) {
		this.wasJobCode = wasJobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getBillable() {
		return billable;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getTerritoryCode() {
		return territoryCode;
	}

	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	public Double getRegularHrs() {
		return regularHrs;
	}

	public void setRegularHrs(Double regularHrs) {
		this.regularHrs = regularHrs;
	}

	public Double getOtHolidayHrs() {
		return otHolidayHrs;
	}

	public void setOtHolidayHrs(Double otHolidayHrs) {
		this.otHolidayHrs = otHolidayHrs;
	}

	public Double getTotalHrs() {
		return totalHrs;
	}

	public void setTotalHrs(Double totalHrs) {
		this.totalHrs = totalHrs;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BillingExtract billingExtract = (BillingExtract) o;

		if (!Objects.equals(id, billingExtract.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
