package com.cp.persistence.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Transaction from darma is rewritten as Source entity
 */
@Entity
@Table(name = "SOURCE")
@SQLDelete(sql = "UPDATE SOURCE SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Source extends AbstractAuditable implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bankaccount_id", nullable = false)
	private BankAccounts bankAccounts;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id", nullable = false)
	private Location location;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "revenuetype_id", nullable = false)
	private RevenueTypes revenueTypes;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tender_id", nullable = false)
	private TenderTypes tenderTypes;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submitted_by")
	private User submittedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "posted_by")
	private User postedBy;

	/*
	 * @Column(name = "revenue_id", nullable = false) private String revenueId;
	 */

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "transaction_date", nullable = false)
	private LocalDateTime transactionDate;

	@Column(name = "amount", nullable = false)
	private BigDecimal amount;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "reconciled_date")
	private LocalDateTime reconciledDate;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "posting_date")
	private LocalDateTime postingDate;

	@Column(name = "sub_account_code", length = 1)
	private Character subAccountCode;

	@Column(name = "deferred_yn", nullable = false, length = 1)
	private char deferredYn;

	@Column(name = "deferred_month_1")
	private Short deferredMonth1;

	@Column(name = "deferred_year_1")
	private Short deferredYear1;

	@Column(name = "deferred_amount_1", scale = 4)
	private BigDecimal deferredAmount1;

	@Column(name = "deferred_month_2")
	private Short deferredMonth2;

	@Column(name = "deferred_year_2")
	private Short deferredYear2;

	@Column(name = "deferred_amount_2", scale = 4)
	private BigDecimal deferredAmount2;

	@Column(name = "deferred_month_3")
	private Short deferredMonth3;

	@Column(name = "deferred_year_3")
	private Short deferredYear3;

	@Column(name = "deferred_amount_3", scale = 4)
	private BigDecimal deferredAmount3;

	@Column(name = "deferred_month_4")
	private Short deferredMonth4;

	@Column(name = "deferred_year_4")
	private Short deferredYear4;

	@Column(name = "deferred_amount_4", scale = 4)
	private BigDecimal deferredAmount4;

	@Column(name = "deferred_month_5")
	private Short deferredMonth5;

	@Column(name = "deferred_year_5")
	private Short deferredYear5;

	@Column(name = "deferred_amount_5", scale = 4)
	private BigDecimal deferredAmount5;

	@Column(name = "deferred_month_6")
	private Short deferredMonth6;

	@Column(name = "deferred_year_6")
	private Short deferredYear6;

	@Column(name = "deferred_amount_6", scale = 4)
	private BigDecimal deferredAmount6;

	@Column(name = "deferred_month_7")
	private Short deferredMonth7;

	@Column(name = "deferred_year_7")
	private Short deferredYear7;

	@Column(name = "deferred_amount_7", scale = 4)
	private BigDecimal deferredAmount7;

	@Column(name = "deferred_month_8")
	private Short deferredMonth8;

	@Column(name = "deferred_year_8")
	private Short deferredYear8;

	@Column(name = "deferred_amount_8", scale = 4)
	private BigDecimal deferredAmount8;

	@Column(name = "deferred_month_9")
	private Short deferredMonth9;

	@Column(name = "deferred_year_9")
	private Short deferredYear9;

	@Column(name = "deferred_amount_9", scale = 4)
	private BigDecimal deferredAmount9;

	@Column(name = "deferred_month_10")
	private Short deferredMonth10;

	@Column(name = "deferred_year_10")
	private Short deferredYear10;

	@Column(name = "deferred_amount_10", scale = 4)
	private BigDecimal deferredAmount10;

	@Column(name = "deferred_month_11")
	private Short deferredMonth11;

	@Column(name = "deferred_year_11")
	private Short deferredYear11;

	@Column(name = "deferred_amount_11", scale = 4)
	private BigDecimal deferredAmount11;

	@Column(name = "deferred_month_12")
	private Short deferredMonth12;

	@Column(name = "deferred_year_12")
	private Short deferredYear12;

	@Column(name = "deferred_amount_12", scale = 4)
	private BigDecimal deferredAmount12;

	@Column(name = "deposit_identifier", length = 100)
	private String depositIdentifier;

	@Column(name = "corrective_yn", nullable = false, length = 1)
	private char correctiveYn;

	@Column(name = "corrective_descr")
	private String correctiveDescr;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "posted_navision_date")
	private LocalDateTime postedNavisionDate;

	@Column(name = "pci", nullable = false)
	private Boolean pci;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "submitted_date")
	private LocalDateTime submittedDate;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<SourceNotes> notes = new HashSet<SourceNotes>();

	@ManyToOne
	private Revenue revenue;

	// export fileds

	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate transactionDateOnly;

	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate submittedDateOnly;

	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate postingDateOnly;

	public LocalDate getPostingDateOnly() {
		return postingDate.toLocalDate();
	}

	public LocalDate getTransactionDateOnly() {
		return transactionDate.toLocalDate();
	}

	public LocalDate getSubmittedDateOnly() {
		return submittedDate.toLocalDate();
	}

	public Revenue getRevenue() {
		return revenue;
	}

	public void setRevenue(Revenue revenue) {
		this.revenue = revenue;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BankAccounts getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(BankAccounts bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public RevenueTypes getRevenueTypes() {
		return revenueTypes;
	}

	public void setRevenueTypes(RevenueTypes revenueTypes) {
		this.revenueTypes = revenueTypes;
	}

	public TenderTypes getTenderTypes() {
		return tenderTypes;
	}

	public void setTenderTypes(TenderTypes tenderTypes) {
		this.tenderTypes = tenderTypes;
	}

	public User getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(User submittedBy) {
		this.submittedBy = submittedBy;
	}

	public User getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(User postedBy) {
		this.postedBy = postedBy;
	}

	/*
	 * public String getRevenueId() { return revenueId; }
	 * 
	 * public void setRevenueId(String revenueId) { this.revenueId = revenueId;
	 * }
	 */

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public LocalDateTime getReconciledDate() {
		return reconciledDate;
	}

	public void setReconciledDate(LocalDateTime reconciledDate) {
		this.reconciledDate = reconciledDate;
	}

	public LocalDateTime getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(LocalDateTime postingDate) {
		this.postingDate = postingDate;
	}

	public Character getSubAccountCode() {
		return subAccountCode;
	}

	public void setSubAccountCode(Character subAccountCode) {
		this.subAccountCode = subAccountCode;
	}

	public char getDeferredYn() {
		return deferredYn;
	}

	public void setDeferredYn(char deferredYn) {
		this.deferredYn = deferredYn;
	}

	public Short getDeferredMonth1() {
		return deferredMonth1;
	}

	public void setDeferredMonth1(Short deferredMonth1) {
		this.deferredMonth1 = deferredMonth1;
	}

	public Short getDeferredYear1() {
		return deferredYear1;
	}

	public void setDeferredYear1(Short deferredYear1) {
		this.deferredYear1 = deferredYear1;
	}

	public BigDecimal getDeferredAmount1() {
		return deferredAmount1;
	}

	public void setDeferredAmount1(BigDecimal deferredAmount1) {
		this.deferredAmount1 = deferredAmount1;
	}

	public Short getDeferredMonth2() {
		return deferredMonth2;
	}

	public void setDeferredMonth2(Short deferredMonth2) {
		this.deferredMonth2 = deferredMonth2;
	}

	public Short getDeferredYear2() {
		return deferredYear2;
	}

	public void setDeferredYear2(Short deferredYear2) {
		this.deferredYear2 = deferredYear2;
	}

	public BigDecimal getDeferredAmount2() {
		return deferredAmount2;
	}

	public void setDeferredAmount2(BigDecimal deferredAmount2) {
		this.deferredAmount2 = deferredAmount2;
	}

	public Short getDeferredMonth3() {
		return deferredMonth3;
	}

	public void setDeferredMonth3(Short deferredMonth3) {
		this.deferredMonth3 = deferredMonth3;
	}

	public Short getDeferredYear3() {
		return deferredYear3;
	}

	public void setDeferredYear3(Short deferredYear3) {
		this.deferredYear3 = deferredYear3;
	}

	public BigDecimal getDeferredAmount3() {
		return deferredAmount3;
	}

	public void setDeferredAmount3(BigDecimal deferredAmount3) {
		this.deferredAmount3 = deferredAmount3;
	}

	public Short getDeferredMonth4() {
		return deferredMonth4;
	}

	public void setDeferredMonth4(Short deferredMonth4) {
		this.deferredMonth4 = deferredMonth4;
	}

	public Short getDeferredYear4() {
		return deferredYear4;
	}

	public void setDeferredYear4(Short deferredYear4) {
		this.deferredYear4 = deferredYear4;
	}

	public BigDecimal getDeferredAmount4() {
		return deferredAmount4;
	}

	public void setDeferredAmount4(BigDecimal deferredAmount4) {
		this.deferredAmount4 = deferredAmount4;
	}

	public Short getDeferredMonth5() {
		return deferredMonth5;
	}

	public void setDeferredMonth5(Short deferredMonth5) {
		this.deferredMonth5 = deferredMonth5;
	}

	public Short getDeferredYear5() {
		return deferredYear5;
	}

	public void setDeferredYear5(Short deferredYear5) {
		this.deferredYear5 = deferredYear5;
	}

	public BigDecimal getDeferredAmount5() {
		return deferredAmount5;
	}

	public void setDeferredAmount5(BigDecimal deferredAmount5) {
		this.deferredAmount5 = deferredAmount5;
	}

	public Short getDeferredMonth6() {
		return deferredMonth6;
	}

	public void setDeferredMonth6(Short deferredMonth6) {
		this.deferredMonth6 = deferredMonth6;
	}

	public Short getDeferredYear6() {
		return deferredYear6;
	}

	public void setDeferredYear6(Short deferredYear6) {
		this.deferredYear6 = deferredYear6;
	}

	public BigDecimal getDeferredAmount6() {
		return deferredAmount6;
	}

	public void setDeferredAmount6(BigDecimal deferredAmount6) {
		this.deferredAmount6 = deferredAmount6;
	}

	public Short getDeferredMonth7() {
		return deferredMonth7;
	}

	public void setDeferredMonth7(Short deferredMonth7) {
		this.deferredMonth7 = deferredMonth7;
	}

	public Short getDeferredYear7() {
		return deferredYear7;
	}

	public void setDeferredYear7(Short deferredYear7) {
		this.deferredYear7 = deferredYear7;
	}

	public BigDecimal getDeferredAmount7() {
		return deferredAmount7;
	}

	public void setDeferredAmount7(BigDecimal deferredAmount7) {
		this.deferredAmount7 = deferredAmount7;
	}

	public Short getDeferredMonth8() {
		return deferredMonth8;
	}

	public void setDeferredMonth8(Short deferredMonth8) {
		this.deferredMonth8 = deferredMonth8;
	}

	public Short getDeferredYear8() {
		return deferredYear8;
	}

	public void setDeferredYear8(Short deferredYear8) {
		this.deferredYear8 = deferredYear8;
	}

	public BigDecimal getDeferredAmount8() {
		return deferredAmount8;
	}

	public void setDeferredAmount8(BigDecimal deferredAmount8) {
		this.deferredAmount8 = deferredAmount8;
	}

	public Short getDeferredMonth9() {
		return deferredMonth9;
	}

	public void setDeferredMonth9(Short deferredMonth9) {
		this.deferredMonth9 = deferredMonth9;
	}

	public Short getDeferredYear9() {
		return deferredYear9;
	}

	public void setDeferredYear9(Short deferredYear9) {
		this.deferredYear9 = deferredYear9;
	}

	public BigDecimal getDeferredAmount9() {
		return deferredAmount9;
	}

	public void setDeferredAmount9(BigDecimal deferredAmount9) {
		this.deferredAmount9 = deferredAmount9;
	}

	public Short getDeferredMonth10() {
		return deferredMonth10;
	}

	public void setDeferredMonth10(Short deferredMonth10) {
		this.deferredMonth10 = deferredMonth10;
	}

	public Short getDeferredYear10() {
		return deferredYear10;
	}

	public void setDeferredYear10(Short deferredYear10) {
		this.deferredYear10 = deferredYear10;
	}

	public BigDecimal getDeferredAmount10() {
		return deferredAmount10;
	}

	public void setDeferredAmount10(BigDecimal deferredAmount10) {
		this.deferredAmount10 = deferredAmount10;
	}

	public Short getDeferredMonth11() {
		return deferredMonth11;
	}

	public void setDeferredMonth11(Short deferredMonth11) {
		this.deferredMonth11 = deferredMonth11;
	}

	public Short getDeferredYear11() {
		return deferredYear11;
	}

	public void setDeferredYear11(Short deferredYear11) {
		this.deferredYear11 = deferredYear11;
	}

	public BigDecimal getDeferredAmount11() {
		return deferredAmount11;
	}

	public void setDeferredAmount11(BigDecimal deferredAmount11) {
		this.deferredAmount11 = deferredAmount11;
	}

	public Short getDeferredMonth12() {
		return deferredMonth12;
	}

	public void setDeferredMonth12(Short deferredMonth12) {
		this.deferredMonth12 = deferredMonth12;
	}

	public Short getDeferredYear12() {
		return deferredYear12;
	}

	public void setDeferredYear12(Short deferredYear12) {
		this.deferredYear12 = deferredYear12;
	}

	public BigDecimal getDeferredAmount12() {
		return deferredAmount12;
	}

	public void setDeferredAmount12(BigDecimal deferredAmount12) {
		this.deferredAmount12 = deferredAmount12;
	}

	public String getDepositIdentifier() {
		return depositIdentifier;
	}

	public void setDepositIdentifier(String depositIdentifier) {
		this.depositIdentifier = depositIdentifier;
	}

	public char getCorrectiveYn() {
		return correctiveYn;
	}

	public void setCorrectiveYn(char correctiveYn) {
		this.correctiveYn = correctiveYn;
	}

	public String getCorrectiveDescr() {
		return correctiveDescr;
	}

	public void setCorrectiveDescr(String correctiveDescr) {
		this.correctiveDescr = correctiveDescr;
	}

	public LocalDateTime getPostedNavisionDate() {
		return postedNavisionDate;
	}

	public void setPostedNavisionDate(LocalDateTime postedNavisionDate) {
		this.postedNavisionDate = postedNavisionDate;
	}

	public Boolean getPci() {
		return pci;
	}

	public void setPci(Boolean pci) {
		this.pci = pci;
	}

	public LocalDateTime getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(LocalDateTime submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Set<SourceNotes> getNotes() {
		return notes;
	}

	public void setNotes(Set<SourceNotes> notes) {
		this.notes = notes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Source contactTypes = (Source) o;

		if (!Objects.equals(id, contactTypes.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
