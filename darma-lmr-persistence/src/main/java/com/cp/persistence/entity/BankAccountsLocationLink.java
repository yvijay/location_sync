package com.cp.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "BANK_ACCOUNT_LOCATION_LINK")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankAccountsLocationLink extends AbstractAuditable implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	
	@ManyToOne
	private BankAccounts bankaccount;

	@ManyToOne
	private Location location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public BankAccounts getBankaccount() {
		return bankaccount;
	}

	public void setBankaccount(BankAccounts bankaccount) {
		this.bankaccount = bankaccount;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "BankAccountsLocationLink [id=" + id + ", bankaccount="
				+ bankaccount + ", location=" + location + "]";
	}
	
	
}
