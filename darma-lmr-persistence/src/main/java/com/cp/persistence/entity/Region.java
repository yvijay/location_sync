package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "REGION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Region extends AbstractAuditable implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;
	
	@Column(name = "logo")
	private String logo;
	
	@ManyToOne
	private Brand brand;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}


	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	@Override
	 public boolean equals(Object o) {
	  if (this == o) {
	   return true;
	  }
	  if (o == null || getClass() != o.getClass()) {
	   return false;
	  }

	  Region regions = (Region) o;

	  if (!Objects.equals(id, regions.id))
	   return false;

	  return true;
	 }

	 @Override
	 public int hashCode() {
	  return Objects.hashCode(id);
	 }

	@Override
	public String toString() {
		return "Region [id=" + id + ", code=" + code + ", description=" + description + ", logo=" + logo + ", brand="
				+ brand + "]";
	}
	
	
	
	
}
