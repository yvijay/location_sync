package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "REGION_LOCATION_LINK")
@SQLDelete(sql = "UPDATE REGION_LOCATION_LINK SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RegionLocationLink extends AbstractAuditable implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@ManyToOne
	private Region region;

	@ManyToOne
	private Location location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RegionLocationLink revenueTypesLocationLink = (RegionLocationLink) o;
		if (revenueTypesLocationLink.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, revenueTypesLocationLink.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
