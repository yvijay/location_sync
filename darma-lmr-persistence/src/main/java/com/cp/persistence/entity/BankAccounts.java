package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "BANK_ACCOUNT")
@SQLDelete(sql = "UPDATE BANK_ACCOUNT SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankAccounts extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "description")
	private String description;

	@Column(name = "account_type")
	private String accountType;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "routing_number")
	private String routingNumber;

	@Column(name = "gl_number")
	private String glNumber;

	@Column(name = "oneGL")
	private String oneGL;

	@ManyToOne
	private Brand brand;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "bankaccount_id",updatable=false)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<BankAccountsLocationLink> bankAccountsLocationLink = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getGlNumber() {
		return glNumber;
	}

	public void setGlNumber(String glNumber) {
		this.glNumber = glNumber;
	}

	public Set<BankAccountsLocationLink> getBankAccountsLocationLink() {
		return bankAccountsLocationLink;
	}

	public void setBankAccountsLocationLink(
			Set<BankAccountsLocationLink> bankAccountsLocationLink) {
		this.bankAccountsLocationLink = bankAccountsLocationLink;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getOneGL() {
		return oneGL;
	}

	public void setOneGL(String oneGL) {
		this.oneGL = oneGL;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BankAccounts bankAccounts = (BankAccounts) o;
		if (bankAccounts.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, bankAccounts.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	

}
