package com.cp.persistence.entity;

// Generated 28 Mar, 2017 12:55:16 PM by Hibernate Tools 5.2.1.Final

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Total from darma is rewritten as Revenue entity
 */
@Entity
@Table(name = "REVENUE")
@SQLDelete(sql = "UPDATE REVENUE SET status = 'X' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "status <> 'X'")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Revenue extends AbstractAuditable implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id", nullable = false)
	private Location location;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bankaccount_id")
	private BankAccounts bankAccounts;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "revenue_Date", nullable = false)
	private LocalDateTime revenueDate;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "number_of_deposits")
	private Integer numberOfDeposits;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submitted_by")
	private User submittedBy;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "submitted_date")
	private LocalDateTime submittedDate;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "revenue_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<RevenueNotes> notes = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "revenue_id", updatable = false, insertable = false)
	@JsonIgnore
	@NotFound(action = NotFoundAction.IGNORE)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private List<Source> sources = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tender_id")
	private TenderTypes tenderTypes;

	@Column(name = "deposit_identifier", length = 100)
	private String depositIdentifier;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "posted_by")
	private User postedBy;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "posting_date")
	private LocalDateTime postingDate;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "reconciled_date")
	private LocalDateTime reconciledDate;

	@Column(name = "mark_for_correction")
	private Boolean markForCorrection;

	public List<Source> getSources() {
		return sources;
	}

	public void setSources(List<Source> sources) {
		this.sources = sources;
	}

	public TenderTypes getTenderTypes() {
		return tenderTypes;
	}

	public void setTenderTypes(TenderTypes tenderTypes) {
		this.tenderTypes = tenderTypes;
	}

	public String getDepositIdentifier() {
		return depositIdentifier;
	}

	public void setDepositIdentifier(String depositIdentifier) {
		this.depositIdentifier = depositIdentifier;
	}

	public User getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(User postedBy) {
		this.postedBy = postedBy;
	}

	public LocalDateTime getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(LocalDateTime postingDate) {
		this.postingDate = postingDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public LocalDateTime getRevenueDate() {
		return revenueDate;
	}

	public void setRevenueDate(LocalDateTime revenueDate) {
		this.revenueDate = revenueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getNumberOfDeposits() {
		return numberOfDeposits;
	}

	public void setNumberOfDeposits(Integer numberOfDeposits) {
		this.numberOfDeposits = numberOfDeposits;
	}

	public User getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(User submittedBy) {
		this.submittedBy = submittedBy;
	}

	public LocalDateTime getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(LocalDateTime submittedDate) {
		this.submittedDate = submittedDate;
	}

	public Set<RevenueNotes> getNotes() {
		return notes;
	}

	public void setNotes(Set<RevenueNotes> notes) {
		this.notes = notes;
	}

	public BankAccounts getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(BankAccounts bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public LocalDateTime getReconciledDate() {
		return reconciledDate;
	}

	public void setReconciledDate(LocalDateTime reconciledDate) {
		this.reconciledDate = reconciledDate;
	}

	public Boolean getMarkForCorrection() {
		return markForCorrection;
	}

	public void setMarkForCorrection(Boolean markForCorrection) {
		this.markForCorrection = markForCorrection;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Revenue contactTypes = (Revenue) o;

		if (!Objects.equals(id, contactTypes.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
