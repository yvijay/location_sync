package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "FINANCIAL_SERVICES")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FinancialServices extends AbstractAuditable implements Serializable  {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "type")
	private String type;

	@Column(name = "description")
	private String description;

	@Column(name = "ancillary_bucket")
	private String ancillaryBucket;

	@Column(name = "c_gl_mna_code")
	private String cGlMnaCode;
	
	@Column(name = "c_gl_nmna_code")
	private String cGlNmnaCode;

	@Column(name = "d_glcode")
	private String dGLCode;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "financialServices_id")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<FinancialYear> financialYear = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAncillaryBucket() {
		return ancillaryBucket;
	}

	public void setAncillaryBucket(String ancillaryBucket) {
		this.ancillaryBucket = ancillaryBucket;
	}

	public String getdGLCode() {
		return dGLCode;
	}

	public void setdGLCode(String dGLCode) {
		this.dGLCode = dGLCode;
	}

	public Set<FinancialYear> getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(Set<FinancialYear> financialYear) {
		this.financialYear = financialYear;
	}

	
	public String getcGlMnaCode() {
		return cGlMnaCode;
	}

	public void setcGlMnaCode(String cGlMnaCode) {
		this.cGlMnaCode = cGlMnaCode;
	}

	public String getcGlNmnaCode() {
		return cGlNmnaCode;
	}

	public void setcGlNmnaCode(String cGlNmnaCode) {
		this.cGlNmnaCode = cGlNmnaCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FinancialServices financialServices = (FinancialServices) o;
		if (financialServices.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, financialServices.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "FinancialServices [id=" + id + ", type=" + type
				+ ", description=" + description + ", ancillaryBucket="
				+ ancillaryBucket + ", cGlMnaCode=" + cGlMnaCode
				+ ", cGlNmnaCode=" + cGlNmnaCode + ", dGLCode=" + dGLCode
				+ ", financialYear=" + financialYear + "]";
	}
	
	
}
