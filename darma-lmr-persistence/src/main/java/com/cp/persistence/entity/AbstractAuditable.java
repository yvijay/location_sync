package com.cp.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.StringUtils;

import com.cp.persistence.dto.enums.StatusEnum;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditable {

	@Column(name = "created_date", insertable = true, updatable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "created_by", insertable = true, updatable = false)
	@CreatedBy
	private String createdBy;

	@Column(name = "last_updated_date", insertable = false, updatable = true)
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedDate;

	@Column(name = "last_updated_by", insertable = false, updatable = true)
	@LastModifiedBy
	private String lastUpdatedBy;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private StatusEnum status;
	
	@Transient
	private String statusDes;

	// ///////////// PRESET COLUMNS /////////////////////
	@PrePersist
	public void setDefaults() {
		this.status = StringUtils.isEmpty(this.status) ? StatusEnum.A
				: this.status;
	}

	@PreRemove
	public void delete() {
		this.status = StatusEnum.X;
	}

	// ///////////// PRESET COLUMNS /////////////////////

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	public String getStatusDes() {
		return getStatus().getValue();
	}

	@Override
	public String toString() {
		return "AbstractAuditable [createdDate=" + createdDate + ", createdBy="
				+ createdBy + ", lastUpdatedDate=" + lastUpdatedDate
				+ ", lastUpdatedBy=" + lastUpdatedBy + ", status=" + status
				+ "]";
	}

}
