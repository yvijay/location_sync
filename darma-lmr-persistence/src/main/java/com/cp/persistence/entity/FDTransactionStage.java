package com.cp.persistence.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="FDTRANSACTION_STAGE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FDTransactionStage  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name="LocationID")
	private String locationID;
	
	@Column(name="ProcessedCurrencyCode")
	private String processedCurrencyCode;
	
	@Column(name="TransactionDate")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime transactionDate;
	
	@Column(name="ExternalMID")
	private String externalMID;
	
	@Column(name="DbaName")
	private String dbaName;
	
	@Column(name="TerminalID")
	private String terminalID;
	
	@Column(name="BatchNumber")
	private String batchNumber;
	
	@Column(name="BatchSequenceNumber")
	private String batchSequenceNumber;
	
	@Column(name="InvoiceNumber")
	private String invoiceNumber;
	
	@Column(name="SubmitDate")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime submitDate;
	
	@Column(name="FundedDate")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime fundedDate;
	
	@Column(name="CardType")
	private String cardType;
	
	@Column(name="CardholderNumber")
	private String cardholderNumber;
	
	@Column(name="ProcessedTransactionAmount")
	private BigDecimal processedTransactionAmount;
	
	@Column(name="TransactionType")
	private String transactionType;
	
	@Column(name="TransactionStatus")
	private String transactionStatus;
	
	@Column(name="PosEntryMode")
	private String posEntryMode;
	
	@Column(name="PosEntryDescription")
	private String posEntryDescription;
	
	@Column(name="AuthorizationCode")
	private String authorizationCode;
	
	@Column(name="ServiceCode")
	private String serviceCode;
	
	@Column(name="AuthorizationTransactionType")
	private String authorizationTransactionType;
	
	@Column(name="PlanCode")
	private String planCode;
	
	@Column(name="PlanCodeDescription")
	private String planCodeDescription;
	
	@Column(name="DebitNetworkID")
	private String debitNetworkID;
	
	@Column(name="RejectReason")
	private String rejectReason;
	
	@Column(name="TrackingNumber")
	private String trackingNumber;
	
	@Column(name="OrderNumber")
	private String orderNumber;
	
	@Column(name="AttachmentName")
	private String attachmentName;
	
	@Column(name="CreatedDate")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime createdDate;
	
	@Column(name="XLocation")
	private String xLocation;
	
	@Column(name="XLocationId")
	private String xLocationId;
	
	@Column(name="XCardType")
	private String xCardType;
	
	@Column(name="XTenderTypeId")
	private String xTenderTypeId;

	
	public String getLocationID() {
		return locationID;
	}
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	public String getProcessedCurrencyCode() {
		return processedCurrencyCode;
	}
	public void setProcessedCurrencyCode(String processedCurrencyCode) {
		this.processedCurrencyCode = processedCurrencyCode;
	}
	
	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}
	public LocalDateTime getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(LocalDateTime submitDate) {
		this.submitDate = submitDate;
	}
	public LocalDateTime getFundedDate() {
		return fundedDate;
	}
	public void setFundedDate(LocalDateTime fundedDate) {
		this.fundedDate = fundedDate;
	}
	public String getExternalMID() {
		return externalMID;
	}
	public void setExternalMID(String externalMID) {
		this.externalMID = externalMID;
	}
	public String getDbaName() {
		return dbaName;
	}
	public void setDbaName(String dbaName) {
		this.dbaName = dbaName;
	}
	public String getTerminalID() {
		return terminalID;
	}
	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getBatchSequenceNumber() {
		return batchSequenceNumber;
	}
	public void setBatchSequenceNumber(String batchSequenceNumber) {
		this.batchSequenceNumber = batchSequenceNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardholderNumber() {
		return cardholderNumber;
	}
	public void setCardholderNumber(String cardholderNumber) {
		this.cardholderNumber = cardholderNumber;
	}
	public BigDecimal getProcessedTransactionAmount() {
		return processedTransactionAmount;
	}
	public void setProcessedTransactionAmount(BigDecimal processedTransactionAmount) {
		this.processedTransactionAmount = processedTransactionAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getPosEntryMode() {
		return posEntryMode;
	}
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}
	public String getPosEntryDescription() {
		return posEntryDescription;
	}
	public void setPosEntryDescription(String posEntryDescription) {
		this.posEntryDescription = posEntryDescription;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getAuthorizationTransactionType() {
		return authorizationTransactionType;
	}
	public void setAuthorizationTransactionType(String authorizationTransactionType) {
		this.authorizationTransactionType = authorizationTransactionType;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public String getPlanCodeDescription() {
		return planCodeDescription;
	}
	public void setPlanCodeDescription(String planCodeDescription) {
		this.planCodeDescription = planCodeDescription;
	}
	public String getDebitNetworkID() {
		return debitNetworkID;
	}
	public void setDebitNetworkID(String debitNetworkID) {
		this.debitNetworkID = debitNetworkID;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getxLocation() {
		return xLocation;
	}
	public void setxLocation(String xLocation) {
		this.xLocation = xLocation;
	}
	public String getxCardType() {
		return xCardType;
	}
	public void setxCardType(String xCardType) {
		this.xCardType = xCardType;
	}
	public String getxLocationId() {
		return xLocationId;
	}
	public void setxLocationId(String xLocationId) {
		this.xLocationId = xLocationId;
	}
	public String getxTenderTypeId() {
		return xTenderTypeId;
	}
	public void setxTenderTypeId(String xTenderTypeId) {
		this.xTenderTypeId = xTenderTypeId;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FDTransactionStage other = (FDTransactionStage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
