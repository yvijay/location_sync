package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "REVENUE_TYPES_LOCATION_LINK")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RevenueTypesLocationLink extends AbstractAuditable implements
		Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@ManyToOne
	private RevenueTypes revenuetypes;


	@ManyToOne
	private Location location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	

	public RevenueTypes getRevenuetypes() {
		return revenuetypes;
	}

	public void setRevenuetypes(RevenueTypes revenuetypes) {
		this.revenuetypes = revenuetypes;
	}


	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RevenueTypesLocationLink revenueTypesLocationLink = (RevenueTypesLocationLink) o;
		if (revenueTypesLocationLink.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, revenueTypesLocationLink.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

}
