package com.cp.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "vw_Alloc_Burdens")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class vwAllocations implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "uniqId")
	private String uniqId;//THIS COULMN IS TO MAINTAIN UNIQUENESS HENCE SHOULDN'T USE ANY WHERE
	
	@Column(name = "id")
	private String id;

	@Column(name = "job_description")
	private String jobDescription;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "location_id")
	private String locationId;

	@Column(name = "type")
	private String type;

	@Column(name = "d_percentage")
	private Double dPercentage;

	@Column(name = "d_gL")
	private String dGL;

	@Column(name = "d_eorC")
	private String dEorC;

	@Column(name = "d_offsetEorC")
	private String dOffsetEorC;

	@Column(name = "c_percentage")
	private Double cPercentage;

	@Column(name = "c_gl")
	private String cGL;

	@Column(name = "c_EorC")
	private String cEorC;

	@Column(name = "c_offsetEorC")
	private String cOffsetEorC;

	@Column(name = "year")
	private Integer year;

	@Column(name = "managed")
	private String managed;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getdGL() {
		return dGL;
	}

	public void setdGL(String dGL) {
		this.dGL = dGL;
	}

	public String getdEorC() {
		return dEorC;
	}

	public void setdEorC(String dEorC) {
		this.dEorC = dEorC;
	}

	public String getdOffsetEorC() {
		return dOffsetEorC;
	}

	public void setdOffsetEorC(String dOffsetEorC) {
		this.dOffsetEorC = dOffsetEorC;
	}

	public Double getdPercentage() {
		return dPercentage;
	}

	public void setdPercentage(Double dPercentage) {
		this.dPercentage = dPercentage;
	}

	public Double getcPercentage() {
		return cPercentage;
	}

	public void setcPercentage(Double cPercentage) {
		this.cPercentage = cPercentage;
	}

	public String getcGL() {
		return cGL;
	}

	public void setcGL(String cGL) {
		this.cGL = cGL;
	}

	public String getcEorC() {
		return cEorC;
	}

	public void setcEorC(String cEorC) {
		this.cEorC = cEorC;
	}

	public String getcOffsetEorC() {
		return cOffsetEorC;
	}

	public void setcOffsetEorC(String cOffsetEorC) {
		this.cOffsetEorC = cOffsetEorC;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getManaged() {
		return managed;
	}

	public void setManaged(String managed) {
		this.managed = managed;
	}
	
	public String getUniqId() {
		return uniqId;
	}

	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}

	@Override
	public String toString() {
		return "vwAllocations [id=" + id + ", jobDescription=" + jobDescription
				+ ", companyName=" + companyName + ", locationId=" + locationId + ", type=" + type + ", dPercentage="
				+ dPercentage + ", dGL=" + dGL + ", dEorC=" + dEorC + ", dOffsetEorC=" + dOffsetEorC + ", cPercentage="
				+ cPercentage + ", cGL=" + cGL + ", cEorC=" + cEorC + ", cOffsetEorC=" + cOffsetEorC + ", year=" + year
				+ ", managed=" + managed + "]";
	}


}
