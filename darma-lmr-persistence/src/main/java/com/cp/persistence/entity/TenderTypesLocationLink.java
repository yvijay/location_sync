package com.cp.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TENDER_TYPES_LOCATION_LINK")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TenderTypesLocationLink extends AbstractAuditable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	@ManyToOne
	private TenderTypes tendertypes;

	@ManyToOne
	private Location location;

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}



	public TenderTypes getTendertypes() {
		return tendertypes;
	}

	public void setTendertypes(TenderTypes tendertypes) {
		this.tendertypes = tendertypes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TenderTypesLocationLink tenderTypesLocationLink = (TenderTypesLocationLink) o;
		if (tenderTypesLocationLink.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, tenderTypesLocationLink.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}



}
