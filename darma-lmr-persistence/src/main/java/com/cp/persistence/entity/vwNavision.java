package com.cp.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "vwNavision")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Immutable
public class vwNavision implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "location_id")
	private String locationId;

	@Column(name = "company")
	private String company;

	@Column(name = "location")
	private String location;

	@Column(name = "GLBaseAccount")
	private String gLBaseAccount;

	@Column(name = "CorE")
	private String corE;
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "RevenueDate")
	private LocalDateTime revenueDate;

	@Column(name = "BorG")
	private String borG;
	
	@Column(name = "Description")
	private String description;
	
	@Column(name = "Debits")
	private BigDecimal debits;
	
	@Column(name = "Credits")
	private BigDecimal credits;
	
	@Column(name = "posted_by")
	private String postedBy;
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "PostingDate")
	private LocalDateTime postingDate;
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "ReconciledDate")
	private LocalDateTime reconciledDate;
	
	@Column(name = "deposit_identifier")
	private String depositidentifier;
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "FilterPostingDate")
	private LocalDateTime filterPostingDate;
	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "posted_navision_date")
	private LocalDateTime postednavisiondate;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate postingDateOnly;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate reconciledDateOnly;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate filterPostingDateOnly;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate revenueDateOnly;
	
	
	public String getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getGLBaseAccount() {
		return gLBaseAccount;
	}

	public void setGLBaseAccount(String gLBaseAccount) {
		this.gLBaseAccount = gLBaseAccount;
	}

	public String getCorE() {
		return corE;
	}

	public void setCorE(String corE) {
		this.corE = corE;
	}

	public LocalDateTime getRevenueDate() {
		return revenueDate;
	}

	public void setRevenueDate(LocalDateTime revenueDate) {
		this.revenueDate = revenueDate;
	}

	public BigDecimal getDebits() {
		return debits;
	}

	public void setDebits(BigDecimal debits) {
		this.debits = debits;
	}

	public BigDecimal getCredits() {
		return credits;
	}

	public void setCredits(BigDecimal credits) {
		this.credits = credits;
	}

	public LocalDateTime getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(LocalDateTime postingDate) {
		this.postingDate = postingDate;
	}

	public LocalDateTime getFilterPostingDate() {
		return filterPostingDate;
	}

	public void setFilterPostingDate(LocalDateTime filterPostingDate) {
		this.filterPostingDate = filterPostingDate;
	}

	public LocalDateTime getPostednavisiondate() {
		return postednavisiondate;
	}

	public void setPostednavisiondate(LocalDateTime postednavisiondate) {
		this.postednavisiondate = postednavisiondate;
	}

	public LocalDateTime getReconciledDate() {
		return reconciledDate;
	}

	public void setReconciledDate(LocalDateTime reconciledDate) {
		this.reconciledDate = reconciledDate;
	}

	public String getBorG() {
		return borG;
	}

	public void setBorG(String borG) {
		this.borG = borG;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDepositidentifier() {
		return depositidentifier;
	}

	public void setDepositidentifier(String depositidentifier) {
		this.depositidentifier = depositidentifier;
	}

	public LocalDate getPostingDateOnly() {
		return postingDate.toLocalDate();
	}

	public LocalDate getReconciledDateOnly() {
		return reconciledDate.toLocalDate();
	}

	public LocalDate getFilterPostingDateOnly() {
		return filterPostingDate.toLocalDate();
	}

	public LocalDate getRevenueDateOnly() {
		return revenueDate.toLocalDate();
	}

	@Override
	public String toString() {
		return "vwNavision [id=" + id + ", locationId=" + locationId + ", company=" + company + ", location=" + location
				+ ", GLBaseAccount=" + gLBaseAccount + ", CorE=" + corE + ", revenueDate=" + revenueDate + ", BorG="
				+ borG + ", description=" + description + ", debits=" + debits + ", credits=" + credits + ", postedBy="
				+ postedBy + ", postingDate=" + postingDate + ", reconciledDate=" + reconciledDate
				+ ", depositidentifier=" + depositidentifier + ", filterPostingDate=" + filterPostingDate
				+ ", postednavisiondate=" + postednavisiondate + ", postingDateOnly=" + postingDateOnly
				+ ", reconciledDateOnly=" + reconciledDateOnly + ", filterPostingDateOnly=" + filterPostingDateOnly
				+ ", revenueDateOnly=" + revenueDateOnly + "]";
	}


	
	
	
}
