package com.cp.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "FINANCIAL_YEAR")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FinancialYear extends AbstractAuditable implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;

	@Column(name = "year")
	private Integer year;

	@Column(name = "year_percent")
	private Float yearPercent;

	@Column(name = "year_amount")
	private BigDecimal yearAmount;

	@Column(name = "overall_percent")
	private Float overallPercent;

	@Size(max = 4000)
	@Column(name = "notes")
	private String notes;

	@ManyToOne
	private FinancialServices financialServices;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Float getYearPercent() {
		return yearPercent;
	}

	public void setYearPercent(Float yearPercent) {
		this.yearPercent = yearPercent;
	}

	public BigDecimal getYearAmount() {
		return yearAmount;
	}

	public void setYearAmount(BigDecimal yearAmount) {
		this.yearAmount = yearAmount;
	}

	public Float getOverallPercent() {
		return overallPercent;
	}

	public void setOverallPercent(Float overallPercent) {
		this.overallPercent = overallPercent;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public FinancialServices getFinancialServices() {
		return financialServices;
	}

	public void setFinancialServices(FinancialServices financialServices) {
		this.financialServices = financialServices;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FinancialYear financialYear = (FinancialYear) o;
		if (financialYear.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, financialYear.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "FinancialYear [id=" + id + ", year=" + year + ", yearPercent="
				+ yearPercent + ", yearAmount=" + yearAmount
				+ ", overallPercent=" + overallPercent + ", notes=" + notes
				+ ", financialServices=" + financialServices + "]";
	}

}
