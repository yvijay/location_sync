package com.cp.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "vwSourcesForPosting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class vwSourcesForPosting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "bankaccount_id")
	private String bankaccountid;
	
	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "location_id")
	private String locationid;
	
	@Column(name = "location_name")
	private String locationName;

	@Column(name = "tender_id")
	private String tenderid;
	
	@Column(name = "tender_description")
	private String tenderDescription;

	@Column(name = "revenuetype_id")
	private String revenuetypeid;
	
	@Column(name = "revenue_description")
	private String revenueDescription;
	
	@Column(name = "transaction_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime transactionDate;

	@Column(name = "submitted_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime submitteddate;
	
	@Column(name = "submitted_display_name")
	private String submittedDisplayName;
	
	@Column(name = "amount")
	private String amount;
	
	@Column(name = "posting_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDateTime postingDate;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	@Column(name = "reconciled_date")
	private LocalDateTime reconciledDate;
	
	@Column(name = "region_id")
	private String regionid;
	
	@Column(name = "sub_account_code")
	private Character subAccountCode;
	
	@Column(name = "submitted_by")
	private String submittedBy;
	
	@Column(name = "sub_total")
	private String subTotal;
	
	@Column(name = "Location")
	private String location;
	
	@Column(name = "corrective_yn")
	private char correctiveYn;
	
	@Column(name = "deposit_identifier")
	private String depositIdentifier;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate postingDateOnly;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate submitteddateOnly;
	
	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@JsonDeserialize(using = CustomDateTimeDeserializer.class)
	private LocalDate transactionDateOnly;
	

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTenderDescription() {
		return tenderDescription;
	}

	public void setTenderDescription(String tenderDescription) {
		this.tenderDescription = tenderDescription;
	}
	
	

	public LocalDateTime getReconciledDate() {
		return reconciledDate;
	}

	public void setReconciledDate(LocalDateTime reconciledDate) {
		this.reconciledDate = reconciledDate;
	}

	public String getRevenueDescription() {
		return revenueDescription;
	}

	public void setRevenueDescription(String revenueDescription) {
		this.revenueDescription = revenueDescription;
	}

	public String getSubmittedDisplayName() {
		return submittedDisplayName;
	}

	public void setSubmittedDisplayName(String submittedDisplayName) {
		this.submittedDisplayName = submittedDisplayName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBankaccountid() {
		return bankaccountid;
	}

	public void setBankaccountid(String bankaccountid) {
		this.bankaccountid = bankaccountid;
	}

	public String getLocationid() {
		return locationid;
	}

	public void setLocationid(String locationid) {
		this.locationid = locationid;
	}

	public String getTenderid() {
		return tenderid;
	}

	public void setTenderid(String tenderid) {
		this.tenderid = tenderid;
	}

	public String getRevenuetypeid() {
		return revenuetypeid;
	}

	public void setRevenuetypeid(String revenuetypeid) {
		this.revenuetypeid = revenuetypeid;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public LocalDateTime getSubmitteddate() {
		return submitteddate;
	}

	public void setSubmitteddate(LocalDateTime submitteddate) {
		this.submitteddate = submitteddate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public LocalDateTime getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(LocalDateTime postingDate) {
		this.postingDate = postingDate;
	}

	public String getRegionid() {
		return regionid;
	}

	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}

	public Character getSubAccountCode() {
		return subAccountCode;
	}

	public void setSubAccountCode(Character subAccountCode) {
		this.subAccountCode = subAccountCode;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public char getCorrectiveYn() {
		return correctiveYn;
	}

	public void setCorrectiveYn(char correctiveYn) {
		this.correctiveYn = correctiveYn;
	}

	public String getDepositIdentifier() {
		return depositIdentifier;
	}

	public void setDepositIdentifier(String depositIdentifier) {
		this.depositIdentifier = depositIdentifier;
	}

	public LocalDate getPostingDateOnly() {
		return postingDate.toLocalDate();
	}

	public LocalDate getSubmitteddateOnly() {
		return submitteddate.toLocalDate();
	}

	public LocalDate getTransactionDateOnly() {
		return transactionDate.toLocalDate();
	}

	@Override
	public String toString() {
		return "vwSourcesForPosting [id=" + id + ", bankaccountid=" + bankaccountid + ", bankName=" + bankName
				+ ", locationid=" + locationid + ", locationName=" + locationName + ", tenderid=" + tenderid
				+ ", tenderDescription=" + tenderDescription + ", revenuetypeid=" + revenuetypeid
				+ ", revenueDescription=" + revenueDescription + ", transactionDate=" + transactionDate
				+ ", submitteddate=" + submitteddate + ", submittedDisplayName=" + submittedDisplayName + ", amount="
				+ amount + ", postingDate=" + postingDate + ", reconciledDate=" + reconciledDate + ", regionid="
				+ regionid + ", subAccountCode=" + subAccountCode + ", submittedBy=" + submittedBy + ", subTotal="
				+ subTotal + ", location=" + location + ", correctiveYn=" + correctiveYn + ", depositIdentifier="
				+ depositIdentifier + ", postingDateOnly=" + postingDateOnly + ", submitteddateOnly="
				+ submitteddateOnly + ", transactionDateOnly=" + transactionDateOnly + "]";
	}


	

	
	
}
