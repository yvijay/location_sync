package com.cp.persistence.mapper;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RevenueAndSourceDTO;
import com.cp.persistence.dto.RevenueDTO;
import com.cp.persistence.dto.SourceDTO;
import com.cp.persistence.entity.Revenue;

@Component
public class RevenueAndSourceMapper {



	@Autowired
	private LocationMapper locationMapper;
	
	@Autowired
	private UserMapper userMapper;

	public Revenue revenueAndSourceDTOToRevenue(RevenueAndSourceDTO revenueAndSourceDTO) {

		if (revenueAndSourceDTO == null)
			return null;
		Revenue revenue = new Revenue();
		revenue.setId(revenueAndSourceDTO.getId());
		revenue.setAmount(revenueAndSourceDTO.getAmount());
		revenue.setLocation(locationMapper.locationDTOToLocation(revenueAndSourceDTO.getLocationDTO()));
		revenue.setNumberOfDeposits(revenueAndSourceDTO.getSources().size());
		revenue.setStatus(revenueAndSourceDTO.getStatus());
		revenue.setRevenueDate(revenueAndSourceDTO.getRevenueDate());
		revenue.setSubmittedDate(revenueAndSourceDTO.getSubmittedDate());
		return revenue;
	}

	public RevenueAndSourceDTO getRevenueAndSourceDTO(RevenueDTO revenueDTO, Set<SourceDTO> setSourceDTO) {

		RevenueAndSourceDTO revenueAndSourceDTO = new RevenueAndSourceDTO();

		revenueAndSourceDTO.setAmount(revenueDTO.getAmount());
		revenueAndSourceDTO.setLocationDTO(revenueDTO.getLocationDTO());
		revenueAndSourceDTO.setSubmittedDate(revenueDTO.getSubmittedDate());
		revenueAndSourceDTO.setNumberOfDeposits(revenueDTO.getNumberOfDeposits());
		revenueAndSourceDTO.setStatus(revenueDTO.getStatus());
		revenueAndSourceDTO.setSubmittedBy(revenueDTO.getSubmittedBy());
		revenueAndSourceDTO.setSubmittedDate(revenueDTO.getSubmittedDate());
		revenueAndSourceDTO.setId(revenueDTO.getId());
		revenueAndSourceDTO.setRevenueDate(revenueDTO.getRevenueDate());
		
		revenueAndSourceDTO.setSources(setSourceDTO);
		return revenueAndSourceDTO;
	}

}
