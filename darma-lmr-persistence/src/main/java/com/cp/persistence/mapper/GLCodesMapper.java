package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.GLCodesDTO;
import com.cp.persistence.entity.GLCodes;


@Component
public class GLCodesMapper {

	@Autowired
	private BrandMapper brandMapper;
	
	public GLCodes gLCodesDTOToGLCodes(GLCodesDTO glCodesDTO)
	{
		if(glCodesDTO ==null)
		{
			return null;
		}
		
		GLCodes glCodes=new GLCodes();
		glCodes.setBrand(brandMapper.brandDTOToBrand(glCodesDTO.getBrandDTO()));
		glCodes.setCode(glCodesDTO.getCode());
		glCodes.setDescription(glCodesDTO.getDescription());
		glCodes.setId(glCodesDTO.getId());
		glCodes.setType(glCodesDTO.getType());
		glCodes.setStatus(glCodesDTO.getStatus());
		return glCodes;
	}
	
	public GLCodesDTO gLCodesToGLCodesDTO(GLCodes glCodes)
	{
		if(glCodes ==null)
		{
			return null;
		}
		
		GLCodesDTO glCodesDTO=new GLCodesDTO();
		glCodesDTO.setBrandDTO(brandMapper.brandToBrandDTO(glCodes.getBrand()));
		glCodesDTO.setCode(glCodes.getCode());
		glCodesDTO.setDescription(glCodes.getDescription());
		glCodesDTO.setId(glCodes.getId());
		glCodesDTO.setType(glCodes.getType());
		glCodesDTO.setStatus(glCodes.getStatus());
		return glCodesDTO;
	}
}
