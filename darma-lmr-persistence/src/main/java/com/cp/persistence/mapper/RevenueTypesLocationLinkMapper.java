package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.RevenueTypesLocationLinkDTO;
import com.cp.persistence.entity.Location;
import com.cp.persistence.entity.RevenueTypesLocationLink;

@Component
public class RevenueTypesLocationLinkMapper {

	@Autowired
	LocationMapper locationMapper;
	@Autowired
	RevenueTypesMapper revenueTypesMapper;

	public RevenueTypesLocationLinkDTO revenueTypeLocationLinkToRevenueTypeLocationLinkDTO(
			RevenueTypesLocationLink revenueTypesLocationLink) {
		if (revenueTypesLocationLink == null) {
			return null;
		}
		RevenueTypesLocationLinkDTO revenueTypesLocationLinkDTO = new RevenueTypesLocationLinkDTO();
		revenueTypesLocationLinkDTO.setId(revenueTypesLocationLink.getId());
		revenueTypesLocationLinkDTO.setRevenueTypesDTO(
				revenueTypesMapper.revenueTypesToRevenueTypesDTO(revenueTypesLocationLink.getRevenuetypes()));
		/*revenueTypesLocationLinkDTO
				.setLocationDTO(getLocationDTO(revenueTypesLocationLink.getLocation()));*/
		revenueTypesLocationLinkDTO.setStatus(revenueTypesLocationLink.getStatus());
		return revenueTypesLocationLinkDTO;

	}

	public RevenueTypesLocationLink revenueTypeLocationLinkDTOToRevenueTypeLocationLink(
			RevenueTypesLocationLinkDTO revenueTypesLocationLinkDTO) {
		if (revenueTypesLocationLinkDTO == null) {
			return null;
		}
		RevenueTypesLocationLink revenueTypesLocationLink = new RevenueTypesLocationLink();
		revenueTypesLocationLink.setRevenuetypes(
				revenueTypesMapper.revenueTypesDTOToRevenueTypes(revenueTypesLocationLinkDTO.getRevenueTypesDTO()));
	revenueTypesLocationLink
				.setLocation(getLocation(revenueTypesLocationLinkDTO.getLocationDTO()));
	revenueTypesLocationLink.setStatus(revenueTypesLocationLinkDTO.getStatus());
		return revenueTypesLocationLink;

	}

	private Location getLocation(LocationDTO locationDTO) {
		if (locationDTO == null) {
			return null;
		}
		Location location = new Location();
		location.setId(locationDTO.getId());
		return location;
	}
}
