package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.TenderTypesDTO;
import com.cp.persistence.entity.TenderTypes;
import com.cp.persistence.repository.TenderTypesRepository;

@Component
public class TenderTypesMapper {
	
	@Autowired
	private TenderTypesRepository tenderTypesRepository;

	public TenderTypesDTO tenderTypesTOTenderTypesDTO(TenderTypes tenderTypes) {
		if (tenderTypes == null) {
			return null;
		}
		TenderTypesDTO tenderTypesDTO = new TenderTypesDTO();

		tenderTypesDTO.setCode(tenderTypes.getCode());
		tenderTypesDTO.setDescription(tenderTypes.getDescription());
		tenderTypesDTO.setId(tenderTypes.getId());
		tenderTypesDTO.setIsPCI(tenderTypes.getIsPCI());
		tenderTypesDTO.setStatus(tenderTypes.getStatus());
		return tenderTypesDTO;

	}

	public TenderTypes tenderTypesDTOTOTenderTypes(TenderTypesDTO tenderTypesDTO) {
		if (tenderTypesDTO == null) {
			return null;
		}
		TenderTypes tenderTypes = new TenderTypes();

		tenderTypes.setCode(tenderTypesDTO.getCode());
		tenderTypes.setDescription(tenderTypesDTO.getDescription());
		tenderTypes.setId(tenderTypesDTO.getId());
		tenderTypes.setIsPCI(tenderTypesDTO.getIsPCI());
		tenderTypes.setStatus(tenderTypesDTO.getStatus());
		return tenderTypes;

	}

	public TenderTypesDTO getTenderTypeDTO(String tenderid) {
		if(tenderid == null)
		return null;
		else return tenderTypesTOTenderTypesDTO(tenderTypesRepository.findOne(tenderid));
			
	}
}
