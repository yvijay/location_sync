package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BankAccountsDTO;
import com.cp.persistence.entity.BankAccounts;
import com.cp.persistence.repository.BankAccountsRepository;

@Component
public class BankAccountsMapper {

	@Autowired
	private ContactTypesMapper contactTypesMapper;

	@Autowired
	private LocationMapper locationMapper;

	@Autowired
	private BrandMapper brandMapper;

	@Autowired
	private BankAccountsRepository bankAccountsRepository;

	public BankAccountsDTO bankAccountsToBankAccountsDTO(
			BankAccounts bankAccounts) {

		if (bankAccounts == null) {
			return null;
		}
		BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
		bankAccountsDTO.setAccountNumber(bankAccounts.getAccountNumber());
		bankAccountsDTO.setAccountType(bankAccounts.getAccountType());
		bankAccountsDTO.setBankName(bankAccounts.getBankName());
		bankAccountsDTO.setOneGL(bankAccounts.getOneGL());
		bankAccountsDTO.setBrandDTO(brandMapper.brandToBrandDTO(bankAccounts
				.getBrand()));
		/*
		 * bankAccountsDTO .setContactTypesID(contactTypesMapper
		 * .contactTypesToContactTypesDTO(bankAccounts .getContactTypesID()));
		 */
		bankAccountsDTO.setDescription(bankAccounts.getDescription());
		bankAccountsDTO.setGlNumber(bankAccounts.getGlNumber());
		bankAccountsDTO.setId(bankAccounts.getId());
		bankAccountsDTO.setRoutingNumber(bankAccounts.getRoutingNumber());
		bankAccountsDTO.setStatus(bankAccounts.getStatus());
		/*
		 * bankAccountsDTO.setLocationDTO((locationMapper
		 * .locationToLocationDTO(bankAccounts.getLocation())));
		 */

		return bankAccountsDTO;

	}

	public BankAccounts bankAccountsDTOToBankAccounts(
			BankAccountsDTO bankAccountsDTO) {

		if (bankAccountsDTO == null) {
			return null;
		}

		BankAccounts bankAccounts = new BankAccounts();

		bankAccounts.setAccountNumber(bankAccountsDTO.getAccountNumber());
		bankAccounts.setAccountType(bankAccountsDTO.getAccountType());
		bankAccounts.setBankName(bankAccountsDTO.getBankName());
		bankAccounts.setBrand(brandMapper.brandDTOToBrand(bankAccountsDTO
				.getBrandDTO()));
		bankAccounts.setOneGL(bankAccountsDTO.getOneGL());
		bankAccounts.setStatus(bankAccountsDTO.getStatus());
		/*
		 * bankAccounts.setContactTypesID(contactTypesMapper
		 * .contactTypesDTOToContactTypes(bankAccountsDTO
		 * .getContactTypesID()));
		 */
		bankAccounts.setDescription(bankAccountsDTO.getDescription());
		bankAccounts.setGlNumber(bankAccountsDTO.getGlNumber());
		bankAccounts.setId(bankAccountsDTO.getId());
		bankAccounts.setRoutingNumber(bankAccountsDTO.getRoutingNumber());
		/*
		 * bankAccounts.setLocation(locationMapper
		 * .locationDTOToLocation((bankAccountsDTO.getLocationDTO())));
		 */
		return bankAccounts;
	}

	/**
	 * THIS METHOD IS SPECIFIC FOR REVENUE AND SOURCE....!
	 * 
	 * @param location
	 * @return
	 */

	public BankAccountsDTO getBankAccountDTO(BankAccounts bankAccounts) {

		if (bankAccounts == null) {
			return null;
		}
		BankAccountsDTO bankAccountsDTO = new BankAccountsDTO();
		bankAccountsDTO.setAccountNumber(bankAccounts.getAccountNumber());
		bankAccountsDTO.setAccountType(bankAccounts.getAccountType());
		bankAccountsDTO.setBankName(bankAccounts.getBankName());
		bankAccountsDTO.setOneGL(bankAccounts.getOneGL());
		/*
		 * bankAccountsDTO .setContactTypesID(contactTypesMapper
		 * .contactTypesToContactTypesDTO(bankAccounts .getContactTypesID()));
		 */
		bankAccountsDTO.setDescription(bankAccounts.getDescription());
		bankAccountsDTO.setGlNumber(bankAccounts.getGlNumber());
		bankAccountsDTO.setId(bankAccounts.getId());
		bankAccountsDTO.setRoutingNumber(bankAccounts.getRoutingNumber());
		bankAccountsDTO.setStatus(bankAccounts.getStatus());
		return bankAccountsDTO;

	}

	public BankAccountsDTO getBankAccountDTO(String bankaccountid) {
		if (bankaccountid == null)
			return null;
		return getBankAccountDTO(bankAccountsRepository.findOne(bankaccountid));
	}

}
