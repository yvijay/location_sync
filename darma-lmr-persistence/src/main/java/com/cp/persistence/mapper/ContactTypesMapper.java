package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.ContactTypesDTO;
import com.cp.persistence.entity.ContactTypes;

@Component
public class ContactTypesMapper {

	public ContactTypesDTO contactTypesToContactTypesDTO(ContactTypes contactTypes) {
		if (contactTypes == null) {
			return null;
		}

		ContactTypesDTO contactTypesDTO = new ContactTypesDTO();

		contactTypesDTO.setCode(contactTypes.getCode());
		contactTypesDTO.setDescription(contactTypes.getDescription());
		contactTypesDTO.setId(contactTypes.getId());
		contactTypesDTO.setStatus(contactTypes.getStatus());
		return contactTypesDTO;
	}

	public ContactTypes contactTypesDTOToContactTypes(
			ContactTypesDTO contactTypesDTO) {
		if (contactTypesDTO == null) {
			return null;
		}

		ContactTypes contactTypes = new ContactTypes();

		contactTypes.setCode(contactTypesDTO.getCode());
		contactTypes.setDescription(contactTypesDTO.getDescription());
		contactTypes.setId(contactTypesDTO.getId());
		contactTypes.setStatus(contactTypesDTO.getStatus());
		return contactTypes;
	}
}
