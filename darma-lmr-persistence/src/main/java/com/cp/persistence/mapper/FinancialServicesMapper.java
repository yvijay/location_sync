package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.FinancialServicesDTO;
import com.cp.persistence.dto.FinancialYearDTO;
import com.cp.persistence.entity.FinancialServices;
import com.cp.persistence.entity.FinancialYear;


@Component
public class FinancialServicesMapper {

	
	@Autowired
	private FinancialYearMapper financialYearMapper;
	
	public FinancialServicesDTO financialServicesToFinancialServicesDTO(
			FinancialServices financialServices) {
		if(financialServices==null)
			return null;
		FinancialServicesDTO financialServicesDTO=new FinancialServicesDTO();
		
		financialServicesDTO.setId(financialServices.getId());
		financialServicesDTO.setAncillaryBucket(financialServices.getAncillaryBucket());
		financialServicesDTO.setcGlMnaCode(financialServices.getcGlMnaCode());
		financialServicesDTO.setcGlNmnaCode(financialServices.getcGlNmnaCode());
		financialServicesDTO.setDescription(financialServices.getDescription());
		financialServicesDTO.setdGLCode(financialServices.getdGLCode());
	//	financialServicesDTO.setFinancialYearDTO(financialServicesSetToFinancialServiceDTOSet(financialServices.getFinancialYear()));
		financialServicesDTO.setType(financialServices.getType());
		return financialServicesDTO;
}

	public FinancialServices financialServicesDTOToFinancialServices(
			FinancialServicesDTO financialServicesDTO) {
		if(financialServicesDTO==null)
			return null;
		FinancialServices financialServices=new FinancialServices();
		
		financialServices.setId(financialServicesDTO.getId());
		financialServices.setAncillaryBucket(financialServicesDTO.getAncillaryBucket());
		financialServices.setcGlMnaCode(financialServicesDTO.getcGlMnaCode());
		financialServices.setcGlNmnaCode(financialServicesDTO.getcGlNmnaCode());
		financialServices.setDescription(financialServicesDTO.getDescription());
		financialServices.setdGLCode(financialServicesDTO.getdGLCode());
		financialServices.setFinancialYear(financialServicesDTOSetToFinancialServiceSet(financialServicesDTO.getFinancialYearDTO()));
		financialServices.setType(financialServicesDTO.getType());
		return financialServices;
}

	private Set<FinancialYear> financialServicesDTOSetToFinancialServiceSet(
			Set<FinancialYearDTO> financialYearDTO) {
		if(financialYearDTO == null) {
			return null;
		}
		
		Set<FinancialYear> financialYear = new HashSet<FinancialYear>();
		for(FinancialYearDTO financialYearsDTO : financialYearDTO){
			financialYear.add(financialYearMapper.financialYearDTOToFinancialYear(financialYearsDTO));
		}
		
		return financialYear;
	}

	
}
