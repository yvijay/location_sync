package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.PositionsAndBillingRatesDTO;
import com.cp.persistence.entity.PositionsAndBillingRates;

@Component
public class PositionsAndBillingRatesMapper {

	@Autowired
	private LocationMapper locationMapper;

	public PositionsAndBillingRatesDTO rolesAndBillingRatesTORolesAndBillingRatesDTO(
			PositionsAndBillingRates rolesAndBillingRates) {
		if (rolesAndBillingRates == null) {
			return null;
		}
		PositionsAndBillingRatesDTO rolesAndBillingRatesDTO = new PositionsAndBillingRatesDTO();

		rolesAndBillingRatesDTO.setBillingRate(rolesAndBillingRates.getBillingRate());
		rolesAndBillingRatesDTO.setId(rolesAndBillingRates.getId());
		rolesAndBillingRatesDTO
				.setLocationDTO(locationMapper.locationToLocationDTO(rolesAndBillingRates.getLocation()));
		//rolesAndBillingRatesDTO.setPosition(rolesAndBillingRates.getPosition());
		rolesAndBillingRatesDTO.setStatus(rolesAndBillingRates.getStatus());
		return rolesAndBillingRatesDTO;
	}

	public PositionsAndBillingRates rolesAndBillingRatesDTOTORolesAndBillingRates(
			PositionsAndBillingRatesDTO rolesAndBillingRatesDTO) {
		if (rolesAndBillingRatesDTO == null) {
			return null;
		}
		PositionsAndBillingRates rolesAndBillingRates = new PositionsAndBillingRates();

		rolesAndBillingRates.setBillingRate(rolesAndBillingRatesDTO.getBillingRate());
		rolesAndBillingRates.setId(rolesAndBillingRatesDTO.getId());
		rolesAndBillingRates
				.setLocation(locationMapper.locationDTOToLocation(rolesAndBillingRatesDTO.getLocationDTO()));
	//	rolesAndBillingRates.setPosition(rolesAndBillingRatesDTO.getPosition());
		rolesAndBillingRates.setStatus(rolesAndBillingRatesDTO.getStatus());
		return rolesAndBillingRates;
	}

	// RolesAndBiilingRates without location
	public PositionsAndBillingRatesDTO rolesAndBillingRatesTORolesAndBillingRatesDTOWithoutLocation(
			PositionsAndBillingRates rolesAndBillingRates) {
		if (rolesAndBillingRates == null) {
			return null;
		}
		PositionsAndBillingRatesDTO rolesAndBillingRatesDTO = new PositionsAndBillingRatesDTO();

		rolesAndBillingRatesDTO.setBillingRate(rolesAndBillingRates.getBillingRate());
		rolesAndBillingRatesDTO.setId(rolesAndBillingRates.getId());
		// rolesAndBillingRatesDTO.setLocationDTO(locationMapper.locationToLocationDTO(rolesAndBillingRates.getLocation()));
	//	rolesAndBillingRatesDTO.setPosition(rolesAndBillingRates.getPosition());
		rolesAndBillingRatesDTO.setStatus(rolesAndBillingRates.getStatus());
		return rolesAndBillingRatesDTO;
	}

}
