package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.JobDetailsDTO;
import com.cp.persistence.entity.JobDetails;

@Component
public class JobDetailsMapper {
	
	public JobDetailsDTO jobDetailsToJobDetailsDTO(JobDetails jobDetails) {
		
		if(jobDetails == null) {
			return null;
		}
		
		JobDetailsDTO jobDetailsDTO = new JobDetailsDTO();
		
		jobDetailsDTO.setId(jobDetails.getId());
		jobDetailsDTO.setJobName(jobDetails.getJobName());
		jobDetailsDTO.setJobCode(jobDetails.getJobCode());
		
		return jobDetailsDTO;
	}
	
	public JobDetails jobDetailsDTOToJobDetails(JobDetailsDTO jobDetailsDTO) {
		
		if (jobDetailsDTO == null) {
			return null;
		}
		
		JobDetails jobDetails = new JobDetails();
		
		jobDetails.setId(jobDetailsDTO.getId());
		jobDetails.setJobName(jobDetailsDTO.getJobName());
		jobDetails.setJobCode(jobDetailsDTO.getJobCode());

		return jobDetails;
	}
	
}
