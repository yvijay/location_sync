package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.SourceDTO;
import com.cp.persistence.dto.SourceNotesDTO;
import com.cp.persistence.entity.Source;
import com.cp.persistence.entity.SourceNotes;

@Component
public class SourceMapper {

	@Autowired
	private BankAccountsMapper bankAccountsMapper;

	@Autowired
	private LocationMapper locationMapper;
	
	@Autowired
	private UserMapper userMapper;

	@Autowired
	private RevenueTypesMapper revenueTypesMapper;

	@Autowired
	private TenderTypesMapper tenderTypesMapper;

	@Autowired
	private SourceNotesMapper sourceNotesMapper;
	
	@Autowired
	private RevenueMapper revenueMapper;

	public SourceDTO sourceTOSourceDTO(Source source) {
		if (source == null) {
			return null;
		}
		SourceDTO sourceDTO = new SourceDTO();
		sourceDTO.setId(source.getId());
		sourceDTO.setAmount(source.getAmount());
		sourceDTO.setBankAccountsDTO(bankAccountsMapper
				.getBankAccountDTO(source.getBankAccounts()));
		sourceDTO.setCorrectiveDescr(source.getCorrectiveDescr());
		sourceDTO.setCorrectiveYn(source.getCorrectiveYn());
		sourceDTO.setDeferredAmount1(source.getDeferredAmount1());
		sourceDTO.setDeferredAmount10(source.getDeferredAmount10());
		sourceDTO.setDeferredAmount11(source.getDeferredAmount11());
		sourceDTO.setDeferredAmount12(source.getDeferredAmount12());
		sourceDTO.setDeferredAmount2(source.getDeferredAmount2());
		sourceDTO.setDeferredAmount3(source.getDeferredAmount3());
		sourceDTO.setDeferredAmount4(source.getDeferredAmount4());
		sourceDTO.setDeferredAmount5(source.getDeferredAmount5());
		sourceDTO.setDeferredAmount6(source.getDeferredAmount6());
		sourceDTO.setDeferredAmount7(source.getDeferredAmount7());
		sourceDTO.setDeferredAmount8(source.getDeferredAmount8());
		sourceDTO.setDeferredAmount9(source.getDeferredAmount9());
		sourceDTO.setDeferredMonth1(source.getDeferredMonth1());
		sourceDTO.setDeferredMonth10(source.getDeferredMonth10());
		sourceDTO.setDeferredMonth11(source.getDeferredMonth11());
		sourceDTO.setDeferredMonth12(source.getDeferredMonth12());
		sourceDTO.setDeferredMonth2(source.getDeferredMonth2());
		sourceDTO.setDeferredMonth3(source.getDeferredMonth3());
		sourceDTO.setDeferredMonth4(source.getDeferredMonth4());
		sourceDTO.setDeferredMonth5(source.getDeferredMonth5());
		sourceDTO.setDeferredMonth6(source.getDeferredMonth6());
		sourceDTO.setDeferredMonth7(source.getDeferredMonth7());
		sourceDTO.setDeferredMonth8(source.getDeferredMonth8());
		sourceDTO.setDeferredMonth9(source.getDeferredMonth9());
		sourceDTO.setDeferredYear1(source.getDeferredYear1());
		sourceDTO.setDeferredYear1(source.getDeferredYear1());
		sourceDTO.setDeferredYear10(source.getDeferredYear10());
		sourceDTO.setDeferredYear11(source.getDeferredYear11());
		sourceDTO.setDeferredYear12(source.getDeferredYear12());
		sourceDTO.setDeferredYear2(source.getDeferredYear2());
		sourceDTO.setDeferredYear3(source.getDeferredYear3());
		sourceDTO.setDeferredYear4(source.getDeferredYear4());
		sourceDTO.setDeferredYear5(source.getDeferredYear5());
		sourceDTO.setDeferredYear6(source.getDeferredYear6());
		sourceDTO.setDeferredYear7(source.getDeferredYear7());
		sourceDTO.setDeferredYear8(source.getDeferredYear8());
		sourceDTO.setDeferredYear9(source.getDeferredYear9());
		sourceDTO.setDeferredYn(source.getDeferredYn());
		sourceDTO.setDepositIdentifier(source.getDepositIdentifier());
		sourceDTO.setId(source.getId());
		sourceDTO.setLocationDTO(locationMapper.getLocationDTOForRevenue(source
				.getLocation()));
		sourceDTO.setSourceNotesDTO(sourceNotesSetToSourceNotesDTOSet(source
				.getNotes()));
		sourceDTO.setPci(source.getPci());
		sourceDTO.setPostedNavisionDate(source.getPostedNavisionDate());
		sourceDTO.setPostingDate(source.getPostingDate());
		sourceDTO.setReconciledDate(source.getReconciledDate());
		sourceDTO.setRevenueTypesDTO(revenueTypesMapper
				.revenueTypesToRevenueTypesDTO(source.getRevenueTypes()));
		sourceDTO.setStatus(source.getStatus());
		sourceDTO.setSubAccountCode(source.getSubAccountCode());
		sourceDTO.setSubmittedDate(source.getSubmittedDate());
		sourceDTO.setTenderTypesDTO(tenderTypesMapper
				.tenderTypesTOTenderTypesDTO(source.getTenderTypes()));
		sourceDTO.setTransactionDate(source.getTransactionDate());
		sourceDTO.setSubmittedBy(userMapper.getUserDTOId(source.getSubmittedBy()));
		sourceDTO.setPostedBy(userMapper.getUserDTOId(source.getPostedBy()));
		sourceDTO.setStatus(source.getStatus());
		sourceDTO.setPostingDate(source.getPostingDate());
		sourceDTO.setRevenueDTO(revenueMapper.getRevenueForSource(source.getRevenue()));
		return sourceDTO;
	}

	private Set<SourceNotesDTO> sourceNotesSetToSourceNotesDTOSet(
			Set<SourceNotes> notes) {

		if (notes == null) {
			return null;

		}
		Set<SourceNotesDTO> sourceNotesDTOSet = new HashSet<>();
		for (SourceNotes sourceNotes : notes) {
			sourceNotesDTOSet.add(sourceNotesMapper
					.notesToNotesDTO(sourceNotes));
		}
		return sourceNotesDTOSet;
	}

	public Source sourceDTOTOSource(SourceDTO sourceDTO) {
		if (sourceDTO == null) {
			return null;
		}
		Source source = new Source();
		source.setAmount(sourceDTO.getAmount());
		source.setBankAccounts(bankAccountsMapper
				.bankAccountsDTOToBankAccounts(sourceDTO.getBankAccountsDTO()));
		source.setCorrectiveDescr(sourceDTO.getCorrectiveDescr());
		source.setCorrectiveYn(sourceDTO.getCorrectiveYn());
		source.setDeferredAmount1(sourceDTO.getDeferredAmount1());
		source.setDeferredAmount10(sourceDTO.getDeferredAmount10());
		source.setDeferredAmount11(sourceDTO.getDeferredAmount11());
		source.setDeferredAmount12(sourceDTO.getDeferredAmount12());
		source.setDeferredAmount2(sourceDTO.getDeferredAmount2());
		source.setDeferredAmount3(sourceDTO.getDeferredAmount3());
		source.setDeferredAmount4(sourceDTO.getDeferredAmount4());
		source.setDeferredAmount5(sourceDTO.getDeferredAmount5());
		source.setDeferredAmount6(sourceDTO.getDeferredAmount6());
		source.setDeferredAmount7(sourceDTO.getDeferredAmount7());
		source.setDeferredAmount8(sourceDTO.getDeferredAmount8());
		source.setDeferredAmount9(sourceDTO.getDeferredAmount9());
		source.setDeferredMonth1(sourceDTO.getDeferredMonth1());
		source.setDeferredMonth10(sourceDTO.getDeferredMonth10());
		source.setDeferredMonth11(sourceDTO.getDeferredMonth11());
		source.setDeferredMonth12(sourceDTO.getDeferredMonth12());
		source.setDeferredMonth2(sourceDTO.getDeferredMonth2());
		source.setDeferredMonth3(sourceDTO.getDeferredMonth3());
		source.setDeferredMonth4(sourceDTO.getDeferredMonth4());
		source.setDeferredMonth5(sourceDTO.getDeferredMonth5());
		source.setDeferredMonth6(sourceDTO.getDeferredMonth6());
		source.setDeferredMonth7(sourceDTO.getDeferredMonth7());
		source.setDeferredMonth8(sourceDTO.getDeferredMonth8());
		source.setDeferredMonth9(sourceDTO.getDeferredMonth9());
		source.setDeferredYear1(sourceDTO.getDeferredYear1());
		source.setDeferredYear1(sourceDTO.getDeferredYear1());
		source.setDeferredYear10(sourceDTO.getDeferredYear10());
		source.setDeferredYear11(sourceDTO.getDeferredYear11());
		source.setDeferredYear12(sourceDTO.getDeferredYear12());
		source.setDeferredYear2(sourceDTO.getDeferredYear2());
		source.setDeferredYear3(sourceDTO.getDeferredYear3());
		source.setDeferredYear4(sourceDTO.getDeferredYear4());
		source.setDeferredYear5(sourceDTO.getDeferredYear5());
		source.setDeferredYear6(sourceDTO.getDeferredYear6());
		source.setDeferredYear7(sourceDTO.getDeferredYear7());
		source.setDeferredYear8(sourceDTO.getDeferredYear8());
		source.setDeferredYear9(sourceDTO.getDeferredYear9());
		source.setDeferredYn(sourceDTO.getDeferredYn());
		source.setDepositIdentifier(sourceDTO.getDepositIdentifier());
		source.setId(sourceDTO.getId());
		source.setLocation(locationMapper.locationDTOToLocation(sourceDTO
				.getLocationDTO()));
		source.setNotes(sourceNotesDTOSetToSourceNotesSet(sourceDTO
				.getSourceNotesDTO()));
		source.setPci(sourceDTO.isPci());
		source.setPostedNavisionDate(sourceDTO.getPostedNavisionDate());
		source.setPostingDate(sourceDTO.getPostingDate());
		source.setReconciledDate(sourceDTO.getReconciledDate());
		source.setRevenueTypes(revenueTypesMapper
				.revenueTypesDTOToRevenueTypes(sourceDTO.getRevenueTypesDTO()));
		source.setStatus(sourceDTO.getStatus());
		source.setSubAccountCode(sourceDTO.getSubAccountCode());
		source.setSubmittedDate(sourceDTO.getSubmittedDate());
		source.setTenderTypes(tenderTypesMapper
				.tenderTypesDTOTOTenderTypes(sourceDTO.getTenderTypesDTO()));
		source.setTransactionDate(sourceDTO.getTransactionDate());
		
		source.setSubmittedBy(userMapper.getUserId(sourceDTO.getSubmittedBy()));
		source.setPostedBy(userMapper.getUserId(sourceDTO.getPostedBy()));
		source.setPostingDate(sourceDTO.getPostingDate());
		source.setStatus(sourceDTO.getStatus());
		source.setRevenue(revenueMapper.getRevenueDTOForSource(sourceDTO.getRevenueDTO()));
		return source;

	}

	private Set<SourceNotes> sourceNotesDTOSetToSourceNotesSet(
			Set<SourceNotesDTO> notesDTO) {

		if (notesDTO == null) {
			return null;

		}
		Set<SourceNotes> sourceNotesSet = new HashSet<>();
		for (SourceNotesDTO sourceNotesDTO : notesDTO) {
			sourceNotesSet.add(sourceNotesMapper
					.notesDTOToNotes(sourceNotesDTO));
		}
		return sourceNotesSet;
	}

	public Set<SourceDTO> sourceSetToSourceDTOSet(Set<Source> sources) {

		if (sources == null) {
			return null;

		}
		Set<SourceDTO> sourceDTOSet = new HashSet<>();
		for (Source source : sources) {
			sourceDTOSet.add(sourceTOSourceDTO(source));
		}
		return sourceDTOSet;
	}



}
