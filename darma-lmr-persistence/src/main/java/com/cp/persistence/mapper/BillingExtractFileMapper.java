package com.cp.persistence.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BillingExtractDTO;
import com.cp.persistence.dto.BillingExtractFileDTO;
import com.cp.persistence.entity.BillingExtract;
import com.cp.persistence.entity.BillingExtractFile;

@Component
public class BillingExtractFileMapper {
	
	@Autowired
	BillingExtractMapper billingExtractMapper;

	public BillingExtractFileDTO billingExtractFileToBillingExtractFileDTO(BillingExtractFile billingExtractFile) {
		
		if(billingExtractFile == null) {
			return null;
		}
		
		BillingExtractFileDTO billingExtractFileDTO = new BillingExtractFileDTO();
		
		billingExtractFileDTO.setId(billingExtractFile.getId());
		billingExtractFileDTO.setPayPeriod(billingExtractFile.getPayPeriod());
		billingExtractFileDTO.setPayGroup(billingExtractFile.getPayGroup());
		billingExtractFileDTO.setBrand(billingExtractFile.getBrand());
		billingExtractFileDTO.setFileTimestamp(billingExtractFile.getFileTimestamp());
		billingExtractFileDTO.setInboundFileName(billingExtractFile.getInboundFileName());
		billingExtractFileDTO.setOutboundFileName(billingExtractFile.getOutboundFileName());
		billingExtractFileDTO.setIsProcessed(billingExtractFile.getIsProcessed());
		billingExtractFileDTO.setBillingExtracts(billingExtractListToBillingExtractDTOList(billingExtractFile.getBillingExtracts()));
		
		return billingExtractFileDTO;
	}
	
	public BillingExtractFile billingExtractFileDTOToBillingExtractFile(BillingExtractFileDTO billingExtractFileDTO) {
		
		if (billingExtractFileDTO == null) {
			return null;
		}
		
		BillingExtractFile billingExtractFile = new BillingExtractFile();
		
		billingExtractFile.setId(billingExtractFileDTO.getId());
		billingExtractFile.setPayPeriod(billingExtractFileDTO.getPayPeriod());
		billingExtractFile.setPayGroup(billingExtractFileDTO.getPayGroup());
		billingExtractFile.setBrand(billingExtractFileDTO.getBrand());
		billingExtractFile.setFileTimestamp(billingExtractFileDTO.getFileTimestamp());
		billingExtractFile.setInboundFileName(billingExtractFileDTO.getInboundFileName());
		billingExtractFile.setOutboundFileName(billingExtractFileDTO.getOutboundFileName());
		billingExtractFile.setIsProcessed(billingExtractFileDTO.getIsProcessed());
		billingExtractFile.setBillingExtracts(billingExtractDTOListToBillingExtractList(billingExtractFileDTO.getBilling()));

		return billingExtractFile;
	}
	
	private List<BillingExtractDTO> billingExtractListToBillingExtractDTOList(List<BillingExtract> billingExtracts) {
		if(billingExtracts == null) {
			return null;
		}
		
		List<BillingExtractDTO> billingExtractDTOs = new ArrayList<BillingExtractDTO>();
		for(BillingExtract billingExtract : billingExtracts){
			billingExtractDTOs.add(billingExtractMapper.billingExtractToBillingExtractDTO(billingExtract));
		}
		
		return billingExtractDTOs;
		
	}
	
	private List<BillingExtract> billingExtractDTOListToBillingExtractList(List<BillingExtractDTO> billingExtractDTOs) {
		if(billingExtractDTOs == null) {
			return null;
		}
		
		List<BillingExtract> billingExtracts = new ArrayList<BillingExtract>();
		for(BillingExtractDTO billingExtractDTO : billingExtractDTOs){
			billingExtracts.add(billingExtractMapper.billingExtractDTOToBillingExtract(billingExtractDTO));
		}
		
		return billingExtracts;
	}

}
