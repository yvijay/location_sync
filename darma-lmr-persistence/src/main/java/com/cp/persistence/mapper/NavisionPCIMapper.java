package com.cp.persistence.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.NavisionPCIDTO;
import com.cp.persistence.entity.NavisionPCI;

@Component
public class NavisionPCIMapper {

	public NavisionPCIDTO navisionPCIToNavisionPCIDTO(NavisionPCI navisionPCI) {
		if (navisionPCI == null) {
			return null;
		}

		NavisionPCIDTO navisionPCIDTO = new NavisionPCIDTO();

		navisionPCIDTO.setLocation_id(navisionPCI.getLocation_id());
		navisionPCIDTO.setCompany(navisionPCI.getCompany());
		navisionPCIDTO.setLocation(navisionPCI.getLocation());
		navisionPCIDTO.setGlBaseAccount(navisionPCI.getGlBaseAccount());
		navisionPCIDTO.setcOrE(navisionPCI.getcOrE());
		navisionPCIDTO.setRevenueDate(navisionPCI.getRevenueDate());
		navisionPCIDTO.setbOrG(navisionPCI.getbOrG());
		navisionPCIDTO.setDescription(trimDescription(navisionPCI
				.getDescription()));
		navisionPCIDTO.setDebits(navisionPCI.getDebits());
		navisionPCIDTO.setCredits(navisionPCI.getCredits());
		navisionPCIDTO.setPostingDate(navisionPCI.getPostingDate());
		navisionPCIDTO.setReconciledDate(navisionPCI.getReconciledDate());
		navisionPCIDTO.setDepositIdentifier(navisionPCI.getDepositIdentifier());
		navisionPCIDTO.setFilterPostingDate(navisionPCI.getFilterPostingDate());
		navisionPCIDTO.setPostedNavisionDate(navisionPCI
				.getPostedNavisionDate());
		navisionPCIDTO.setComment(navisionPCI.getComment());
		navisionPCIDTO.setSortBy(navisionPCI.getSortBy());

		return navisionPCIDTO;
	}

	/**
	 * 
	 * TO TRIM THE DESCRIPTION TO 50 characters.........!
	 * @param description
	 * @return
	 */
	private String trimDescription(String description) {
		if (description == null) {
			return null;
		}
		String desc;
		if (description.length() > 50) {
			desc = description.substring(0, 50);
		} else {
			desc = description;
		}
		return desc;
	}

	public NavisionPCI navisionPCIDTOToNavisionPCI(NavisionPCIDTO navisionPCIDTO) {
		if (navisionPCIDTO == null) {
			return null;
		}

		NavisionPCI navisionPCI = new NavisionPCI();

		navisionPCI.setLocation_id(navisionPCIDTO.getLocation_id());
		navisionPCI.setCompany(navisionPCIDTO.getCompany());
		navisionPCI.setLocation(navisionPCIDTO.getLocation());
		navisionPCI.setGlBaseAccount(navisionPCIDTO.getGlBaseAccount());
		navisionPCI.setcOrE(navisionPCIDTO.getcOrE());
		navisionPCI.setRevenueDate(navisionPCIDTO.getRevenueDate());
		navisionPCI.setbOrG(navisionPCIDTO.getbOrG());
		navisionPCI.setDescription(navisionPCIDTO.getDescription());
		navisionPCI.setDebits(navisionPCIDTO.getDebits());
		navisionPCI.setCredits(navisionPCIDTO.getCredits());
		navisionPCI.setPostingDate(navisionPCIDTO.getPostingDate());
		navisionPCI.setReconciledDate(navisionPCIDTO.getReconciledDate());
		navisionPCI.setDepositIdentifier(navisionPCIDTO.getDepositIdentifier());
		navisionPCI.setFilterPostingDate(navisionPCIDTO.getFilterPostingDate());
		navisionPCI.setPostedNavisionDate(navisionPCIDTO
				.getPostedNavisionDate());
		navisionPCI.setComment(navisionPCIDTO.getComment());
		navisionPCI.setSortBy(navisionPCIDTO.getSortBy());

		return navisionPCI;
	}

	public List<NavisionPCIDTO> navisionPCISToNavisionPCIDTOs(
			List<NavisionPCI> navisionPCIS) {
		if (navisionPCIS == null) {
			return null;
		}

		List<NavisionPCIDTO> list = new ArrayList<NavisionPCIDTO>();
		for (NavisionPCI navisionPCI : navisionPCIS) {
			list.add(navisionPCIToNavisionPCIDTO(navisionPCI));
		}

		return list;
	}
}
