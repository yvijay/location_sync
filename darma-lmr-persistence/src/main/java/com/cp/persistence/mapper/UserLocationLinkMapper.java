package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.UserLocationLinkDTO;
import com.cp.persistence.entity.UserLocationLink;

@Component
public class UserLocationLinkMapper {

	@Autowired
	LocationMapper locationMapper;
	@Autowired
	UserMapper userMapper;
   //get users only
	public UserLocationLinkDTO userLocationLinkToUserLocationLinkDTO(
			UserLocationLink usersLocationLink) {
		if (usersLocationLink == null) {
			return null;
		}
		UserLocationLinkDTO usersLocationLinkDTO = new UserLocationLinkDTO();
		usersLocationLinkDTO.setId(usersLocationLink.getId());
		usersLocationLinkDTO.setUserDTO(userMapper
				.userToUserDTO(usersLocationLink.getUser()));

	/*	usersLocationLinkDTO.setLocationDTO(locationMapper
				.locationToLocationDTO(usersLocationLink.getLocation()));*/
		usersLocationLinkDTO.setStatus(usersLocationLink.getStatus());
		return usersLocationLinkDTO;

	}
	//get locations only
	public UserLocationLinkDTO userLocationLinkToUserLocationLinkDTO1(
			UserLocationLink usersLocationLink) {
		if (usersLocationLink == null) {
			return null;
		}
		UserLocationLinkDTO usersLocationLinkDTO = new UserLocationLinkDTO();
		usersLocationLinkDTO.setId(usersLocationLink.getId());
	/*	usersLocationLinkDTO.setUserDTO(userMapper
				.userToUserDTO(usersLocationLink.getUser()));*/

		usersLocationLinkDTO.setLocationDTO(locationMapper
				.locationToLocationDTO(usersLocationLink.getLocation()));
		usersLocationLinkDTO.setStatus(usersLocationLink.getStatus());
		return usersLocationLinkDTO;

	}


	public UserLocationLink userLocationLinkDTOToUserLocationLink(
			UserLocationLinkDTO usersLocationLinkDTO) {
		if (usersLocationLinkDTO == null) {
			return null;
		}
		UserLocationLink usersLocationLink = new UserLocationLink();
		usersLocationLink.setId(usersLocationLinkDTO.getId());
		usersLocationLink.setUser(userMapper.userDTOToUser(usersLocationLinkDTO
				.getUserDTO()));
		usersLocationLink.setLocation(locationMapper
				.locationDTOToLocation(usersLocationLinkDTO.getLocationDTO()));
		usersLocationLink.setStatus(usersLocationLinkDTO.getStatus());
		return usersLocationLink;

	}

}
