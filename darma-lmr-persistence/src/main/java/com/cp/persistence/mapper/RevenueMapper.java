package com.cp.persistence.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RevenueAndSourceDTO;
import com.cp.persistence.dto.RevenueDTO;
import com.cp.persistence.dto.RevenueNotesDTO;
import com.cp.persistence.dto.SourceDTO;
import com.cp.persistence.entity.Revenue;
import com.cp.persistence.entity.RevenueNotes;
import com.cp.persistence.entity.Source;

@Component
public class RevenueMapper {

	@Autowired
	private LocationMapper locationMapper;

	@Autowired
	private RevenueNotesMapper revenueNotesMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private SourceMapper sourceMapper;

	@Autowired
	private TenderTypesMapper tenderTypesMapper;

	@Autowired
	private BankAccountsMapper bankAccountsMapper;

	public RevenueDTO revenueTORevenueDTO(Revenue revenue) {
		if (revenue == null) {
			return null;
		}
		RevenueDTO revenueDTO = new RevenueDTO();
		revenueDTO.setId(revenue.getId());
		revenueDTO.setAmount(revenue.getAmount());
		revenueDTO.setLocationDTO(locationMapper
				.getLocationDTOForRevenue(revenue.getLocation()));
		revenueDTO.setBankAccountsDTO(bankAccountsMapper
				.getBankAccountDTO(revenue.getBankAccounts()));
		revenueDTO.setRevenueDate(revenue.getRevenueDate());
		revenueDTO.setStatus(revenue.getStatus());
		revenueDTO.setSubmittedBy(userMapper.getUserDTOId(revenue
				.getSubmittedBy()));
		revenueDTO.setSubmittedDate(revenue.getSubmittedDate());
		revenueDTO.setDepositIdentifier(revenue.getDepositIdentifier());
		revenueDTO
				.setSourcesDTO(sourceListToSourceListDTO(revenue.getSources()));
		revenueDTO.setNumberOfDeposits(revenue.getNumberOfDeposits());
		revenueDTO.setTenderTypesDTO(tenderTypesMapper
				.tenderTypesTOTenderTypesDTO(revenue.getTenderTypes()));
		revenueDTO.setPostingDate(revenue.getPostingDate());
		revenueDTO.setPostedBy(userMapper.getUserDTOId(revenue.getPostedBy()));
		revenueDTO.setReconciledDate(revenue.getReconciledDate());
		revenueDTO.setMarkForCorrection(revenue.getMarkForCorrection());
		return revenueDTO;
	}

	private List<SourceDTO> sourceListToSourceListDTO(List<Source> sources) {
		if (sources == null) {
			return null;

		}
		List<SourceDTO> SourceDTOs = new ArrayList<>();
		for (Source source : sources) {
			SourceDTOs.add(sourceMapper.sourceTOSourceDTO(source));

		}
		return SourceDTOs;
	}

	private Set<RevenueNotesDTO> revenueSetToRevenueDTOSet(
			Set<RevenueNotes> revenuenotes) {
		if (revenuenotes == null) {
			return null;

		}
		Set<RevenueNotesDTO> revenueNotesDTOSet = new HashSet<>();
		for (RevenueNotes revenueNotes : revenuenotes) {
			revenueNotesDTOSet.add(revenueNotesMapper
					.notesToNotesDTO(revenueNotes));
		}
		return revenueNotesDTOSet;
	}

	public Revenue revenueDTOTORevenue(RevenueDTO revenueDTO, Boolean flag) {
		if (revenueDTO == null) {
			return null;
		}
		Revenue revenue = new Revenue();
		revenue.setId(revenueDTO.getId());
		revenue.setAmount(revenueDTO.getAmount());
		revenue.setId(revenueDTO.getId());
		revenue.setLocation(locationMapper.locationDTOToLocation(revenueDTO
				.getLocationDTO()));
		revenue.setBankAccounts(bankAccountsMapper
				.bankAccountsDTOToBankAccounts(revenueDTO.getBankAccountsDTO()));
		revenue.setNotes(revenueSetDTOToRevenueSet(revenueDTO.getNotesDTO()));
		revenue.setNumberOfDeposits(revenueDTO.getSourcesDTO().size());
		revenue.setRevenueDate(revenueDTO.getRevenueDate());
		revenue.setStatus(revenueDTO.getStatus());
		revenue.setSubmittedBy(userMapper.getUserId(revenueDTO.getSubmittedBy()));
		revenue.setSubmittedDate(revenueDTO.getSubmittedDate());
		revenue.getSources().addAll(
				sourceListDTOToSourceList(revenueDTO.getSourcesDTO(), revenue));
		revenue.setDepositIdentifier(revenueDTO.getDepositIdentifier());
		revenue.setNumberOfDeposits(revenueDTO.getSourcesDTO().size());
		revenue.setTenderTypes(tenderTypesMapper
				.tenderTypesDTOTOTenderTypes(revenueDTO.getTenderTypesDTO()));
		revenue.setPostingDate(revenueDTO.getPostingDate());
		revenue.setPostedBy(userMapper.getUserId(revenueDTO.getPostedBy()));
		revenue.setReconciledDate(revenueDTO.getReconciledDate());

		if ((revenueDTO.getMarkForCorrection() != null) && (flag)) {

			revenue.setMarkForCorrection(revenueDTO.getMarkForCorrection());

		} else {
			revenue.setMarkForCorrection(false);
		}

		return revenue;

	}

	private Set<RevenueNotes> revenueSetDTOToRevenueSet(
			Set<RevenueNotesDTO> notesDTO) {
		if (notesDTO == null) {
			return null;

		}
		Set<RevenueNotes> revenueNotesSet = new HashSet<>();
		for (RevenueNotesDTO revenueNotesDTO : notesDTO) {
			revenueNotesSet.add(revenueNotesMapper
					.notesDTOToNotes(revenueNotesDTO));
		}
		return revenueNotesSet;
	}

	private List<Source> sourceListDTOToSourceList(List<SourceDTO> sourceDTOs,
			Revenue revenue) {
		if (sourceDTOs == null) {
			return null;

		}
		List<Source> sources = new ArrayList<>();
		for (SourceDTO sourceDTO : sourceDTOs) {
			Source source = sourceMapper.sourceDTOTOSource(sourceDTO);
			source.setRevenue(revenue);
			sources.add(source);

		}
		return sources;
	}

	public RevenueAndSourceDTO revenueAndSourceToRevenueSourceDTO(
			Revenue revenue, Set<Source> listOfSources) {
		if (revenue == null) {
			return null;

		}
		RevenueAndSourceDTO revenueAndSourceDTO = new RevenueAndSourceDTO();
		revenueAndSourceDTO.setId(revenue.getId());
		revenueAndSourceDTO.setAmount(revenue.getAmount());
		revenueAndSourceDTO.setNumberOfDeposits(revenue.getNumberOfDeposits());
		revenueAndSourceDTO.setRevenueDate(revenue.getRevenueDate());
		revenueAndSourceDTO.setStatus(revenue.getStatus());
		revenueAndSourceDTO.setSubmittedBy(userMapper.userToUserDTO(revenue
				.getSubmittedBy()));
		revenueAndSourceDTO.setSubmittedDate(revenue.getSubmittedDate());
		revenueAndSourceDTO.setLocationDTO(locationMapper
				.getLocationDTOForRevenue(revenue.getLocation()));
		revenueAndSourceDTO.setNotesDTO(revenueSetToRevenueDTOSet(revenue
				.getNotes()));

		revenueAndSourceDTO.setSources(sourceMapper
				.sourceSetToSourceDTOSet(listOfSources));

		return revenueAndSourceDTO;

	}

	public RevenueDTO getRevenueForSource(Revenue revenue) {
		if (revenue == null) {
			return null;

		}
		RevenueDTO revenueDTO = new RevenueDTO();
		revenueDTO.setId(revenue.getId());

		return revenueDTO;
	}

	public Revenue getRevenueDTOForSource(RevenueDTO revenueDTO) {
		if (revenueDTO == null) {
			return null;

		}
		Revenue revenue = new Revenue();
		revenue.setId(revenueDTO.getId());

		return revenue;
	}

	public RevenueDTO getRevenueDTOForSourceSave(RevenueDTO revenueDTO) {
		if (revenueDTO == null) {
			return null;

		}
		RevenueDTO revenue = new RevenueDTO();
		revenue.setId(revenueDTO.getId());

		return revenue;
	}

	public RevenueDTO getAllRevenuesWithOutSources(Revenue revenue) {
		if (revenue == null) {
			return null;
		}
		RevenueDTO revenueDTO = new RevenueDTO();
		revenueDTO.setId(revenue.getId());
		revenueDTO.setAmount(revenue.getAmount());
		revenueDTO.setLocationDTO(locationMapper
				.getLocationDTOForRevenue(revenue.getLocation()));
		revenueDTO.setBankAccountsDTO(bankAccountsMapper
				.getBankAccountDTO(revenue.getBankAccounts()));
		revenueDTO.setRevenueDate(revenue.getRevenueDate());
		revenueDTO.setStatus(revenue.getStatus());
		revenueDTO.setSubmittedBy(userMapper.getUserDTOForSource(revenue
				.getSubmittedBy()));
		revenueDTO.setSubmittedDate(revenue.getSubmittedDate());
		revenueDTO.setDepositIdentifier(revenue.getDepositIdentifier());
		revenueDTO.setNumberOfDeposits(revenue.getNumberOfDeposits());
		revenueDTO.setTenderTypesDTO(tenderTypesMapper
				.tenderTypesTOTenderTypesDTO(revenue.getTenderTypes()));
		revenueDTO.setPostingDate(revenue.getPostingDate());
		revenueDTO.setPostedBy(userMapper.getUserDTOForSource(revenue
				.getPostedBy()));
		revenueDTO.setReconciledDate(revenue.getReconciledDate());
		revenueDTO.setMarkForCorrection(revenue.getMarkForCorrection());
		return revenueDTO;
	}

}
