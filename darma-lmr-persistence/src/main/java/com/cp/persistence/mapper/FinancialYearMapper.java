package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.FinancialYearDTO;
import com.cp.persistence.entity.FinancialYear;

@Component
public class FinancialYearMapper {

	@Autowired
	private FinancialServicesMapper financialServicesMapper;

	public FinancialYearDTO financialYearToFinancialYearDTO(
			FinancialYear financialYear) {
		if (financialYear == null) {
			return null;
		}
		FinancialYearDTO financialYearDTO = new FinancialYearDTO();

		financialYearDTO.setId(financialYear.getId());
		financialYearDTO.setNotes(financialYear.getNotes());
		financialYearDTO.setOverallPercent(financialYear.getOverallPercent());
		financialYearDTO.setYear(financialYear.getYear());
		financialYearDTO.setYearAmount(financialYear.getYearAmount());
		financialYearDTO.setYearPercent(financialYear.getYearPercent());
		financialYearDTO.setFinancialServicesDTO(financialServicesMapper
				.financialServicesToFinancialServicesDTO((financialYear
						.getFinancialServices())));
		return financialYearDTO;

	}

	public FinancialYear financialYearDTOToFinancialYear(
			FinancialYearDTO financialYearDTO) {
		if (financialYearDTO == null) {
			return null;
		}
		FinancialYear financialYear = new FinancialYear();

		financialYear.setId(financialYearDTO.getId());
		financialYear.setNotes(financialYearDTO.getNotes());
		financialYear.setOverallPercent(financialYearDTO.getOverallPercent());
		financialYear.setYear(financialYearDTO.getYear());
		financialYear.setYearAmount(financialYearDTO.getYearAmount());
		financialYear.setYearPercent(financialYearDTO.getYearPercent());
		financialYear.setFinancialServices(financialServicesMapper
				.financialServicesDTOToFinancialServices((financialYearDTO
						.getFinancialServicesDTO())));
		return financialYear;
	}

}
