package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BurdenInfoDTO;
import com.cp.persistence.entity.BurdenInfo;

@Component
public class BurdenInfoMapper {

	@Autowired
	private LocationMapper locationMapper;

	@Autowired
	private ExclusionBurdensMapper exclusionBurdensMapper;

	public BurdenInfoDTO burdenInfoToBurdenInfoDTO(BurdenInfo burdensInfo) {
		if (burdensInfo == null)
			return null;

		BurdenInfoDTO burdenInfoDTO = new BurdenInfoDTO();

		burdenInfoDTO.setId(burdensInfo.getId());
		burdenInfoDTO.setExclusion(burdensInfo.getExclusion());
		burdenInfoDTO.setUseActuals(burdensInfo.getUseActuals());
	/*	burdenInfoDTO.setLocationDTO(locationMapper
				.locationToLocationDTO(burdensInfo.getLocation()));*/
		burdenInfoDTO.setStatus(burdensInfo.getStatus());

		return burdenInfoDTO;
	}

	public BurdenInfo burdenInfoDTOToBurdenInfo(BurdenInfoDTO burdenInfoDTO) {

		if (burdenInfoDTO == null) {
			return null;
		}

		BurdenInfo burdenInfo = new BurdenInfo();

		burdenInfo.setId(burdenInfoDTO.getId());
		burdenInfo.setExclusion(burdenInfoDTO.getExclusion());
		burdenInfo.setUseActuals(burdenInfoDTO.getUseActuals());
	/*	burdenInfo.setLocation(locationMapper
				.locationDTOToLocation(burdenInfoDTO.getLocationDTO()));*/
		burdenInfo.setStatus(burdenInfoDTO.getStatus());

		return burdenInfo;
	}

	public BurdenInfoDTO burdenInfoToBurdenInfoDTO1(BurdenInfo burdensInfo) {
		if (burdensInfo == null)
			return null;

		BurdenInfoDTO burdenInfoDTO = new BurdenInfoDTO();

		burdenInfoDTO.setId(burdensInfo.getId());
		burdenInfoDTO.setExclusion(burdensInfo.getExclusion());
		burdenInfoDTO.setUseActuals(burdensInfo.getUseActuals());
		/*burdenInfoDTO.setLocationDTO(locationMapper
				.locationToLocationDTO(burdensInfo.getLocation()));*/
		burdenInfoDTO.setStatus(burdensInfo.getStatus());

		return burdenInfoDTO;
	}
	
}
