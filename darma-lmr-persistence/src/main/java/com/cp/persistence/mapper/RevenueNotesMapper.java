package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RevenueNotesDTO;
import com.cp.persistence.entity.RevenueNotes;


@Component
public class RevenueNotesMapper {

	public RevenueNotesDTO notesToNotesDTO(RevenueNotes notes){
		if(notes==null)
			return null;
		
		RevenueNotesDTO notesDTO=new RevenueNotesDTO();
		
		notesDTO.setId(notes.getId());
		notesDTO.setNoteHeading(notes.getNoteHeading());
		notesDTO.setNotes(notes.getNotes());
		notesDTO.setStatus(notes.getStatus());
		
		
		return notesDTO;
	}
	
	public RevenueNotes notesDTOToNotes(RevenueNotesDTO notesDTO){
		
		if(notesDTO==null)
		{
			return null;
		}
		
		RevenueNotes notes=new RevenueNotes();
		
		notes.setId(notesDTO.getId());
		notes.setNoteHeading(notesDTO.getNoteHeading());
		notes.setNotes(notesDTO.getNotes());
		notes.setStatus(notesDTO.getStatus());
		return notes;
	}
	
}
