package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationMoreInfoDTO;
import com.cp.persistence.entity.LocationMoreInfo;

@Component
public class LocationMoreInfoMapper {

	public LocationMoreInfo locationMoreInfoDTOToLocationMoreInfo(
			LocationMoreInfoDTO locationMoreInfoDTO) {
		if (locationMoreInfoDTO == null) {
			return null;
		}

		LocationMoreInfo locationMoreInfo = new LocationMoreInfo();
		locationMoreInfo.setActionDate(locationMoreInfoDTO.getActionDate());
		locationMoreInfo.setContractedAnnualHours(locationMoreInfoDTO
				.getContractedAnnualHours());
		locationMoreInfo.setContractHours(locationMoreInfoDTO
				.getContractHours());
		locationMoreInfo.setContractPeriodFrom(locationMoreInfoDTO
				.getContractPeriodFrom());
		locationMoreInfo.setContractPeriodTo(locationMoreInfoDTO
				.getContractPeriodTo());
		locationMoreInfo.setExpires(locationMoreInfoDTO.getExpires());
		locationMoreInfo.setId(locationMoreInfoDTO.getId());
		locationMoreInfo.setLastDateContractRevised(locationMoreInfoDTO
				.getLastDateContractRevised());
		locationMoreInfo.setManagementGroup(locationMoreInfoDTO
				.getManagementGroup());
		locationMoreInfo.setOldLocationID(locationMoreInfoDTO
				.getOldLocationID());
		locationMoreInfo.setpONumber(locationMoreInfoDTO.getpONumber());
		locationMoreInfo.setRevisionDate(locationMoreInfoDTO.getRevisionDate());
		locationMoreInfo.setrVP(locationMoreInfoDTO.getrVP());
		locationMoreInfo.setSubsidyMonthlyRate(locationMoreInfoDTO
				.getSubsidyMonthlyRate());
		locationMoreInfo.setsVP(locationMoreInfoDTO.getsVP());
		locationMoreInfo.setVertical(locationMoreInfoDTO.getVertical());
		locationMoreInfo.setOpenDate(locationMoreInfoDTO.getOpenDate());
		locationMoreInfo.setCloseDate(locationMoreInfoDTO.getCloseDate());
		locationMoreInfo.setStatus(locationMoreInfoDTO.getStatus());
		return locationMoreInfo;

	}

	public LocationMoreInfoDTO locationMoreInfoToLocationMoreInfoDTO(
			LocationMoreInfo locationMoreInfo) {
		if (locationMoreInfo == null) {
			return null;
		}

		LocationMoreInfoDTO locationMoreInfoDTO = new LocationMoreInfoDTO();
		locationMoreInfoDTO.setActionDate(locationMoreInfo.getActionDate());
		locationMoreInfoDTO.setContractedAnnualHours(locationMoreInfo
				.getContractedAnnualHours());
		locationMoreInfoDTO.setContractHours(locationMoreInfo
				.getContractHours());
		locationMoreInfoDTO.setContractPeriodFrom(locationMoreInfo
				.getContractPeriodFrom());
		locationMoreInfoDTO.setContractPeriodTo(locationMoreInfo
				.getContractPeriodTo());
		locationMoreInfoDTO.setExpires(locationMoreInfo.getExpires());
		locationMoreInfoDTO.setId(locationMoreInfo.getId());
		locationMoreInfoDTO.setLastDateContractRevised(locationMoreInfo
				.getLastDateContractRevised());
		locationMoreInfoDTO.setManagementGroup(locationMoreInfo
				.getManagementGroup());
		locationMoreInfoDTO.setOldLocationID(locationMoreInfo
				.getOldLocationID());
		locationMoreInfoDTO.setpONumber(locationMoreInfo.getpONumber());
		locationMoreInfoDTO.setRevisionDate(locationMoreInfo.getRevisionDate());
		locationMoreInfoDTO.setrVP(locationMoreInfo.getrVP());
		locationMoreInfoDTO.setSubsidyMonthlyRate(locationMoreInfo
				.getSubsidyMonthlyRate());
		locationMoreInfoDTO.setsVP(locationMoreInfo.getsVP());
		locationMoreInfoDTO.setVertical(locationMoreInfo.getVertical());
		locationMoreInfoDTO.setOpenDate(locationMoreInfo.getOpenDate());
		locationMoreInfoDTO.setCloseDate(locationMoreInfo.getCloseDate());
		locationMoreInfoDTO.setStatus(locationMoreInfo.getStatus());
		return locationMoreInfoDTO;

	}
}
