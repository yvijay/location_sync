package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RoleDTO;
import com.cp.persistence.dto.UserDTO;
import com.cp.persistence.dto.UserLocationLinkDTO;
import com.cp.persistence.entity.Role;
import com.cp.persistence.entity.User;
import com.cp.persistence.entity.UserLocationLink;
import com.cp.persistence.repository.UserRepository;

@Component
public class UserMapper {

	@Autowired
	private RoleMapper roleMapper;

	@Autowired
	private BrandMapper brandMapper;

	@Autowired
	private LocationMapper locationMapper;
	@Autowired
	private UserLocationLinkMapper userLocationLinkMapper;
	
	@Autowired
	private UserRepository userRepository;

	public UserDTO userToUserDTO(User user) {
		if (user == null) {
			return null;
		}
		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setUserName(user.getUserName());
		userDTO.setEmailAddress(user.getEmailAddress());
		userDTO.setPhone(user.getPhone());
		userDTO.setStatus(user.getStatus());
		userDTO.setUsertype(user.getUsertype());
		userDTO.setDisplayName(user.getDisplayName());
		userDTO.setBrandCode(user.getBrandCode());
		userDTO.setRolesDTO(rolesSetToRolesDTOSet(user.getRoles()));
		userDTO.setUsersLocationLinkDTO(userLocationLinkSetToUserLocationLinkDTOSet(user.getUsersLocationLink()));

		return userDTO;

	}
	//get All users without userLocationLnk
	public UserDTO userToUserDTOWithOutUserLocLnk(User user) {
		if (user == null) {
			return null;
		}
		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setUserName(user.getUserName());
		userDTO.setEmailAddress(user.getEmailAddress());
		userDTO.setPhone(user.getPhone());
		userDTO.setStatus(user.getStatus());
		userDTO.setUsertype(user.getUsertype());
		userDTO.setDisplayName(user.getDisplayName());
		userDTO.setBrandCode(user.getBrandCode());
		userDTO.setRolesDTO(rolesSetToRolesDTOSet(user.getRoles()));

		return userDTO;

	}

	private Set<RoleDTO> rolesSetToRolesDTOSet(Set<Role> roles) {
		if (roles == null) {
			return null;

		}
		Set<RoleDTO> roleDTOSet = new HashSet<>();
		for (Role role : roles) {
			roleDTOSet.add(roleMapper.rolesToRolesDTO(role));
		}
		return roleDTOSet;

	}
	
	
	private Set<UserLocationLinkDTO> userLocationLinkSetToUserLocationLinkDTOSet(Set<UserLocationLink> userLocationLink) {
		if (userLocationLink == null) {
			return null;

		}
		Set<UserLocationLinkDTO> roleDTOSet = new HashSet<>();
		for (UserLocationLink role : userLocationLink) {
			roleDTOSet.add(userLocationLinkMapper.userLocationLinkToUserLocationLinkDTO1(role));
		}
		return roleDTOSet;

	}
	
	private Set<UserLocationLink> userLocationLinkDTOSetToUserLocationLinkDTO(Set<UserLocationLinkDTO> userLocationLinkDTO) {
		if (userLocationLinkDTO == null) {
			return null;

		}
		Set<UserLocationLink> roleDTOSet = new HashSet<>();
		for (UserLocationLinkDTO role : userLocationLinkDTO) {
			roleDTOSet.add(userLocationLinkMapper.userLocationLinkDTOToUserLocationLink(role));
		}
		return roleDTOSet;

	}

	public User userDTOToUser(UserDTO userDTO) {
		if (userDTO == null) {
			return null;
		}
		User user = new User();

		user.setId(userDTO.getId());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmailAddress(userDTO.getEmailAddress());
		user.setPhone(userDTO.getPhone());
		user.setUserName(userDTO.getUserName());
		user.setStatus(userDTO.getStatus());
		user.setUsertype(userDTO.getUsertype());
		user.setBrandCode(userDTO.getBrandCode());
		user.setRoles(rolesDTOSetToRolesSet(userDTO.getRolesDTO()));
		user.setBrand(brandMapper.brandDTOToBrand(userDTO.getBrandDTO()));
		user.setUsersLocationLink(userLocationLinkDTOSetToUserLocationLinkDTO(userDTO.getUsersLocationLinkDTO()));
		return user;

	}

	private Set<Role> rolesDTOSetToRolesSet(Set<RoleDTO> set) {
		if (set == null) {
			return null;

		}
		Set<Role> roleSet = new HashSet<>();
		for (RoleDTO roleDTO : set) {
			roleSet.add(roleMapper.rolesDTOToRoles(roleDTO));
		}
		return roleSet;

	}

	public UserDTO getUserDTOForSource(User user) {
		if (user == null) {
			return null;
		}
		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setUserName(user.getUserName());
		userDTO.setEmailAddress(user.getEmailAddress());
		userDTO.setPhone(user.getPhone());
		userDTO.setStatus(user.getStatus());
		userDTO.setUsertype(user.getUsertype());
		userDTO.setBrandCode(user.getBrandCode());
		return userDTO;

	}

	public User getUserId(UserDTO userDTO) {
		if (userDTO == null) {
			return null;
		}
		User user = new User();

		user.setId(userDTO.getId());
		return user;
	}
	
	public UserDTO getUserDTOId(User user) {
		if (user == null) {
			return null;
		}
		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setDisplayName(user.getDisplayName());
		userDTO.setUserName(user.getUserName());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		return userDTO;
	}

	public UserDTO getUserDTO(String submittedBy) {
		if(submittedBy == null)
		return null;
		else
			return userToUserDTO(userRepository.findOne(submittedBy));
	}
	
	
}
