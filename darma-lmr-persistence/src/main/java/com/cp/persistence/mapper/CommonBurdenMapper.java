package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.CommonBurdenDTO;
import com.cp.persistence.entity.CommonBurden;

@Component
public class CommonBurdenMapper {

	@Autowired
	private BrandMapper brandMapper;

	public CommonBurden burdensDTOToCommonBurden(CommonBurdenDTO commonsBurdensDTO) {
		if (commonsBurdensDTO == null)
			return null;

		CommonBurden commonBurdens = new CommonBurden();

		commonBurdens.setcEorC(commonsBurdensDTO.getcEorC());
		commonBurdens.setcGL(commonsBurdensDTO.getcGL());
		commonBurdens.setcOffsetEorC(commonsBurdensDTO.getcOffsetEorC());
		commonBurdens.setcPercentage(commonsBurdensDTO.getcPercentage());
		commonBurdens.setdEorC(commonsBurdensDTO.getdEorC());
		commonBurdens.setdGL(commonsBurdensDTO.getdGL());
		commonBurdens.setdOffsetEorC(commonsBurdensDTO.getdOffsetEorC());
		commonBurdens.setdPercentage(commonsBurdensDTO.getdPercentage());
		commonBurdens.setId(commonsBurdensDTO.getId());
		commonBurdens.setIncludingTips(commonsBurdensDTO.getIncludingTips());
		commonBurdens.setJobDescription(commonsBurdensDTO.getJobDescription());
		commonBurdens.setOffsettingAccount(commonsBurdensDTO.getOffsettingAccount());
		commonBurdens.setType(commonsBurdensDTO.getType());
		commonBurdens.setBrand(brandMapper.brandDTOToBrand(commonsBurdensDTO.getBrandDTO()));
		commonBurdens.setStatus(commonsBurdensDTO.getStatus());
		return commonBurdens;
	}

	public CommonBurdenDTO burdensToCommonBurdenDTO(CommonBurden commonBurdens) {
		if (commonBurdens == null)
			return null;

		CommonBurdenDTO commonBurdensDTO = new CommonBurdenDTO();

		commonBurdensDTO.setcEorC(commonBurdens.getcEorC());
		commonBurdensDTO.setcGL(commonBurdens.getcGL());
		commonBurdensDTO.setcOffsetEorC(commonBurdens.getcOffsetEorC());
		commonBurdensDTO.setcPercentage(commonBurdens.getcPercentage());
		commonBurdensDTO.setdEorC(commonBurdens.getdEorC());
		commonBurdensDTO.setdGL(commonBurdens.getdGL());
		commonBurdensDTO.setdOffsetEorC(commonBurdens.getdOffsetEorC());
		commonBurdensDTO.setdPercentage(commonBurdens.getdPercentage());
		commonBurdensDTO.setId(commonBurdens.getId());
		commonBurdensDTO.setIncludingTips(commonBurdens.getIncludingTips());
		commonBurdensDTO.setJobDescription(commonBurdens.getJobDescription());
		commonBurdensDTO.setOffsettingAccount(commonBurdens.getOffsettingAccount());
		commonBurdensDTO.setType(commonBurdens.getType());
		commonBurdensDTO.setBrandDTO(brandMapper.brandToBrandDTO(commonBurdens.getBrand()));
		commonBurdensDTO.setStatus(commonBurdens.getStatus());
		return commonBurdensDTO;
	}

	// CommonBurden without location

	public CommonBurdenDTO burdensToCommonBurdenDTOWithOutLocation(CommonBurden burdens) {
		if (burdens == null)
			return null;

		CommonBurdenDTO commonBurdensDTO = new CommonBurdenDTO();

		commonBurdensDTO.setcEorC(burdens.getcEorC());
		commonBurdensDTO.setcGL(burdens.getcGL());
		commonBurdensDTO.setcOffsetEorC(burdens.getcOffsetEorC());
		commonBurdensDTO.setcPercentage(burdens.getcPercentage());
		commonBurdensDTO.setdEorC(burdens.getdEorC());
		commonBurdensDTO.setdGL(burdens.getdGL());
		commonBurdensDTO.setdOffsetEorC(burdens.getdOffsetEorC());
		commonBurdensDTO.setdPercentage(burdens.getdPercentage());
		commonBurdensDTO.setId(burdens.getId());
		commonBurdensDTO.setIncludingTips(burdens.getIncludingTips());
		commonBurdensDTO.setJobDescription(burdens.getJobDescription());
		commonBurdensDTO.setOffsettingAccount(burdens.getOffsettingAccount());
		commonBurdensDTO.setType(burdens.getType());
		commonBurdensDTO.setBrandDTO(brandMapper.brandToBrandDTO(burdens.getBrand()));
		// burdensDTO.setLocationDTO(locationMapper.locationToLocationDTO(burdens.getLocation()));

		return commonBurdensDTO;
	}

}
