package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RoleDTO;
import com.cp.persistence.entity.Role;

@Component
public class RoleMapper {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PermissionMapper permissionMapper;

	public RoleDTO rolesToRolesDTO(Role role) {
		if (role == null) {
			return null;
		}
		RoleDTO rolesDTO = new RoleDTO();
		rolesDTO.setId(role.getId());
		rolesDTO.setName(role.getName());
		rolesDTO.setCode(role.getCode());
		rolesDTO.setDescription(role.getDescription());
	/*	rolesDTO.setPermissionsDTO(permissionSetToPermissionDTOSet(role
				.getPermissions()));*/
		rolesDTO.setUsertype(role.getUsertype());
		// rolesDTO.setUsers(userSetToUserDTOSet(role.getUsers()));

		return rolesDTO;

	}

	public Role rolesDTOToRoles(RoleDTO roleDTO) {
		if (roleDTO == null) {
			return null;
		}
		Role roles = new Role();
		roles.setId(roleDTO.getId());
		roles.setName(roleDTO.getName());
		roles.setCode(roleDTO.getCode());
		roles.setDescription(roleDTO.getDescription());
		/*roles.setPermissions(permissionDTOSetToPermissionSet(roleDTO
				.getPermissionsDTO()));*/
		roles.setUsertype(roleDTO.getUsertype());
		// roles.setUsers(userDTOSetToUserSet(roleDTO.getUsers()));

		return roles;

	}

}
