package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.SourceNotesDTO;
import com.cp.persistence.entity.SourceNotes;


@Component
public class SourceNotesMapper {

	public SourceNotesDTO notesToNotesDTO(SourceNotes notes){
		if(notes==null)
			return null;
		
		SourceNotesDTO notesDTO=new SourceNotesDTO();
		
		notesDTO.setId(notes.getId());
		notesDTO.setNoteHeading(notes.getNoteHeading());
		notesDTO.setNotes(notes.getNotes());
		notesDTO.setStatus(notes.getStatus());
		
		
		return notesDTO;
	}
	
	public SourceNotes notesDTOToNotes(SourceNotesDTO notesDTO){
		
		if(notesDTO==null)
		{
			return null;
		}
		
		SourceNotes notes=new SourceNotes();
		
		notes.setId(notesDTO.getId());
		notes.setNoteHeading(notesDTO.getNoteHeading());
		notes.setNotes(notesDTO.getNotes());
		notes.setStatus(notesDTO.getStatus());
		return notes;
	}
	
}
