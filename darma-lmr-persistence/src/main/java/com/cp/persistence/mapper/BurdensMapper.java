package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BurdensDTO;
import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.entity.Burdens;
import com.cp.persistence.entity.Location;

@Component
public class BurdensMapper {

	@Autowired
	private LocationMapper locationMapper;

	public Burdens burdensDTOToBurdens(BurdensDTO burdensDTO) {
		if (burdensDTO == null)
			return null;

		Burdens burdens = new Burdens();

		burdens.setcEorC(burdensDTO.getcEorC());
		burdens.setcGL(burdensDTO.getcGL());
		burdens.setcOffsetEorC(burdensDTO.getcOffsetEorC());
		burdens.setcPercentage(burdensDTO.getcPercentage());
		burdens.setdEorC(burdensDTO.getdEorC());
		burdens.setdGL(burdensDTO.getdGL());
		burdens.setdOffsetEorC(burdensDTO.getdOffsetEorC());
		burdens.setdPercentage(burdensDTO.getdPercentage());
		burdens.setId(burdensDTO.getId());
		burdens.setIncludingTips(burdensDTO.getIncludingTips());
		burdens.setJobDescription(burdensDTO.getJobDescription());
		burdens.setOffsettingAccount(burdensDTO.getOffsettingAccount());
		burdens.setType(burdensDTO.getType());
		burdens.setLocation(getLocation(burdensDTO.getLocationDTO()));

		burdens.setStatus(burdensDTO.getStatus());
		return burdens;
	}

	public BurdensDTO burdensToBurdensDTO(Burdens burdens) {
		if (burdens == null)
			return null;

		BurdensDTO burdensDTO = new BurdensDTO();

		burdensDTO.setcEorC(burdens.getcEorC());
		burdensDTO.setcGL(burdens.getcGL());
		burdensDTO.setcOffsetEorC(burdens.getcOffsetEorC());
		burdensDTO.setcPercentage(burdens.getcPercentage());
		burdensDTO.setdEorC(burdens.getdEorC());
		burdensDTO.setdGL(burdens.getdGL());
		burdensDTO.setdOffsetEorC(burdens.getdOffsetEorC());
		burdensDTO.setdPercentage(burdens.getdPercentage());
		burdensDTO.setId(burdens.getId());
		burdensDTO.setIncludingTips(burdens.getIncludingTips());
		burdensDTO.setJobDescription(burdens.getJobDescription());
		burdensDTO.setOffsettingAccount(burdens.getOffsettingAccount());
		burdensDTO.setType(burdens.getType());
		// burdensDTO.setLocationDTO(getLocationDTO(burdens.getLocation()));
		burdensDTO.setStatus(burdens.getStatus());
		return burdensDTO;
	}

	// Burdens without location

	public BurdensDTO burdensToBurdensDTOWithOutLocation(Burdens burdens) {
		if (burdens == null)
			return null;

		BurdensDTO burdensDTO = new BurdensDTO();

		burdensDTO.setcEorC(burdens.getcEorC());
		burdensDTO.setcGL(burdens.getcGL());
		burdensDTO.setcOffsetEorC(burdens.getcOffsetEorC());
		burdensDTO.setcPercentage(burdens.getcPercentage());
		burdensDTO.setdEorC(burdens.getdEorC());
		burdensDTO.setdGL(burdens.getdGL());
		burdensDTO.setdOffsetEorC(burdens.getdOffsetEorC());
		burdensDTO.setdPercentage(burdens.getdPercentage());
		burdensDTO.setId(burdens.getId());
		burdensDTO.setIncludingTips(burdens.getIncludingTips());
		burdensDTO.setJobDescription(burdens.getJobDescription());
		burdensDTO.setOffsettingAccount(burdens.getOffsettingAccount());
		burdensDTO.setType(burdens.getType());
		// burdensDTO.setLocationDTO(getLocationDTO(burdens.getLocation()));

		return burdensDTO;
	}

	private Location getLocation(LocationDTO locationDTO) {
		if (locationDTO == null) {
			return null;
		}
		Location location = new Location();
		location.setId(locationDTO.getId());
		return location;
	}

}
