package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BankAccountsLocationLinkDTO;
import com.cp.persistence.dto.BurdenInfoDTO;
import com.cp.persistence.dto.BurdensDTO;
import com.cp.persistence.dto.ExclusionBurdensDTO;
import com.cp.persistence.dto.LocationBurdensDTO;
import com.cp.persistence.dto.LocationContactDTO;
import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.LocationLnkDTO;
import com.cp.persistence.dto.RevenueTypesLocationLinkDTO;
import com.cp.persistence.dto.TenderTypesLocationLinkDTO;
import com.cp.persistence.dto.vwAllocationsDTO;
import com.cp.persistence.entity.BankAccountsLocationLink;
import com.cp.persistence.entity.Burdens;
import com.cp.persistence.entity.ExclusionBurdens;
import com.cp.persistence.entity.Location;
import com.cp.persistence.entity.LocationContact;
import com.cp.persistence.entity.RevenueTypesLocationLink;
import com.cp.persistence.entity.TenderTypesLocationLink;
import com.cp.persistence.entity.vwAllocations;
import com.cp.persistence.repository.LocationRepository;

@Component
public class LocationMapper {

	@Autowired
	private BurdensMapper burdensMapper;
	@Autowired
	private vwAllocationsMapper vwAllocationsMapper;
	@Autowired
	private ExclusionBurdensMapper exclusionBurdensMapper;

	@Autowired
	private BankAccountsMapper bankAccountsMapper;
	
	@Autowired
	private BankAccountsLocationLinkMapper bankAccountLocationLnkMapper;

	@Autowired
	private LocationContactMapper locationContactMapper;

	@Autowired
	private LocationMoreInfoMapper locationMoreInfoMapper;

	@Autowired
	private LocationNotesMapper locationNotesMapper;

	@Autowired
	private PositionsAndBillingRatesMapper rolesAndBillingRatesMapper;

	@Autowired
	private RevenueMapper revenueMapper;

	@Autowired
	private RevenueTypesMapper revenueTypesMapper;

	@Autowired
	private TenderTypesLocationLinkMapper tenderTypesLinkMapper;
	@Autowired
	private RevenueTypesLocationLinkMapper revenueTypesLocationLinkMapper;

	@Autowired
	private BurdenInfoMapper burdenInfoMapper;

	@Autowired
	private RegionMapper regionMapper;

	@Autowired
	private BrandMapper brandMapper;

	@Autowired
	private LocationRepository locationRepository;

	public Location locationDTOToLocation(LocationDTO locationDTO) {

		if (locationDTO == null)
			return null;

		Location location = new Location();
		location.setLocationID(locationDTO.getLocationID());
		location.setClientLegalName(locationDTO.getClientLegalName());
		location.setAddress1(locationDTO.getAddress1());
		location.setAddress2(locationDTO.getAddress2());
		location.setCity(locationDTO.getCity());
		location.setCompanyName(locationDTO.getCompanyName());
		location.setContractType(locationDTO.getContractType());
		location.setId(locationDTO.getId());
		location.setNoofSpaces(locationDTO.getNoofSpaces());
		if (locationDTO.getName() == null
				|| locationDTO.getName().equals("NULL")) {
			location.setName("");
		} else {
			location.setName(locationDTO.getName());
		}
		location.setParentLocationID(locationDTO.getParentLocationID());
		location.setParentLocationName(locationDTO.getParentLocationName());
		location.setStatus(locationDTO.getStatus());
		location.setServiceType(locationDTO.getServiceType());
		location.setState(locationDTO.getState());
		location.setSubsidyType(locationDTO.getSubsidyType());
		location.setZipCode(locationDTO.getZipCode());
		location.setStatus(locationDTO.getStatus());
		location.setBrandCode(locationDTO.getBrandCode());
		location.setBrand(brandMapper.brandDTOToBrand(locationDTO.getBrandDTO()));
		// location.setBankAccounts(bankAccountsDTOSetToBankAccountsSet(locationDTO.getBankAccountsDTO()));
		location.setBurdens(burdensDTOToBurdensSet(locationDTO.getBurdensDTO()));
		// location.setLocationContact(locationContactDTOToLocationContact(locationDTO.getLocationContactDTO()));
		location.setLocationMoreInfo(locationMoreInfoMapper
				.locationMoreInfoDTOToLocationMoreInfo(locationDTO
						.getLocationMoreInfoDTO()));
	
		location.setPhone(locationDTO.getPhone());
		location.setCell(locationDTO.getCell());
		location.setFax(locationDTO.getFax());
		location.setExt(locationDTO.getExt());
		location.setApplicableForDarma(locationDTO.getApplicableForDarma());
		return location;

	}

	private Set<Burdens> burdensDTOToBurdensSet(Set<BurdensDTO> set) {
		if (set == null) {
			return null;

		}
		Set<Burdens> burdensSet = new HashSet<>();
		for (BurdensDTO burdensDTOs : set) {
			burdensSet.add(burdensMapper.burdensDTOToBurdens(burdensDTOs));
		}
		return burdensSet;
	}

	// get all
	public LocationDTO locationToLocationDTO(Location location) {

		if (location == null)
			return null;

		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setAddress1(location.getAddress1());
		locationDTO.setClientLegalName(location.getClientLegalName());
		locationDTO.setAddress2(location.getAddress2());
		locationDTO.setCity(location.getCity());
		locationDTO.setCompanyName(location.getCompanyName());
		locationDTO.setContractType(location.getContractType());
		locationDTO.setId(location.getId());
		locationDTO.setLocationID(location.getLocationID());
		locationDTO.setNoofSpaces(location.getNoofSpaces());
		if (location.getName() == null || location.getName().equals("NULL")) {
			locationDTO.setName("");
		} else {
			locationDTO.setName(location.getName());
		}
		locationDTO.setParentLocationID(location.getParentLocationID());
		locationDTO.setParentLocationName(location.getParentLocationName());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setServiceType(location.getServiceType());
		locationDTO.setState(location.getState());
		locationDTO.setSubsidyType(location.getSubsidyType());
		locationDTO.setZipCode(location.getZipCode());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setBrandCode(location.getBrandCode());
		locationDTO.setPhone(location.getPhone());
		locationDTO.setCell(location.getCell());
		locationDTO.setFax(location.getFax());
		locationDTO.setExt(location.getExt());
		locationDTO.setApplicableForDarma(location.getApplicableForDarma());
		locationDTO
				.setBrandDTO(brandMapper.brandToBrandDTO(location.getBrand()));
		return locationDTO;

	}

	// get one
	public LocationDTO locationToLocationDTO1(Location location) {

		if (location == null)
			return null;

		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setAddress1(location.getAddress1());
		locationDTO.setClientLegalName(location.getClientLegalName());
		locationDTO.setAddress2(location.getAddress2());
		locationDTO.setCity(location.getCity());
		locationDTO.setCompanyName(location.getCompanyName());
		locationDTO.setContractType(location.getContractType());
		locationDTO.setId(location.getId());
		locationDTO.setLocationID(location.getLocationID());
		locationDTO.setNoofSpaces(location.getNoofSpaces());
		if (location.getName() == null || location.getName().equals("NULL")) {
			locationDTO.setName("");
		} else {
			locationDTO.setName(location.getName());
		}
		locationDTO.setParentLocationID(location.getParentLocationID());
		locationDTO.setParentLocationName(location.getParentLocationName());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setServiceType(location.getServiceType());
		locationDTO.setState(location.getState());
		locationDTO.setSubsidyType(location.getSubsidyType());
		locationDTO.setZipCode(location.getZipCode());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setApplicableForDarma(location.getApplicableForDarma());
		/*
		 * locationDTO
		 * .setBurdensDTO(burdensToBurdensDTOSet(location.getBurdens()));
		 */
		locationDTO.setBrandCode(location.getBrandCode());
		locationDTO
				.setLocationContactDTO(locationContactSetToLocationContactDTOSet(location
						.getLocationContact()));
		// locationDTO.setBankAccountsDTO(bankAccountsSetToBankAccountsDTOSet(location.getBankAccounts()));
		locationDTO.setLocationMoreInfoDTO(locationMoreInfoMapper
				.locationMoreInfoToLocationMoreInfoDTO(location
						.getLocationMoreInfo()));
		/*
		 * locationDTO.setNotesDTO(locationNotesSetToLocationNotesDTOSet(location
		 * .getNotes()));
		 * locationDTO.setTenderTypesLnkDTO(tenderTypesSetToTenderTypesDTOSet
		 * (location.getTenderTypesLnk())); locationDTO.setRevenueTypesLnkDTO(
		 * revenueTypesLnkSetTorevenueTypesLnkDTOSet
		 * (location.getRevenueTypesLocationLink()));
		 * locationDTO.setRolesAndBillingRatesDTO(
		 * rolesAndBillingRatesSetToRolesAndBillingRatesDTOSet
		 * (location.getRolesAndBillingRates()));
		 * locationDTO.setBurdenInfo(locationSetToLocationDTOSet
		 * (location.getBurdenInfo()));
		 * locationDTO.setRegionDTO(regionMapper.regionTORegionDTO
		 * (location.getRegion()));
		 */
		locationDTO.setPhone(location.getPhone());
		locationDTO.setCell(location.getCell());
		locationDTO.setFax(location.getFax());
		locationDTO.setExt(location.getExt());
		locationDTO
				.setBrandDTO(brandMapper.brandToBrandDTO(location.getBrand()));
		return locationDTO;

	}

	private Set<RevenueTypesLocationLinkDTO> revenueTypesLnkSetTorevenueTypesLnkDTOSet(
			Set<RevenueTypesLocationLink> revenueTypesLocationLink) {
		if (revenueTypesLocationLink == null) {
			return null;

		}
		Set<RevenueTypesLocationLinkDTO> revenueTypesLocationLinkDTOSet = new HashSet<>();
		for (RevenueTypesLocationLink revenueTypesLnk : revenueTypesLocationLink) {
			revenueTypesLocationLinkDTOSet
					.add(revenueTypesLocationLinkMapper
							.revenueTypeLocationLinkToRevenueTypeLocationLinkDTO(revenueTypesLnk));
		}
		return revenueTypesLocationLinkDTOSet;
	}

	private Set<LocationContactDTO> locationContactSetToLocationContactDTOSet(
			Set<LocationContact> set) {
		if (set == null) {
			return null;

		}
		Set<LocationContactDTO> locationContactDTOSet = new HashSet<>();
		for (LocationContact locationContacts : set) {
			locationContactDTOSet.add(locationContactMapper
					.locationContactToLocationContactDTO(locationContacts));
		}
		return locationContactDTOSet;
	}

	private Set<BurdensDTO> burdensToBurdensDTOSet(Set<Burdens> set) {
		if (set == null) {
			return null;

		}
		Set<BurdensDTO> burdensDTOSet = new HashSet<>();
		for (Burdens burden : set) {
			burdensDTOSet.add(burdensMapper.burdensToBurdensDTO(burden));
		}
		return burdensDTOSet;
	}

	private Set<TenderTypesLocationLinkDTO> tenderTypesSetToTenderTypesDTOSet(
			Set<TenderTypesLocationLink> tenderTypesLnk) {
		if (tenderTypesLnk == null) {
			return null;

		}
		Set<TenderTypesLocationLinkDTO> tenderTypesLocationLinkDTOSet = new HashSet<>();
		for (TenderTypesLocationLink tenderTypesLnks : tenderTypesLnk) {
			tenderTypesLocationLinkDTOSet
					.add(tenderTypesLinkMapper
							.tenderTypesLocationLinkTOTenderTypesLocationLinkDTO(tenderTypesLnks));
		}
		return tenderTypesLocationLinkDTOSet;
	}

	/**
	 * THIS METHOD IS SPECIFIC FOR REVENUE AND SOURCE....!
	 * 
	 * @param location
	 * @return
	 */
	public LocationDTO getLocationDTOForRevenue(Location location) {
		if (location == null)
			return null;

		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setAddress1(location.getAddress1());
		locationDTO.setAddress2(location.getAddress2());
		locationDTO.setCity(location.getCity());
		locationDTO.setCompanyName(location.getCompanyName());
		locationDTO.setContractType(location.getContractType());
		locationDTO.setId(location.getId());
		locationDTO.setLocationID(location.getLocationID());
		locationDTO.setName(location.getName());
		if (location.getName() == null || location.getName().equals("NULL")) {
			locationDTO.setName("");
		} else {
			locationDTO.setName(location.getName());
		}
		locationDTO.setParentLocationID(location.getParentLocationID());
		locationDTO.setParentLocationName(location.getParentLocationName());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setServiceType(location.getServiceType());
		locationDTO.setState(location.getState());
		locationDTO.setSubsidyType(location.getSubsidyType());
		locationDTO.setZipCode(location.getZipCode());
		locationDTO.setApplicableForDarma(location.getApplicableForDarma());
		return locationDTO;
	}

	/**
	 * THIS METHOD IS SPECIFIC FOR REVENUE AND SOURCE....!
	 * 
	 * @param locationDTO
	 * @return
	 */
	public Location getLocationForRevenue(LocationDTO locationDTO) {
		if (locationDTO == null)
			return null;

		Location location = new Location();
		location.setAddress1(locationDTO.getAddress1());
		location.setAddress2(locationDTO.getAddress2());
		location.setCity(locationDTO.getCity());
		location.setCompanyName(locationDTO.getCompanyName());
		location.setContractType(locationDTO.getContractType());
		location.setId(locationDTO.getId());
		if (locationDTO.getName() == null
				|| locationDTO.getName().equals("NULL")) {
			location.setName("");
		} else {
			location.setName(locationDTO.getName());
		}
		location.setParentLocationID(locationDTO.getParentLocationID());
		location.setParentLocationName(locationDTO.getParentLocationName());
		location.setStatus(locationDTO.getStatus());
		location.setServiceType(locationDTO.getServiceType());
		location.setState(locationDTO.getState());
		location.setSubsidyType(locationDTO.getSubsidyType());
		location.setZipCode(locationDTO.getZipCode());
		location.setStatus(locationDTO.getStatus());
		location.setApplicableForDarma(locationDTO.getApplicableForDarma());
		return location;
	}

	public LocationDTO getLocationDTO(String locationid) {
		if (locationid == null)
			return null;
		else
			return locationToLocationDTO(locationRepository.findOne(locationid));
	}

	public LocationDTO locationToLocationDTOForBillingConv(Location location) {

		if (location == null)
			return null;

		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setAddress1(location.getAddress1());
		locationDTO.setClientLegalName(location.getClientLegalName());
		locationDTO.setAddress2(location.getAddress2());
		locationDTO.setCity(location.getCity());
		locationDTO.setCompanyName(location.getCompanyName());
		locationDTO.setContractType(location.getContractType());
		locationDTO.setId(location.getId());
		locationDTO.setLocationID(location.getLocationID());
		if (location.getName() == null || location.getName().equals("NULL")) {
			locationDTO.setName("");
		} else {
			locationDTO.setName(location.getName());
		}
		locationDTO.setNoofSpaces(location.getNoofSpaces());
		locationDTO.setParentLocationID(location.getParentLocationID());
		locationDTO.setParentLocationName(location.getParentLocationName());
		locationDTO.setStatus(location.getStatus());
		locationDTO.setServiceType(location.getServiceType());
		locationDTO.setState(location.getState());
		locationDTO.setSubsidyType(location.getSubsidyType());
		locationDTO.setZipCode(location.getZipCode());
		locationDTO.setStatus(location.getStatus());

		locationDTO.setPhone(location.getPhone());
		locationDTO.setCell(location.getCell());
		locationDTO.setFax(location.getFax());
		locationDTO.setExt(location.getExt());
		locationDTO.setApplicableForDarma(location.getApplicableForDarma());
		return locationDTO;

	}

	// get burdens
	public LocationBurdensDTO locationToLocationDTOWithBurdens(
			Set<Burdens> burdens, Set<ExclusionBurdens> excburdens, Location location, List<vwAllocations> list) {

		if (location == null)
			return null;

		LocationBurdensDTO locationDTO = new LocationBurdensDTO();
		locationDTO.setId(location.getId());
		locationDTO
				.setBurdensDTO(burdensToBurdensDTOSet(burdens));
		BurdenInfoDTO burdenInfoDTO = burdenInfoMapper
				.burdenInfoToBurdenInfoDTO1(location.getBurdenInfo());
		locationDTO.setBurdenInfoDTO(burdenInfoDTO);
		locationDTO.setExclusionBurdensDTO(exclusionBurdensToExclusionBurdensDTOSet(excburdens));
		
		Set<vwAllocationsDTO> set = new HashSet<vwAllocationsDTO>();
		if (list != null) {
			for (vwAllocations vwAllocations2 : list) {

				set.add(vwAllocationsMapper
						.vwAllocationsTovwAllocationsDTO(vwAllocations2));
			}

			locationDTO.setVwAllocationsDTO(set);
		}

		return locationDTO;

	}

	private Set<ExclusionBurdensDTO> exclusionBurdensToExclusionBurdensDTOSet(
			Set<ExclusionBurdens> set) {
		if (set == null) {
			return null;

		}
		Set<ExclusionBurdensDTO> burdensDTOSet = new HashSet<>();
		for (ExclusionBurdens burden : set) {
			burdensDTOSet.add(exclusionBurdensMapper
					.exclusionBurdensToExclusionBurdensDTO(burden));
		}
		return burdensDTOSet;
	}

	
	
	
	
		/**
		 * GET ONLY LOCATION SPECIFIC DATA...............!
		 * @param location
		 * @return
		 */
		public LocationDTO getOnlyLocationData(Location location) {

			if (location == null)
				return null;

			LocationDTO locationDTO = new LocationDTO();
			locationDTO.setAddress1(location.getAddress1());
			locationDTO.setClientLegalName(location.getClientLegalName());
			locationDTO.setAddress2(location.getAddress2());
			locationDTO.setCity(location.getCity());
			locationDTO.setCompanyName(location.getCompanyName());
			locationDTO.setContractType(location.getContractType());
			locationDTO.setId(location.getId());
			locationDTO.setLocationID(location.getLocationID());
			locationDTO.setNoofSpaces(location.getNoofSpaces());
			if (location.getName() == null || location.getName().equals("NULL")) {
				locationDTO.setName("");
			} else {
				locationDTO.setName(location.getName());
			}
			locationDTO.setParentLocationID(location.getParentLocationID());
			locationDTO.setParentLocationName(location.getParentLocationName());
			locationDTO.setStatus(location.getStatus());
			locationDTO.setServiceType(location.getServiceType());
			locationDTO.setState(location.getState());
			locationDTO.setSubsidyType(location.getSubsidyType());
			locationDTO.setZipCode(location.getZipCode());
			locationDTO.setStatus(location.getStatus());
			locationDTO.setPhone(location.getPhone());
			locationDTO.setCell(location.getCell());
			locationDTO.setFax(location.getFax());
			locationDTO.setExt(location.getExt());
			locationDTO.setApplicableForDarma(location.getApplicableForDarma());
			return locationDTO;

		}

		/**
		 * 
		 * TO GET LOCATION LINK DATA BASED ON ID............!
		 * @param bankAccountsLocationLinks
		 * @param tenderTypesLocationLinks
		 * @param revenueTypesLocationLinks
		 * @return
		 */
		public LocationLnkDTO getLocationLnkData(
				Set<BankAccountsLocationLink> bankAccountsLocationLinks,
				Set<TenderTypesLocationLink> tenderTypesLocationLinks,
				Set<RevenueTypesLocationLink> revenueTypesLocationLinks) {
			LocationLnkDTO locationLnkDto = new LocationLnkDTO();
			locationLnkDto.setTenderTypesLocationLinks(tenderTypesSetToTenderTypesDTOSet(tenderTypesLocationLinks));
			locationLnkDto.setRevenueTypesLocationLinks(revenueTypesLnkSetTorevenueTypesLnkDTOSet(revenueTypesLocationLinks));
			locationLnkDto.setBankAccountsLocationLinks(bankAccountsLnkSetToBankAccountsLnkDTOSet(bankAccountsLocationLinks));
			return locationLnkDto;
		}

		private Set<BankAccountsLocationLinkDTO> bankAccountsLnkSetToBankAccountsLnkDTOSet(
				Set<BankAccountsLocationLink> bankAccountsLocationLinks) {
			if (bankAccountsLocationLinks == null) {
				return null;

			}
			Set<BankAccountsLocationLinkDTO> tenderTypesLocationLinkDTOSet = new HashSet<>();
			for (BankAccountsLocationLink tenderTypesLnks : bankAccountsLocationLinks) {
				tenderTypesLocationLinkDTOSet
						.add(bankAccountLocationLnkMapper
								.bankAccountsLocationLinkTOBankAccountsLocationLinkDTO(tenderTypesLnks));
			}
			return tenderTypesLocationLinkDTOSet;
		}
	
	
	
	
	
	
}
