package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RegionDTO;
import com.cp.persistence.entity.Region;

@Component
public class RegionMapper {
	
	@Autowired
	private BrandMapper brandMapper;


	public RegionDTO regionTORegionDTO(Region region){
		if(region==null){
			return null;
		}
		RegionDTO regionDTO=new RegionDTO();
		
		regionDTO.setBrand(brandMapper.brandToBrandDTO(region.getBrand()));
		regionDTO.setCode(region.getCode());
		regionDTO.setDescription(region.getDescription());
		regionDTO.setId(region.getId());
		regionDTO.setLogo(region.getLogo());
		regionDTO.setStatus(region.getStatus());
		return regionDTO;
		
	}
	public Region regionDTOTORegion(RegionDTO regionDTO){
		if(regionDTO==null){
			return null;
		}
		
		Region region=new Region();
		
		region.setBrand(brandMapper.brandDTOToBrand(regionDTO.getBrand()));
		region.setCode(regionDTO.getCode());
		region.setDescription(regionDTO.getDescription());
		region.setId(regionDTO.getId());
		region.setLogo(regionDTO.getLogo());
		region.setStatus(regionDTO.getStatus());
		return region;
		
	}
}
