package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.TenderTypesLocationLinkDTO;
import com.cp.persistence.entity.Location;
import com.cp.persistence.entity.TenderTypesLocationLink;

@Component
public class TenderTypesLocationLinkMapper {
	@Autowired
	private TenderTypesMapper tenderTypesMapper;

	@Autowired
	private LocationMapper locationMapper;

	public TenderTypesLocationLinkDTO tenderTypesLocationLinkTOTenderTypesLocationLinkDTO(
			TenderTypesLocationLink tenderTypesLocationLink) {
		if (tenderTypesLocationLink == null) {
			return null;

		}
		TenderTypesLocationLinkDTO tenderTypesLocationLinkDTO = new TenderTypesLocationLinkDTO();
		tenderTypesLocationLinkDTO.setId(tenderTypesLocationLink.getId());
		/*tenderTypesLocationLinkDTO
				.setLocationDTO(getLocationDTO(tenderTypesLocationLink
						.getLocation()));*/
		tenderTypesLocationLinkDTO.setTenderTypesDTO(tenderTypesMapper
				.tenderTypesTOTenderTypesDTO(tenderTypesLocationLink
						.getTendertypes()));
		tenderTypesLocationLinkDTO.setStatus(tenderTypesLocationLink
				.getStatus());
		return tenderTypesLocationLinkDTO;
	}

	private Location getLocation(LocationDTO locationDTO) {
		if (locationDTO == null) {
			return null;
		}
		Location location = new Location();
		location.setId(locationDTO.getId());
		return location;
	}

	public TenderTypesLocationLink tenderTypesLocationLinkDTOTOTenderTypesLocationLink(
			TenderTypesLocationLinkDTO tenderTypesLocationLinkDTO) {
		if (tenderTypesLocationLinkDTO == null) {
			return null;
		}
		TenderTypesLocationLink tenderTypesLocationLink = new TenderTypesLocationLink();
		tenderTypesLocationLink.setId(tenderTypesLocationLinkDTO.getId());
		tenderTypesLocationLink
				.setLocation(getLocation(tenderTypesLocationLinkDTO
						.getLocationDTO()));
		tenderTypesLocationLink.setTendertypes(tenderTypesMapper
				.tenderTypesDTOTOTenderTypes(tenderTypesLocationLinkDTO
						.getTenderTypesDTO()));
		tenderTypesLocationLink.setStatus(tenderTypesLocationLinkDTO
				.getStatus());
		return tenderTypesLocationLink;
	}
}
