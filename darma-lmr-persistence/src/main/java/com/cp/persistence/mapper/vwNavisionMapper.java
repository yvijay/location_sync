package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.vwNavisionDTO;
import com.cp.persistence.entity.vwNavision;

@Component
public class vwNavisionMapper {

	
	public vwNavisionDTO vwNavisionTovwNavisionDTO(
			vwNavision vwNavision) {
		if (vwNavision == null) {
			return null;
		}
		vwNavisionDTO vwNavisionDTO = new vwNavisionDTO();
		vwNavisionDTO.setId(vwNavision.getId());
		vwNavisionDTO.setBorG(vwNavision.getBorG());
		vwNavisionDTO.setCompany(vwNavision.getCompany());
		vwNavisionDTO.setCorE(vwNavision.getCorE());
		vwNavisionDTO.setCredits(vwNavision.getCredits());
		vwNavisionDTO.setDebits(vwNavision.getDebits());
		vwNavisionDTO.setDepositidentifier(vwNavision.getDepositidentifier());
		vwNavisionDTO.setFilterPostingDate(vwNavision.getFilterPostingDate());
		vwNavisionDTO.setDescription(vwNavision.getDescription());
		vwNavisionDTO.setFilterPostingDate(vwNavision.getFilterPostingDate());
		vwNavisionDTO.setGLBaseAccount(vwNavision.getGLBaseAccount());
		vwNavisionDTO.setLocationId(vwNavision.getLocationId());
		vwNavisionDTO.setLocation(vwNavision.getLocation());
		vwNavisionDTO.setPostednavisiondate(vwNavision.getPostednavisiondate());
		vwNavisionDTO.setPostingDate(vwNavision.getPostingDate());
		vwNavisionDTO.setReconciledDate(vwNavision.getReconciledDate());
		vwNavisionDTO.setRevenueDate(vwNavision.getRevenueDate());
		vwNavisionDTO.setPostedBy(vwNavision.getPostedBy());
		
		return vwNavisionDTO;
	}

	public vwNavision vwNavisionDTOTovwNavision(
			vwNavisionDTO vwNavisionDTO) {
		if (vwNavisionDTO == null) {
			return null;
		}
		vwNavision vwNavision = new vwNavision();
		vwNavision.setId(vwNavisionDTO.getId());
		vwNavision.setBorG(vwNavisionDTO.getBorG());
		vwNavision.setCompany(vwNavisionDTO.getCompany());
		vwNavision.setCorE(vwNavisionDTO.getCorE());
		vwNavision.setCredits(vwNavisionDTO.getCredits());
		vwNavision.setDebits(vwNavisionDTO.getDebits());
		vwNavision.setDepositidentifier(vwNavisionDTO.getDepositidentifier());
		vwNavision.setFilterPostingDate(vwNavisionDTO.getFilterPostingDate());
		vwNavision.setDescription(vwNavisionDTO.getDescription());
		vwNavision.setFilterPostingDate(vwNavisionDTO.getFilterPostingDate());
		vwNavision.setGLBaseAccount(vwNavisionDTO.getGLBaseAccount());
		vwNavision.setLocationId(vwNavisionDTO.getLocationId());
		vwNavision.setLocation(vwNavisionDTO.getLocation());
		vwNavision.setPostednavisiondate(vwNavisionDTO.getPostednavisiondate());
		vwNavision.setPostingDate(vwNavisionDTO.getPostingDate());
		vwNavision.setReconciledDate(vwNavisionDTO.getReconciledDate());
		vwNavision.setRevenueDate(vwNavisionDTO.getRevenueDate());
		vwNavision.setPostedBy(vwNavisionDTO.getPostedBy());
	
		return vwNavision;
	}
}
