package com.cp.persistence.mapper;


import org.springframework.stereotype.Component;

import com.cp.persistence.dto.PositionsDTO;
import com.cp.persistence.entity.Positions;

@Component
public class PositionsMapper {

	public PositionsDTO positionsTOPositionsDTO(Positions positions){
		if(positions==null){
			return null;
		}
		PositionsDTO positionsDTO=new PositionsDTO();
		
		positionsDTO.setCode(positions.getCode());
		positionsDTO.setDescription(positions.getDescription());
		positionsDTO.setId(positions.getId());
		positionsDTO.setStatus(positions.getStatus());
		return positionsDTO;
		
	}
	public Positions positionsDTOTOPositions(PositionsDTO positionsDTO){
		if(positionsDTO==null){
			return null;
		}
		Positions positions=new Positions();
		
		positions.setCode(positionsDTO.getCode());
		positions.setDescription(positionsDTO.getDescription());
		positions.setId(positionsDTO.getId());
		positions.setStatus(positionsDTO.getStatus());
		return positions;
		
	}
}
