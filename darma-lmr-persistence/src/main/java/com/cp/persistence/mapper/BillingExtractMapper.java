package com.cp.persistence.mapper;

import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BillingExtractDTO;
import com.cp.persistence.entity.BillingExtract;
import com.cp.persistence.entity.BillingExtractFile;

@Component
public class BillingExtractMapper {
	
	public BillingExtractDTO billingExtractToBillingExtractDTO(BillingExtract billingExtract) {
		if(billingExtract == null) {
			return null;
		}
		
		BillingExtractDTO billingExtractDTO = new BillingExtractDTO();
		
		billingExtractDTO.setId(billingExtract.getId());
		billingExtractDTO.setPayPeriod(billingExtract.getPayPeriod());
		billingExtractDTO.setPayDate(billingExtract.getPayDate());
		billingExtractDTO.setBrandCode(billingExtract.getBrandCode());
		billingExtractDTO.setLocationName(billingExtract.getLocationName());
		billingExtractDTO.setGlString(billingExtract.getGlString());
		billingExtractDTO.setParentOrgUnitCode(billingExtract.getParentOrgUnitCode());
		billingExtractDTO.setOnsiteDeptName(billingExtract.getOnsiteDeptName());
		billingExtractDTO.setOnsiteDeptDesc(billingExtract.getOnsiteDeptDesc());
		billingExtractDTO.setWasOrgCode(billingExtract.getWasOrgCode());
		billingExtractDTO.setWasDeptCode(billingExtract.getWasDeptCode());
		billingExtractDTO.setWasJobCode(billingExtract.getWasJobCode());
		billingExtractDTO.setJobName(billingExtract.getJobName());
		billingExtractDTO.setBillable(billingExtract.getBillable());
		billingExtractDTO.setPositionCode(billingExtract.getPositionCode());
		billingExtractDTO.setTerritoryCode(billingExtract.getTerritoryCode());
		billingExtractDTO.setRegularHrs(billingExtract.getRegularHrs());
		billingExtractDTO.setOtHolidayHrs(billingExtract.getOtHolidayHrs());
		billingExtractDTO.setTotalHrs(billingExtract.getTotalHrs());
		billingExtractDTO.setBillingExtractFile(billingExtractFileToBillingExtractFileId(billingExtract.getBillingExtractFile()));
		
		return billingExtractDTO;
	}
	
	public BillingExtract billingExtractDTOToBillingExtract(BillingExtractDTO billingExtractDTO) {
		if(billingExtractDTO == null) {
			return null;
		}
		
		BillingExtract billingExtract = new BillingExtract();
		
		billingExtract.setId(billingExtractDTO.getId());
		billingExtract.setPayPeriod(billingExtractDTO.getPayPeriod());
		billingExtract.setPayDate(billingExtractDTO.getPayDate());
		billingExtract.setBrandCode(billingExtractDTO.getBrandCode());
		billingExtract.setLocationName(billingExtractDTO.getLocationName());
		billingExtract.setGlString(billingExtractDTO.getGlString());
		billingExtract.setParentOrgUnitCode(billingExtractDTO.getParentOrgUnitCode());
		billingExtract.setOnsiteDeptName(billingExtractDTO.getOnsiteDeptName());
		billingExtract.setOnsiteDeptDesc(billingExtractDTO.getOnsiteDeptDesc());
		billingExtract.setWasOrgCode(billingExtractDTO.getWasOrgCode());
		billingExtract.setWasDeptCode(billingExtractDTO.getWasDeptCode());
		billingExtract.setWasJobCode(billingExtractDTO.getWasJobCode());
		billingExtract.setJobName(billingExtractDTO.getJobName());
		billingExtract.setBillable(billingExtractDTO.getBillable());
		billingExtract.setPositionCode(billingExtractDTO.getPositionCode());
		billingExtract.setTerritoryCode(billingExtractDTO.getTerritoryCode());
		billingExtract.setRegularHrs(billingExtractDTO.getRegularHrs());
		billingExtract.setOtHolidayHrs(billingExtractDTO.getOtHolidayHrs());
		billingExtract.setTotalHrs(billingExtractDTO.getTotalHrs());
		billingExtract.setBillingExtractFile(billingExtractFileIdToBillingExtractFile(billingExtractDTO.getBillingExtractFile()));
		
		return billingExtract;
	}
	
	private String billingExtractFileToBillingExtractFileId(BillingExtractFile billingExtractFile) {
		if(billingExtractFile == null){
			return null;
		}
		
		return billingExtractFile.getId();
	}
	
	private BillingExtractFile billingExtractFileIdToBillingExtractFile(String billingExtractFileId) {
		if(billingExtractFileId == null) {
			return null;
		}
		
		BillingExtractFile billingExtractFile = new BillingExtractFile();
		billingExtractFile.setId(billingExtractFileId);
		return billingExtractFile;
	}
}
