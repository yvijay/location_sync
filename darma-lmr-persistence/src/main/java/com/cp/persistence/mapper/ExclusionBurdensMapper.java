package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.ExclusionBurdensDTO;
import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.entity.ExclusionBurdens;
import com.cp.persistence.entity.Location;

@Component
public class ExclusionBurdensMapper {

	@Autowired
	private LocationMapper locationMapper;

	public ExclusionBurdens exclusionBurdensDTOToExclusionBurdens(
			ExclusionBurdensDTO exclusionBurdensDTO) {
		if (exclusionBurdensDTO == null)
			return null;

		ExclusionBurdens exclusionBurdens = new ExclusionBurdens();

		exclusionBurdens.setId(exclusionBurdensDTO.getId());
		exclusionBurdens.setcEorC(exclusionBurdensDTO.getcEorC());
		exclusionBurdens.setcGL(exclusionBurdensDTO.getcGL());
		exclusionBurdens.setcOffsetEorC(exclusionBurdensDTO.getcOffsetEorC());
		exclusionBurdens.setcPercentage(exclusionBurdensDTO.getcPercentage());
		exclusionBurdens.setdEorC(exclusionBurdensDTO.getdEorC());
		exclusionBurdens.setdGL(exclusionBurdensDTO.getdGL());
		exclusionBurdens.setdOffsetEorC(exclusionBurdensDTO.getdOffsetEorC());
		exclusionBurdens.setdPercentage(exclusionBurdensDTO.getdPercentage());
		exclusionBurdens.setIncludingTips(exclusionBurdensDTO
				.getIncludingTips());
		exclusionBurdens.setJobDescription(exclusionBurdensDTO
				.getJobDescription());
		exclusionBurdens.setOffsettingAccount(exclusionBurdensDTO
				.getOffsettingAccount());
		exclusionBurdens.setType(exclusionBurdensDTO.getType());
		exclusionBurdens.setLocation(getLocation(exclusionBurdensDTO
				.getLocationDTO()));
		exclusionBurdens.setMappingLocationId(exclusionBurdensDTO
				.getMappingLocationId());
		exclusionBurdens.setStatus(exclusionBurdensDTO.getStatus());
		return exclusionBurdens;
	}

	public ExclusionBurdensDTO exclusionBurdensToExclusionBurdensDTO(
			ExclusionBurdens exclusionBurdens) {
		if (exclusionBurdens == null)
			return null;

		ExclusionBurdensDTO exclusionBurdensDTO = new ExclusionBurdensDTO();

		exclusionBurdensDTO.setId(exclusionBurdens.getId());
		exclusionBurdensDTO.setcEorC(exclusionBurdens.getcEorC());
		exclusionBurdensDTO.setcGL(exclusionBurdens.getcGL());
		exclusionBurdensDTO.setcOffsetEorC(exclusionBurdens.getcOffsetEorC());
		exclusionBurdensDTO.setcPercentage(exclusionBurdens.getcPercentage());
		exclusionBurdensDTO.setdEorC(exclusionBurdens.getdEorC());
		exclusionBurdensDTO.setdGL(exclusionBurdens.getdGL());
		exclusionBurdensDTO.setdOffsetEorC(exclusionBurdens.getdOffsetEorC());
		exclusionBurdensDTO.setdPercentage(exclusionBurdens.getdPercentage());
		exclusionBurdensDTO.setIncludingTips(exclusionBurdens
				.getIncludingTips());
		exclusionBurdensDTO.setJobDescription(exclusionBurdens
				.getJobDescription());
		exclusionBurdensDTO.setOffsettingAccount(exclusionBurdens
				.getOffsettingAccount());
		exclusionBurdensDTO.setType(exclusionBurdens.getType());
		// burdensDTO.setLocationDTO(getLocationDTO(burdens.getLocation()));
		exclusionBurdensDTO.setStatus(exclusionBurdens.getStatus());
		exclusionBurdensDTO.setMappingLocationId(exclusionBurdens
				.getMappingLocationId());
		return exclusionBurdensDTO;
	}

	private Location getLocation(LocationDTO locationDTO) {
		if (locationDTO == null) {
			return null;
		}
		Location location = new Location();
		location.setId(locationDTO.getId());
		return location;
	}

}
