package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.vwAllocationsDTO;
import com.cp.persistence.entity.vwAllocations;

@Component
public class vwAllocationsMapper {

	@Autowired
	private LocationMapper locationMapper;

	public vwAllocations vwAllocationsDTOTovwAllocations(
			vwAllocationsDTO vwAllocationsDTO) {
		if (vwAllocationsDTO == null)
			return null;

		vwAllocations vwAllocations = new vwAllocations();

		vwAllocations.setId(vwAllocationsDTO.getId());

		vwAllocations.setcEorC(vwAllocationsDTO.getcEorC());
		vwAllocations.setcGL(vwAllocationsDTO.getcGL());
		vwAllocations.setcOffsetEorC(vwAllocationsDTO.getcOffsetEorC());
		vwAllocations.setcPercentage(vwAllocationsDTO.getcPercentage());
		vwAllocations.setdEorC(vwAllocationsDTO.getdEorC());
		vwAllocations.setdGL(vwAllocationsDTO.getdGL());
		vwAllocations.setdOffsetEorC(vwAllocationsDTO.getdOffsetEorC());
		vwAllocations.setdPercentage(vwAllocationsDTO.getdPercentage());
		vwAllocations.setJobDescription(vwAllocationsDTO.getJobDescription());
		vwAllocations.setType(vwAllocationsDTO.getType());
		vwAllocations.setLocationId(vwAllocationsDTO.getLocationId());
		vwAllocations.setYear(vwAllocationsDTO.getYear());
		vwAllocations.setManaged(vwAllocationsDTO.getManaged());
		vwAllocations.setCompanyName(vwAllocationsDTO.getCompanyName());
		return vwAllocations;
	}

	public vwAllocationsDTO vwAllocationsTovwAllocationsDTO(
			vwAllocations vwAllocations) {
		if (vwAllocations == null)
			return null;

		vwAllocationsDTO vwAllocationsDTO = new vwAllocationsDTO();

		vwAllocationsDTO.setcEorC(vwAllocations.getcEorC());
		vwAllocationsDTO.setcGL(vwAllocations.getcGL());
		vwAllocationsDTO.setcOffsetEorC(vwAllocations.getcOffsetEorC());
		vwAllocationsDTO.setcPercentage(vwAllocations.getcPercentage());
		vwAllocationsDTO.setdEorC(vwAllocations.getdEorC());
		vwAllocationsDTO.setdGL(vwAllocations.getdGL());
		vwAllocationsDTO.setdOffsetEorC(vwAllocations.getdOffsetEorC());
		vwAllocationsDTO.setdPercentage(vwAllocations.getdPercentage());
		vwAllocationsDTO.setId(vwAllocations.getId());
		vwAllocationsDTO.setJobDescription(vwAllocations.getJobDescription());
		vwAllocationsDTO.setType(vwAllocations.getType());
		vwAllocationsDTO.setLocationId(vwAllocations.getLocationId());
		vwAllocationsDTO.setYear(vwAllocations.getYear());
		vwAllocationsDTO.setManaged(vwAllocations.getManaged());
		vwAllocationsDTO.setCompanyName(vwAllocations.getCompanyName());
		return vwAllocationsDTO;
	}

}
