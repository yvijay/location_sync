package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationNotesDTO;
import com.cp.persistence.entity.LocationNotes;


@Component
public class LocationNotesMapper {

	
	@Autowired
	LocationMapper LocationMapper;
	public LocationNotesDTO locationNotesToLocationNotesDTO(LocationNotes notes){
		if(notes==null)
			return null;
		
		LocationNotesDTO notesDTO=new LocationNotesDTO();
		
		notesDTO.setId(notes.getId());
		notesDTO.setNoteHeading(notes.getNoteHeading());
		notesDTO.setNotes(notes.getNotes());
		notesDTO.setStatus(notes.getStatus());		
		
		return notesDTO;
	}
	
	public LocationNotes locationNotesDTOToLocationNotes(LocationNotesDTO notesDTO){
		
		if(notesDTO==null)
		{
			return null;
		}
		
		LocationNotes notes=new LocationNotes();
		
		notes.setId(notesDTO.getId());
		notes.setNoteHeading(notesDTO.getNoteHeading());
		notes.setNotes(notesDTO.getNotes());
		notes.setLocation(LocationMapper.locationDTOToLocation(notesDTO.getLocationDTO()));
		notes.setStatus(notesDTO.getStatus());
		return notes;
	}
	
}
