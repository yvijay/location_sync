package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationContactDTO;
import com.cp.persistence.entity.LocationContact;

@Component
public class LocationContactMapper {

	@Autowired
	private LocationMapper locationMapper;
	@Autowired
	private BrandMapper brandMapper;

	@Autowired
	private ContactTypesMapper contactTypesMapper;

	public LocationContact locationContactDTOToLocationContact(
			LocationContactDTO locationContactDTO) {

		if (locationContactDTO == null)
			return null;

		LocationContact locationContact = new LocationContact();

		locationContact.setAddress1(locationContactDTO.getAddress1());
		locationContact.setAddress2(locationContactDTO.getAddress2());
		locationContact.setBusinessName(locationContactDTO.getBusinessName());
		locationContact.setCity(locationContactDTO.getCity());
		locationContact.setContacttype(contactTypesMapper
				.contactTypesDTOToContactTypes(locationContactDTO
						.getContactType()));
		locationContact.setEmailAddress(locationContactDTO.getEmailAddress());
		locationContact.setFirstName(locationContactDTO.getFirstName());
		locationContact.setId(locationContactDTO.getId());
		locationContact.setLastName(locationContactDTO.getLastName());
		locationContact.setPrior(locationContactDTO.getPrior());
		locationContact.setState(locationContactDTO.getState());
		locationContact.setZip(locationContactDTO.getZip());
		locationContact.setPhone(locationContactDTO.getPhone());
		locationContact.setCell(locationContactDTO.getCell());
		locationContact.setFax(locationContactDTO.getFax());
		locationContact.setExt(locationContactDTO.getExt());
		locationContact.setBrand(brandMapper.brandDTOToBrand(locationContactDTO
				.getBrandDTO()));
		locationContact.setLocation(locationMapper
				.locationDTOToLocation(locationContactDTO.getLocationDTO()));
		locationContact.setStatus(locationContactDTO.getStatus());
		return locationContact;

	}

	public LocationContactDTO locationContactToLocationContactDTO(
			LocationContact locationContact) {

		if (locationContact == null)
			return null;

		LocationContactDTO locationContactDTO = new LocationContactDTO();

		locationContactDTO.setAddress1(locationContact.getAddress1());
		locationContactDTO.setAddress2(locationContact.getAddress2());
		locationContactDTO.setBusinessName(locationContact.getBusinessName());
		locationContactDTO.setCity(locationContact.getCity());
		locationContactDTO
				.setContactType(contactTypesMapper
						.contactTypesToContactTypesDTO(locationContact
								.getContacttype()));
		locationContactDTO.setEmailAddress(locationContact.getEmailAddress());
		locationContactDTO.setFirstName(locationContact.getFirstName());
		locationContactDTO.setId(locationContact.getId());
		locationContactDTO.setLastName(locationContact.getLastName());
		locationContactDTO.setPrior(locationContact.getPrior());
		locationContactDTO.setState(locationContact.getState());
		locationContactDTO.setZip(locationContact.getZip());
		locationContactDTO.setPhone(locationContact.getPhone());
		locationContactDTO.setCell(locationContact.getCell());
		locationContactDTO.setFax(locationContact.getFax());
		locationContactDTO.setExt(locationContact.getExt());
		// locationContactDTO.setBrandDTO(brandMapper.brandToBrandDTO(locationContact.getBrand()));

		// locationContactDTO.setLocationDTO(locationMapper.locationToLocationDTO(locationContact.getLocation()));
		locationContactDTO.setStatus(locationContact.getStatus());
		return locationContactDTO;

	}

	// LocationContacts without location
	public LocationContactDTO locationContactToLocationContactDTOWithoutLocation(
			LocationContact locationContact) {

		if (locationContact == null)
			return null;

		LocationContactDTO locationContactDTO = new LocationContactDTO();

		locationContactDTO.setAddress1(locationContact.getAddress1());
		locationContactDTO.setAddress2(locationContact.getAddress2());
		locationContactDTO.setBusinessName(locationContact.getBusinessName());
		locationContactDTO.setCity(locationContact.getCity());
		// locationContactDTO.setContactType(locationContact.getContactType());
		locationContactDTO.setEmailAddress(locationContact.getEmailAddress());
		locationContactDTO.setFirstName(locationContact.getFirstName());
		locationContactDTO.setId(locationContact.getId());
		locationContactDTO.setLastName(locationContact.getLastName());
		locationContactDTO.setPrior(locationContact.getPrior());
		locationContactDTO.setState(locationContact.getState());
		locationContactDTO.setZip(locationContact.getZip());
		locationContactDTO.setPhone(locationContact.getPhone());
		locationContactDTO.setCell(locationContact.getCell());
		locationContactDTO.setFax(locationContact.getFax());
		locationContactDTO.setExt(locationContact.getExt());
		locationContactDTO.setStatus(locationContact.getStatus());
		// locationContactDTO.setBrandDTO(brandMapper.brandToBrandDTO(locationContact.getBrand()));
		// locationContactDTO.setLocationDTO(locationMapper.locationToLocationDTO(locationContact.getLocation()));

		return locationContactDTO;

	}

}
