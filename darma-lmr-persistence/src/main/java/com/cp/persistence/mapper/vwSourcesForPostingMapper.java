package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BankAccountsDTO;
import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.RevenueTypesDTO;
import com.cp.persistence.dto.TenderTypesDTO;
import com.cp.persistence.dto.UserDTO;
import com.cp.persistence.dto.vwSourcesForPostingDTO;
import com.cp.persistence.entity.vwSourcesForPosting;

@Component
public class vwSourcesForPostingMapper {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private BankAccountsMapper bankAccountsMapper;
	
	@Autowired
	private LocationMapper locationMapper;
	
	@Autowired
	private TenderTypesMapper tenderTypesMapper;
	
	@Autowired
	private RevenueTypesMapper revenueTypesMapper;
	
	public vwSourcesForPostingDTO vwSourcesForPostingTovwSourcesForPostingDTO1(
			vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		vwSourcesForPostingDTO vwSourcesForPostingDTO = new vwSourcesForPostingDTO();
		vwSourcesForPostingDTO.setAmount(vwSourcesForPosting.getAmount());
		vwSourcesForPostingDTO.setBankaccountDTO(bankAccountsMapper.getBankAccountDTO(vwSourcesForPosting.getBankaccountid()));
		vwSourcesForPostingDTO.setCorrectiveYn(vwSourcesForPosting.getCorrectiveYn());
		vwSourcesForPostingDTO.setDepositIdentifier(vwSourcesForPosting.getDepositIdentifier());
		vwSourcesForPostingDTO.setId(vwSourcesForPosting.getId());
		vwSourcesForPostingDTO.setLocation(vwSourcesForPosting.getLocation());
		vwSourcesForPostingDTO.setLocationDTO(locationMapper.getLocationDTO(vwSourcesForPosting.getLocationid()));
		vwSourcesForPostingDTO.setPostingDate(vwSourcesForPosting.getPostingDate());
		vwSourcesForPostingDTO.setRegionid(vwSourcesForPosting.getRegionid());
		vwSourcesForPostingDTO.setRevenueTypeDTO(revenueTypesMapper.getRevenueTypeDTO(vwSourcesForPosting.getRevenuetypeid()));
		vwSourcesForPostingDTO.setSubAccountCode(vwSourcesForPosting.getSubAccountCode());
		vwSourcesForPostingDTO.setSubmittedBy(userMapper.getUserDTO(vwSourcesForPosting.getSubmittedBy()));
		vwSourcesForPostingDTO.setSubmitteddate(vwSourcesForPosting.getSubmitteddate());
		vwSourcesForPostingDTO.setSubTotal(vwSourcesForPosting.getSubTotal());
		vwSourcesForPostingDTO.setTenderTypeDTO(tenderTypesMapper.getTenderTypeDTO(vwSourcesForPosting.getTenderid()));
		vwSourcesForPostingDTO.setTransactionDate(vwSourcesForPosting.getTransactionDate());
		return vwSourcesForPostingDTO;
	}
	
	public vwSourcesForPostingDTO vwSourcesForPostingTovwSourcesForPostingDTO(
			vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		vwSourcesForPostingDTO vwSourcesForPostingDTO = new vwSourcesForPostingDTO();
		vwSourcesForPostingDTO.setAmount(vwSourcesForPosting.getAmount());
		vwSourcesForPostingDTO.setBankaccountDTO(getBankAccountDTO(vwSourcesForPosting));
		vwSourcesForPostingDTO.setCorrectiveYn(vwSourcesForPosting.getCorrectiveYn());
		vwSourcesForPostingDTO.setDepositIdentifier(vwSourcesForPosting.getDepositIdentifier());
		vwSourcesForPostingDTO.setId(vwSourcesForPosting.getId());
		vwSourcesForPostingDTO.setLocation(vwSourcesForPosting.getLocation());
		vwSourcesForPostingDTO.setLocationDTO(getLocationDTO(vwSourcesForPosting));
		vwSourcesForPostingDTO.setPostingDate(vwSourcesForPosting.getPostingDate());
		vwSourcesForPostingDTO.setRegionid(vwSourcesForPosting.getRegionid());
		vwSourcesForPostingDTO.setRevenueTypeDTO(getRevenueTypeDTO(vwSourcesForPosting));
		vwSourcesForPostingDTO.setSubAccountCode(vwSourcesForPosting.getSubAccountCode());
		vwSourcesForPostingDTO.setSubmittedBy(getUserDTO(vwSourcesForPosting));
		vwSourcesForPostingDTO.setSubmitteddate(vwSourcesForPosting.getSubmitteddate());
		vwSourcesForPostingDTO.setSubTotal(vwSourcesForPosting.getSubTotal());
		vwSourcesForPostingDTO.setTenderTypeDTO(getTenderTypeDTO(vwSourcesForPosting));
		vwSourcesForPostingDTO.setTransactionDate(vwSourcesForPosting.getTransactionDate());
		vwSourcesForPostingDTO.setReconciledDate(vwSourcesForPosting.getReconciledDate());
		return vwSourcesForPostingDTO;
	}

	/**
	 * TO GET TENDERTYPES DTO
	 * @param vwSourcesForPosting
	 * @return TenderTypesDTO
	 */
	private TenderTypesDTO getTenderTypeDTO(
			vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		TenderTypesDTO tenderTypesDTO=new TenderTypesDTO();
		tenderTypesDTO.setId(vwSourcesForPosting.getTenderid());
		tenderTypesDTO.setCode(vwSourcesForPosting.getTenderDescription());
		return tenderTypesDTO;
	}

	/**
	 * TO GET USER DTO
	 * @param vwSourcesForPosting
	 * @return UserDTO
	 */
	private UserDTO getUserDTO(vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		UserDTO userDTO= new UserDTO();
		userDTO.setId(vwSourcesForPosting.getSubmittedBy());
		userDTO.setDisplayName(vwSourcesForPosting.getSubmittedDisplayName());
		return userDTO;
	}

	/**
	 * TO GET REVENUETYPES DTO
	 * @param vwSourcesForPosting
	 * @return RevenueTypesDTO
	 */
	private RevenueTypesDTO getRevenueTypeDTO(
			vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		RevenueTypesDTO revenueTypesDTO= new RevenueTypesDTO();
		revenueTypesDTO.setId(vwSourcesForPosting.getRevenuetypeid());
		revenueTypesDTO.setCode(vwSourcesForPosting.getRevenueDescription());
		return revenueTypesDTO;
		
	}

	/**
	 * TO GET LOCATION DTO
	 * @param vwSourcesForPosting
	 * @return LocationDTO
	 */
	private LocationDTO getLocationDTO(vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		
		LocationDTO locationDTO=new LocationDTO();
		locationDTO.setId(vwSourcesForPosting.getLocationid());
		locationDTO.setName(vwSourcesForPosting.getLocationName());
		return locationDTO;
	}
	/**
	 * TO GET BANKACCOUNTS DTO
	 * @param vwSourcesForPosting
	 * @return BankAccountsDTO
	 */
	private BankAccountsDTO getBankAccountDTO(
			vwSourcesForPosting vwSourcesForPosting) {
		if (vwSourcesForPosting == null) {
			return null;
		}
		
		BankAccountsDTO bankAccountsDTO=new BankAccountsDTO();
		bankAccountsDTO.setId(vwSourcesForPosting.getId());
		bankAccountsDTO.setBankName(vwSourcesForPosting.getBankName());
		return bankAccountsDTO;
	}
}
