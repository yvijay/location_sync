package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RegionLocationLinkDTO;
import com.cp.persistence.entity.RegionLocationLink;

@Component
public class RegionLocationLinkMapper {

	@Autowired
	LocationMapper locationMapper;
	@Autowired
	RegionMapper regionMapper;

	public RegionLocationLinkDTO regionLocationLinkToRegionLocationLinkDTO(
			RegionLocationLink regionLocationLink) {
		if (regionLocationLink == null) {
			return null;
		}
		RegionLocationLinkDTO regionLocationLinkDTO = new RegionLocationLinkDTO();
		regionLocationLinkDTO.setId(regionLocationLink.getId());
		regionLocationLinkDTO.setRegionDTO(
				regionMapper.regionTORegionDTO(regionLocationLink.getRegion()));
	/*	revenueTypesLocationLinkDTO
				.setLocationDTO(locationMapper.locationToLocationDTO(revenueTypesLocationLink.getLocation()));*/
		regionLocationLinkDTO.setStatus(regionLocationLink.getStatus());
		return regionLocationLinkDTO;

	}

	public RegionLocationLink regionLocationLinkDTOToRegionLocationLink(
			RegionLocationLinkDTO regionLocationLinkDTO) {
		if (regionLocationLinkDTO == null) {
			return null;
		}
		RegionLocationLink regionLocationLink = new RegionLocationLink();
		regionLocationLink.setRegion(
				regionMapper.regionDTOTORegion(regionLocationLinkDTO.getRegionDTO()));;
	regionLocationLink
				.setLocation(locationMapper.locationDTOToLocation(regionLocationLinkDTO.getLocationDTO()));
	regionLocationLink.setStatus(regionLocationLinkDTO.getStatus());
		return regionLocationLink;

	}

	/*
	 * public List<RegionLocationLink>
	 * revenueTypeLocationLinkDTOToRevenueTypeLocationLinkForLink(
	 * RegionLocationLinkDTO revenueTypesLocationLinkDTO) { if
	 * (revenueTypesLocationLinkDTO == null) { return null; }
	 * List<RegionLocationLink> revenueTypesLocationLinks = new
	 * ArrayList<>(); for (RegionLocationLinkDTO
	 * revenueTypesLocationLinkDTOs : revenueTypesLocationLinkDTO
	 * .getRevenueTypeLocationLinks()) { RegionLocationLink
	 * revenueTypesLocationLink = new RegionLocationLink();
	 * revenueTypesLocationLink.setRevenueTypes(revenueTypesLocationLinkDTOs.
	 * getRevenueTypesDTO());
	 * revenueTypesLocationLink.setGlNumber(revenueTypesLocationLinkDTOs.
	 * getGlNumber()); revenueTypesLocationLink
	 * .setLocation(locationMapper.locationDTOToLocation(
	 * revenueTypesLocationLinkDTO.getLocation()));
	 * revenueTypesLocationLinks.add(revenueTypesLocationLink); }
	 * 
	 * return revenueTypesLocationLinks;
	 * 
	 * }
	 * 
	 * public LocationAndRevenueTypesLinkDTO
	 * revenueTypeLocationLinkToRevenueTypeLocationLinkForLinkDTO(
	 * List<RegionLocationLink> revenueTypesLocationLinks) { if
	 * (revenueTypesLocationLinks == null) { return null; }
	 * 
	 * LocationAndRevenueTypesLinkDTO revenueTypesLocationLinksDTO2 = new
	 * LocationAndRevenueTypesLinkDTO(); Set<RegionLocationLinkDTO>
	 * revenueTypesLocationLinksDTOset = new HashSet<>(); for
	 * (RegionLocationLink revenueTypesLocationLink :
	 * revenueTypesLocationLinks) { RegionLocationLinkDTO
	 * revenueTypesLocationLinkDTO = new RegionLocationLinkDTO();
	 * revenueTypesLocationLinkDTO.setGlNumber(revenueTypesLocationLink.
	 * getGlNumber());
	 * revenueTypesLocationLinkDTO.setRevenueTypesDTO(revenueTypesLocationLink.
	 * getRevenueTypes()); revenueTypesLocationLinkDTO
	 * .setLocation(locationMapper.locationToLocationDTO(
	 * revenueTypesLocationLink.getLocation()));
	 * revenueTypesLocationLinksDTOset.add(revenueTypesLocationLinkDTO); }
	 * 
	 * revenueTypesLocationLinksDTO2.setRevenueTypeLocationLinks(
	 * revenueTypesLocationLinksDTOset); return revenueTypesLocationLinksDTO2; }
	 */
}
