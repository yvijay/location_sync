package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BrandDTO;
import com.cp.persistence.dto.GLCodesDTO;
import com.cp.persistence.entity.Brand;
import com.cp.persistence.entity.GLCodes;

@Component
public class BrandMapper {

	@Autowired
	private GLCodesMapper glCodesMapper;

	@Autowired
	private RegionMapper regionMapper;

	@Autowired
	private UserMapper userMapper;

	public BrandDTO brandToBrandDTO(Brand brand) {
		if (brand == null) {
			return null;
		}

		BrandDTO brandDTO = new BrandDTO();

		brandDTO.setCode(brand.getCode());
		brandDTO.setDescription(brand.getDescription());
		brandDTO.setId(brand.getId());
		brandDTO.setLogo(brand.getLogo());
		
	//	brandDTO.setRegionsDTO(regionSetToRegionDTOSet(brand.getRegions()));
		//brandDTO.setUserDTO(userSetToUserDTOSet(brand.getUser()));
		brandDTO.setStatus(brand.getStatus());
		return brandDTO;

	}

	public Brand brandDTOToBrand(BrandDTO brandDTO) {
		if (brandDTO == null) {
			return null;
		}

		Brand brand = new Brand();

		brand.setCode(brandDTO.getCode());
		brand.setDescription(brandDTO.getDescription());
		brand.setId(brandDTO.getId());
		brand.setLogo(brandDTO.getLogo());
		if (brandDTO.getgLCodesDTO()!=null) {
			brand.setgLCodes(gLCodeDTOSetToGLCodeSet(brandDTO.getgLCodesDTO()));
		}
		//brand.setRegions(regionDTOSetToRegionSet(brandDTO.getRegionsDTO()));
		//brand.setUser(userDTOSetToUserSet(brandDTO.getUserDTO()));
		brand.setStatus(brandDTO.getStatus());
		return brand;
	}

	private Set<GLCodes> gLCodeDTOSetToGLCodeSet(Set<GLCodesDTO> getgLCodesDTO) {
		if (getgLCodesDTO == null)
			return null;

		Set<GLCodes> gLCodesSet = new HashSet<>();
		for (GLCodesDTO gLCodesDTO : getgLCodesDTO) {
			gLCodesSet.add(glCodesMapper.gLCodesDTOToGLCodes(gLCodesDTO));
		}
		return gLCodesSet;
	}

}
