package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.LocationSyncDTO;
import com.cp.persistence.entity.Location;
import com.cp.persistence.repository.LocationRepository;

@Component
public class LocationSyncMapper {

	@Autowired
	private LocationRepository locationRepository;

	public Location locationSyncDTOToLocation(LocationSyncDTO locationSyncDTO) {

		if (locationSyncDTO == null)
			return null;

		Location location = new Location();
		location.setLocationID(locationSyncDTO.getLocationID());
		location.setClientLegalName(locationSyncDTO.getLegalEntity());
		location.setAddress1(null);
		location.setAddress2(null);
		location.setCity(null);
		location.setCompanyName(null);
		location.setContractType(locationSyncDTO.getSiteOrSubsite());
		location.setId(locationSyncDTO.getId());
		location.setNoofSpaces(locationSyncDTO.getNoofSpaces());
		location.setName(locationSyncDTO.getLocationName());
		location.setParentLocationID(null);
		location.setParentLocationName(null);
		location.setStatus(locationSyncDTO.getStatus());
		location.setServiceType(locationSyncDTO.getBusinessUnitLevel());
		location.setState(locationSyncDTO.getRegion());
		location.setSubsidyType(locationSyncDTO.getFacilityType());
		location.setZipCode(null);
		location.setBrandCode(locationSyncDTO.getBrand());
		location.setBrand(null);
		location.setBurdens(null);
		location.setLocationMoreInfo(null);
		location.setPhone(null);
		location.setCell(null);
		location.setFax(null);
		location.setExt(null);
		location.setApplicableForDarma(null);
		return location;

	}


}
