package com.cp.persistence.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.PermissionDTO;
import com.cp.persistence.dto.RoleDTO;
import com.cp.persistence.entity.Permission;
import com.cp.persistence.entity.Role;

@Component
public class PermissionMapper {

	@Autowired
	private RoleMapper roleMapper;

	public PermissionDTO permissionToPermissionDTO(Permission permission) {
		if (permission == null) {
			return null;
		}
		PermissionDTO permissionDTO = new PermissionDTO();

		permissionDTO.setId(permission.getId());
		permissionDTO.setName(permission.getName());
		permissionDTO.setRoles(rolesSetToRolesDTOSet(permission.getRoles()));
		return permissionDTO;

	}

	private Set<RoleDTO> rolesSetToRolesDTOSet(Set<Role> roles) {
		if (roles == null) {
			return null;

		}
		Set<RoleDTO> roleDTOSet = new HashSet<>();
		for (Role role : roles) {
			roleDTOSet.add(roleMapper.rolesToRolesDTO(role));
		}
		return roleDTOSet;

	}

	public Permission permissionDTOToPermission(PermissionDTO permissionDTO) {
		if (permissionDTO == null) {
			return null;
		}
		Permission permission = new Permission();

		permission.setId(permissionDTO.getId());
		permission.setName(permissionDTO.getName());
		permission.setRoles(rolesDTOSetToRolesSet(permissionDTO.getRoles()));
		return permission;

	}

	private Set<Role> rolesDTOSetToRolesSet(Set<RoleDTO> roles) {
		if (roles == null) {
			return null;

		}
		Set<Role> roleSet = new HashSet<>();
		for (RoleDTO roleDTO : roles) {
			roleSet.add(roleMapper.rolesDTOToRoles(roleDTO));
		}
		return roleSet;
	}

}
