package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.RevenueTypesDTO;
import com.cp.persistence.entity.RevenueTypes;
import com.cp.persistence.repository.RevenueTypesRepository;

@Component
public class RevenueTypesMapper {
	
	@Autowired
	private RevenueTypesRepository revenueTypesRepository;

	public RevenueTypesDTO revenueTypesToRevenueTypesDTO(
			RevenueTypes revenueTypes) {
		if (revenueTypes == null) {
			return null;
		}
		RevenueTypesDTO revenueTypesDTO = new RevenueTypesDTO();

		revenueTypesDTO.setCode(revenueTypes.getCode());
		revenueTypesDTO.setDescription(revenueTypes.getDescription());
		revenueTypesDTO.setId(revenueTypes.getId());
		revenueTypesDTO.setGlNumber(revenueTypes.getGlNumber());
		revenueTypesDTO.setStatus(revenueTypes.getStatus());
		revenueTypesDTO.setOneGL(revenueTypes.getOneGL());
		revenueTypesDTO.setCompanyName(revenueTypes.getCompanyName());
		return revenueTypesDTO;
	}

	public RevenueTypes revenueTypesDTOToRevenueTypes(
			RevenueTypesDTO revenueTypesDTO) {
		if (revenueTypesDTO == null) {
			return null;
		}
		RevenueTypes revenueTypes = new RevenueTypes();

		revenueTypes.setCode(revenueTypesDTO.getCode());
		revenueTypes.setDescription(revenueTypesDTO.getDescription());
		revenueTypes.setId(revenueTypesDTO.getId());
		revenueTypes.setGlNumber(revenueTypesDTO.getGlNumber());
		revenueTypes.setStatus(revenueTypesDTO.getStatus());
		revenueTypes.setOneGL(revenueTypesDTO.getOneGL());
		revenueTypes.setCompanyName(revenueTypesDTO.getCompanyName());
		return revenueTypes;

	}

	public RevenueTypesDTO getRevenueTypeDTO(String revenuetypeid) {
		if(revenuetypeid == null)
		return null;
		else return revenueTypesToRevenueTypesDTO(revenueTypesRepository.findOne(revenuetypeid));
	}
}
