package com.cp.persistence.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.persistence.dto.BankAccountsLocationLinkDTO;
import com.cp.persistence.entity.BankAccountsLocationLink;

@Component
public class BankAccountsLocationLinkMapper {
	@Autowired
	private BankAccountsMapper bankAccountsMapper;

	@Autowired
	private LocationMapper locationMapper;

	public BankAccountsLocationLinkDTO bankAccountsLocationLinkTOBankAccountsLocationLinkDTO(
			BankAccountsLocationLink bankAccountsLocationLink) {
		if (bankAccountsLocationLink == null) {
			return null;

		}
		BankAccountsLocationLinkDTO bankAccountsLocationLinkDTO = new BankAccountsLocationLinkDTO();
		bankAccountsLocationLinkDTO.setId(bankAccountsLocationLink.getId());
		/*
		 * bankAccountsLocationLinkDTO
		 * .setLocationDTO(locationMapper.locationToLocationDTO
		 * (bankAccountsLocationLink.getLocation()));
		 */
		bankAccountsLocationLinkDTO.setBankAccountsDTO(bankAccountsMapper
				.bankAccountsToBankAccountsDTO(bankAccountsLocationLink
						.getBankaccount()));

		bankAccountsLocationLinkDTO.setStatus(bankAccountsLocationLink
				.getStatus());

		return bankAccountsLocationLinkDTO;
	}

	public BankAccountsLocationLink bankAccountsLocationLinkDTOTOBankAccountsLocationLink(
			BankAccountsLocationLinkDTO bankAccountsLocationLinkDTO) {
		if (bankAccountsLocationLinkDTO == null) {
			return null;
		}
		BankAccountsLocationLink bankAccountsLocationLink = new BankAccountsLocationLink();
		bankAccountsLocationLink.setId(bankAccountsLocationLinkDTO.getId());
		bankAccountsLocationLink.setLocation(locationMapper
				.locationDTOToLocation(bankAccountsLocationLinkDTO
						.getLocationDTO()));
		bankAccountsLocationLink.setBankaccount(bankAccountsMapper
				.bankAccountsDTOToBankAccounts(bankAccountsLocationLinkDTO
						.getBankAccountsDTO()));
		bankAccountsLocationLink.setStatus(bankAccountsLocationLinkDTO
				.getStatus());
		return bankAccountsLocationLink;
	}
}
