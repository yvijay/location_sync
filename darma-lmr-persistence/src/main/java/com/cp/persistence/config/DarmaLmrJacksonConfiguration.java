package com.cp.persistence.config;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cp.persistence.entity.util.CustomDateTimeDeserializer;
import com.cp.persistence.entity.util.CustomDateTimeSerializer;
import com.cp.persistence.entity.util.CustomLocalDateSerializer;
import com.cp.persistence.entity.util.ISO8601LocalDateDeserializer;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Configuration
public class DarmaLmrJacksonConfiguration {

	@Bean
	public JodaModule darmaLmrJacksonJodaModule() {
		JodaModule module = new JodaModule();
		module.addSerializer(LocalDateTime.class, new CustomDateTimeSerializer());
		module.addDeserializer(LocalDateTime.class, new CustomDateTimeDeserializer());
		module.addSerializer(LocalDate.class, new CustomLocalDateSerializer());
		module.addDeserializer(LocalDate.class, new ISO8601LocalDateDeserializer());
		return module;
	}
}
