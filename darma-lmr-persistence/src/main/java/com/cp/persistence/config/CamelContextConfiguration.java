package com.cp.persistence.config;

import javax.sql.DataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CamelContextConfiguration {

	@Autowired
	DataSource lmrDataSource;

	@Bean
	private CamelContext getCamelContext() {
		SimpleRegistry reg = new SimpleRegistry();
		reg.put("lmrDataSource", lmrDataSource);
		return new DefaultCamelContext(reg);

	}

}
