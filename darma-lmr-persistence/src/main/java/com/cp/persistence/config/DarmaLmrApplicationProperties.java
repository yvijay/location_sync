package com.cp.persistence.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Properties specific to Application.
 *
 * <p>
 * Properties are configured in the application.properties file.
 * </p>
 */
@Configuration
@ConfigurationProperties(prefix = "lmr", ignoreUnknownFields = false)
public class DarmaLmrApplicationProperties {
	private String apiuri;
	private final Async async = new Async();

	private final Http http = new Http();

	private final Datasource datasource = new Datasource();

	private final Cache cache = new Cache();

	private final CorsConfiguration cors = new CorsConfiguration();

	public String getApiuri() {
		return apiuri;
	}

	public void setApiuri(String apiuri) {
		this.apiuri = apiuri;
	}

	public Async getAsync() {
		return async;
	}

	public Http getHttp() {
		return http;
	}

	public Datasource getDatasource() {
		return datasource;
	}

	public Cache getCache() {
		return cache;
	}

	public CorsConfiguration getCors() {
		return cors;
	}

	public static class Async {

		private int corePoolSize = 2;

		private int maxPoolSize = 50;

		private int queueCapacity = 10000;

		public int getCorePoolSize() {
			return corePoolSize;
		}

		public void setCorePoolSize(int corePoolSize) {
			this.corePoolSize = corePoolSize;
		}

		public int getMaxPoolSize() {
			return maxPoolSize;
		}

		public void setMaxPoolSize(int maxPoolSize) {
			this.maxPoolSize = maxPoolSize;
		}

		public int getQueueCapacity() {
			return queueCapacity;
		}

		public void setQueueCapacity(int queueCapacity) {
			this.queueCapacity = queueCapacity;
		}
	}

	public static class Http {

		private final Cache cache = new Cache();

		public Cache getCache() {
			return cache;
		}

		public static class Cache {

			private int timeToLiveInDays = 31;

			public int getTimeToLiveInDays() {
				return timeToLiveInDays;
			}

			public void setTimeToLiveInDays(int timeToLiveInDays) {
				this.timeToLiveInDays = timeToLiveInDays;
			}
		}
	}

	public static class Datasource {

		private boolean cachePrepStmts = true;

		private int prepStmtCacheSize = 250;

		private int prepStmtCacheSqlLimit = 2048;

		private boolean useServerPrepStmts = true;

		public boolean isCachePrepStmts() {
			return cachePrepStmts;
		}

		public void setCachePrepStmts(boolean cachePrepStmts) {
			this.cachePrepStmts = cachePrepStmts;
		}

		public int getPrepStmtCacheSize() {
			return prepStmtCacheSize;
		}

		public void setPrepStmtCacheSize(int prepStmtCacheSize) {
			this.prepStmtCacheSize = prepStmtCacheSize;
		}

		public int getPrepStmtCacheSqlLimit() {
			return prepStmtCacheSqlLimit;
		}

		public void setPrepStmtCacheSqlLimit(int prepStmtCacheSqlLimit) {
			this.prepStmtCacheSqlLimit = prepStmtCacheSqlLimit;
		}

		public boolean isUseServerPrepStmts() {
			return useServerPrepStmts;
		}

		public void setUseServerPrepStmts(boolean useServerPrepStmts) {
			this.useServerPrepStmts = useServerPrepStmts;
		}
	}

	public static class Cache {

		private int timeToLiveSeconds = 3600;

		private final Ehcache ehcache = new Ehcache();

		public int getTimeToLiveSeconds() {
			return timeToLiveSeconds;
		}

		public void setTimeToLiveSeconds(int timeToLiveSeconds) {
			this.timeToLiveSeconds = timeToLiveSeconds;
		}

		public Ehcache getEhcache() {
			return ehcache;
		}

		public static class Ehcache {

			private String maxBytesLocalHeap = "16M";

			public String getMaxBytesLocalHeap() {
				return maxBytesLocalHeap;
			}

			public void setMaxBytesLocalHeap(String maxBytesLocalHeap) {
				this.maxBytesLocalHeap = maxBytesLocalHeap;
			}
		}
	}

}
