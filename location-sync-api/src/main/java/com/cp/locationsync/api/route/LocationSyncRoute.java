package com.cp.locationsync.api.route;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.locationsync.api.route.transform.LocationSyncToCsvRowTransformer;
import com.cp.locationsync.api.route.util.LocationSyncCsvRecord;

/**
 * @author akhileshv
 *
 */
@Component
public class LocationSyncRoute extends RouteBuilder {

	BindyCsvDataFormat bindyCsvDataFormat = new BindyCsvDataFormat(
			LocationSyncCsvRecord.class);

	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	LocationSyncToCsvRowTransformer locationSyncToCsvRowTransformer;
	
	

	@Override
	public void configure() throws Exception {
		loadProperties();
		
		onException(Exception.class)
		.handled(true)
		.maximumRedeliveries(0)
		.process(locationSyncToCsvRowTransformer)
   .end();
		
		from("direct:locationSyncRoute")
		.onCompletion().onWhen(simple("${header.CamelBatchComplete} == true")) 
			.log("COMPLETED BATCH PROCESSING COMPLETED...........!")
		.end() 
		.id("locationSyncRoute")
			.log("locationSync body: ${body}")
		    	.process(locationSyncToCsvRowTransformer)
		    	.marshal(bindyCsvDataFormat)
		    	.multicast()
		    	.to("{{locationsync.sftp.server1}}","{{locationsync.sftp.server2}}","{{locationsync.sftp.server3}}");

	}
	/**
	 * This method loads application.properties and dynamic-camel.properties 
	 * since dynamic camel expression has conflicts with spring application.properties 
	 */
	private void loadProperties(){
		PropertiesComponent pc = new PropertiesComponent();
		pc.setLocations(new String[]{"classpath:dynamic-camel.properties","classpath:application.properties"});
		camelContext.addComponent("properties", pc);
	}
}