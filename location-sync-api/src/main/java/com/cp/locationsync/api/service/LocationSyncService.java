package com.cp.locationsync.api.service;

import java.util.List;

import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.LocationSyncDTO;
import com.cp.persistence.entity.Location;

public interface LocationSyncService {

	/**
	 * Save a  Location.
	 * 
	 * @return the persisted entity
	 */
	public LocationDTO save(LocationDTO locationDTO);

	public void startExport(List<LocationSyncDTO> locSyncLocationDTOs);

	public List<Location> saveLocations(List<LocationSyncDTO> locSyncLocationDTOs);



}
