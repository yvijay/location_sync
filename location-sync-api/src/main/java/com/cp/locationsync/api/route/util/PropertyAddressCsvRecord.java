package com.cp.locationsync.api.route.util;

import java.io.Serializable;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",")
public class PropertyAddressCsvRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	@DataField(pos = 12)
	private String address1;
	@DataField(pos = 13)
	private String address2;
	@DataField(pos = 14)
	private String state;
	@DataField(pos = 15)
	private String city;
	@DataField(pos = 16)
	private String zip;
	@DataField(pos = 17)
	private String country;

	
	
	
	
	public PropertyAddressCsvRecord(String id, String address1,
			String address2, String state, String city, String zip,
			String country) {
		this.id = id;
		this.address1 = address1;
		this.address2 = address2;
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.country = country;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	@Override
	public String toString() {
		return "PropertyAddressDTO [id=" + id + ", address1=" + address1
				+ ", address2=" + address2 + ", state=" + state + ", city="
				+ city + ", zip=" + zip + ", country=" + country + "]";
	}
}
