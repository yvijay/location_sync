package com.cp.locationsync.api;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

/**
 * TODO: Need to make more flexible with profiling and place holder..
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = { "com.cp.locationsync.api",
		"com.cp.persistence", "com.cp.locationsync.security",
		"com.cp.locationsync.commons" })
@EnableAutoConfiguration
@EntityScan("com.cp.persistence.entity")
public class ApiApplication extends SpringBootServletInitializer {

	private static final Logger log = LoggerFactory
			.getLogger(ApiApplication.class);

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(ApiApplication.class);
	}

	/**
	 * Main method, used to run the application.
	 */
	public static void main(String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(ApiApplication.class);

		ConfigurableApplicationContext ctx = app.run(args);

		Environment env = ctx.getEnvironment();

		log.info(
				"Access URLs:\n----------------------------------------------------------\n\t"
						+ "Local: \t\thttp://127.0.0.1:{}\n\t"
						+ "External: \thttp://{}:{}\n----------------------------------------------------------",
				env.getProperty("server.port"), InetAddress.getLocalHost()
						.getHostAddress(), env.getProperty("server.port"));

	}

}
