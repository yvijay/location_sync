package com.cp.locationsync.api.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cp.locationsync.api.service.LocationSyncService;
import com.cp.persistence.dto.LocationSyncDTO;
import com.cp.persistence.entity.Location;
import com.cp.persistence.mapper.LocationMapper;

/**
 * REST controller for managing Locations.
 */
@RestController
@RequestMapping("system3/api")
public class LocationSyncResource {

	private final Logger log = LoggerFactory.getLogger(LocationSyncResource.class);

	@Autowired
	private LocationSyncService locationService;
	
	@Autowired
	private LocationMapper locationMapper;

	/**
	 * POST /locations -> Create a new Locations.
	 */
	@RequestMapping(value = "/locationSync", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LocationSyncDTO>> create(
			@Valid @RequestBody List<LocationSyncDTO> locSyncLocationDTOs)
			throws URISyntaxException {
		log.debug("REST REQUEST TO SAVE LOCATIONS USING LOCATIONSYNC DTO's  : {}", locSyncLocationDTOs);
		List<Location> locations=locationService.saveLocations(locSyncLocationDTOs);
		if(!locations.isEmpty()){
			log.debug("LOCATIONS WERE SAVED SUCCESFULLY...STARTED EXPORTING TO SFTP SERVER....!");
			locationService.startExport(locSyncLocationDTOs);
		}
		return new ResponseEntity<List<LocationSyncDTO>>(HttpStatus.OK);

	}
}
