package com.cp.locationsync.api.route.util;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.OneToMany;

import com.cp.persistence.dto.enums.StatusEnum;

@CsvRecord(separator = ",")
public class LocationSyncCsvRecord {

	@DataField(pos = 1)
	private String brand;

	@DataField(pos = 2)
	private String siteOrSubsite;

	@DataField(pos = 3)
	private String locationID;

	@DataField(pos = 4)
	private String locationName;

	@DataField(pos = 5)
	private String legalEntity;

	@OneToMany
	private List<MailingAddressCsvRecord> mailingAddress;

	@OneToMany
	private List<PropertyAddressCsvRecord> propertyAddress;

	@DataField(pos = 18)
	private String operationType;

	@DataField(pos = 19)
	private String facilityType;

	

	@DataField(pos = 20)
	private StatusEnum status;

	

	@DataField(pos = 21)
	private Integer noofSpaces;

	@DataField(pos = 22)
	private String timeZone;

	@DataField(pos = 23)
	private String region;

	@DataField(pos = 24)
	private String businessUnitLevel;

	@DataField(pos = 25)
	private String territoryLevel;

	@DataField(pos = 26)
	private String locationManager;

	@DataField(pos = 27)
	private String secondaryApprovingManager;

	@DataField(pos = 28)
	private String svp;

	@DataField(pos = 29)
	private String rvp;

	@DataField(pos = 30,pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private LocalDateTime startDate;
	
	@DataField(pos = 31,pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private LocalDateTime endDate;
	
	
	
	
	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public List<MailingAddressCsvRecord> getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(List<MailingAddressCsvRecord> mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public List<PropertyAddressCsvRecord> getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(List<PropertyAddressCsvRecord> propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getSiteOrSubsite() {
		return siteOrSubsite;
	}

	public void setSiteOrSubsite(String siteOrSubsite) {
		this.siteOrSubsite = siteOrSubsite;
	}

	public String getLocationID() {
		return locationID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	/*public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}*/

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	/*public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}*/

	public Integer getNoofSpaces() {
		return noofSpaces;
	}

	public void setNoofSpaces(Integer noofSpaces) {
		this.noofSpaces = noofSpaces;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getBusinessUnitLevel() {
		return businessUnitLevel;
	}

	public void setBusinessUnitLevel(String businessUnitLevel) {
		this.businessUnitLevel = businessUnitLevel;
	}

	public String getTerritoryLevel() {
		return territoryLevel;
	}

	public void setTerritoryLevel(String territoryLevel) {
		this.territoryLevel = territoryLevel;
	}

	public String getLocationManager() {
		return locationManager;
	}

	public void setLocationManager(String locationManager) {
		this.locationManager = locationManager;
	}

	public String getSecondaryApprovingManager() {
		return secondaryApprovingManager;
	}

	public void setSecondaryApprovingManager(String secondaryApprovingManager) {
		this.secondaryApprovingManager = secondaryApprovingManager;
	}

	public String getSvp() {
		return svp;
	}

	public void setSvp(String svp) {
		this.svp = svp;
	}

	public String getRvp() {
		return rvp;
	}

	public void setRvp(String rvp) {
		this.rvp = rvp;
	}

	
	

	
	
	



}
