package com.cp.locationsync.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cp.locationsync.api.service.LocationSyncService;
import com.cp.persistence.dto.LocationDTO;
import com.cp.persistence.dto.LocationSyncDTO;
import com.cp.persistence.entity.Location;
import com.cp.persistence.mapper.LocationMapper;
import com.cp.persistence.mapper.LocationSyncMapper;
import com.cp.persistence.repository.LocationRepository;

/**
 * @author akhileshv
 *
 */
@Service
public class LocationSyncServiceImpl implements LocationSyncService {

	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private LocationMapper locationMapper;

	@Autowired
	private LocationSyncMapper locationSyncMapper;

	/**
	 * Save a Location.
	 * 
	 * @return the persisted entity
	 */
	@Autowired
	ProducerTemplate producerTemplate;

	@Override
	public LocationDTO save(LocationDTO locationDTO) {

		Location location = locationMapper.locationDTOToLocation(locationDTO);
		location = locationRepository.save(location);
		return locationMapper.locationToLocationDTO(location);
	}

	@Override
	public void startExport(List<LocationSyncDTO> locSyncLocationDTOs) {

		producerTemplate.sendBody("direct:locationSyncRoute",
				locSyncLocationDTOs);

	}

	@Override
	public List<Location> saveLocations(
			List<LocationSyncDTO> locSyncLocationDTOs) {
		List<Location> locations= new ArrayList<Location>();
		for (LocationSyncDTO locationSyncDTO2 : locSyncLocationDTOs) {
			locations.add(locationSyncMapper
					.locationSyncDTOToLocation(locationSyncDTO2));
		}
		if(!locations.isEmpty())
			locationRepository.save(locations);
		return locations;
	}
}
