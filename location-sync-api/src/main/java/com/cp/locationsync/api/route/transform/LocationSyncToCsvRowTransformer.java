package com.cp.locationsync.api.route.transform;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cp.locationsync.api.route.util.LocationSyncCsvRecord;
import com.cp.locationsync.api.route.util.MailingAddressCsvRecord;
import com.cp.locationsync.api.route.util.PropertyAddressCsvRecord;
import com.cp.persistence.dto.LocationSyncDTO;
import com.cp.persistence.dto.MailingAddressDTO;
import com.cp.persistence.dto.PropertyAddressDTO;
import com.cp.persistence.mapper.LocationSyncMapper;

@Component
public class LocationSyncToCsvRowTransformer implements Processor {

	@Autowired
	private LocationSyncMapper locationSyncMapper;

	@Override
	public void process(Exchange exchange) throws Exception {
		@SuppressWarnings("unchecked")
		List<Object> body = (List<Object>) exchange.getIn().getBody(
				ArrayList.class);
		List<LocationSyncCsvRecord> locationSyncCsvRecords = populateLocationSyncCsvRecords(body);
		exchange.getIn().setBody(locationSyncCsvRecords);
	}

	private List<LocationSyncCsvRecord> populateLocationSyncCsvRecords(
			List<Object> body) {
		List<LocationSyncCsvRecord> locationSyncCsvRecords = new ArrayList<LocationSyncCsvRecord>();

		for (Object body1 : body) {
			LocationSyncCsvRecord locationSyncCsvRecord = new LocationSyncCsvRecord();
			LocationSyncDTO locationSyncDTO = (LocationSyncDTO) body1;
			locationSyncCsvRecord.setBrand(locationSyncDTO.getBrand());
			locationSyncCsvRecord.setBusinessUnitLevel(locationSyncDTO
					.getBusinessUnitLevel());
			locationSyncCsvRecord.setEndDate(getJavaTime(locationSyncDTO
					.getEndDate()));
			locationSyncCsvRecord.setFacilityType(locationSyncDTO
					.getFacilityType());
			locationSyncCsvRecord.setLegalEntity(locationSyncDTO
					.getLegalEntity());
			locationSyncCsvRecord
					.setLocationID(locationSyncDTO.getLocationID());
			locationSyncCsvRecord.setLocationManager(locationSyncDTO
					.getLocationManager());
			locationSyncCsvRecord.setLocationName(locationSyncDTO
					.getLocationName());
			locationSyncCsvRecord
					.setMailingAddress(convertMailingAddressDtoToCsv(locationSyncDTO
							.getMailingAddress()));
			locationSyncCsvRecord
					.setNoofSpaces(locationSyncDTO.getNoofSpaces());
			locationSyncCsvRecord.setOperationType(locationSyncDTO
					.getOperationType());
			locationSyncCsvRecord
					.setPropertyAddress(convertPropertyAddressDtoToCsv(locationSyncDTO
							.getPropertyAddress()));
			locationSyncCsvRecord.setRegion(locationSyncDTO.getRegion());
			locationSyncCsvRecord.setRvp(locationSyncDTO.getRvp());
			locationSyncCsvRecord.setSecondaryApprovingManager(locationSyncDTO
					.getSecondaryApprovingManager());
			locationSyncCsvRecord.setSiteOrSubsite(locationSyncDTO
					.getSiteOrSubsite());
			locationSyncCsvRecord.setStartDate(getJavaTime(locationSyncDTO.getStartDate()));
			locationSyncCsvRecord.setStatus(locationSyncDTO.getStatus());
			locationSyncCsvRecord.setSvp(locationSyncDTO.getSvp());
			locationSyncCsvRecord.setTerritoryLevel(locationSyncDTO
					.getTerritoryLevel());
			locationSyncCsvRecord.setTimeZone(locationSyncDTO.getTimeZone());
			locationSyncCsvRecords.add(locationSyncCsvRecord);
		}
		return locationSyncCsvRecords;

	}

	/**
	 * To Convert JodaTime to JavaTime
	 * @param endDate
	 * @return
	 */
	private LocalDateTime getJavaTime(org.joda.time.LocalDateTime endDate) {
		return java.time.LocalDateTime.ofInstant(
				java.time.Instant.ofEpochMilli(endDate
						.toDateTime(DateTimeZone.UTC).toInstant().getMillis()),
				ZoneId.of("UTC"));
	}

	/**
	 * @param mailingAddress
	 * @return List<MailingAddressCsvRecord>
	 * 
	 *         To Generate List of MailingAddressCsvRecords from List of
	 *         MailingAddressDTO
	 */
	private List<MailingAddressCsvRecord> convertMailingAddressDtoToCsv(
			List<MailingAddressDTO> mailingAddress) {
		if (mailingAddress == null)
			return null;

		return mailingAddress
				.stream()
				.map(mailingAddres -> new MailingAddressCsvRecord(mailingAddres
						.getId(), mailingAddres.getAddress1(), mailingAddres
						.getAddress2(), mailingAddres.getCountry(),
						mailingAddres.getCity(), mailingAddres.getState(),
						mailingAddres.getZip())).collect(Collectors.toList());

	}

	/**
	 * @param propertyAddress
	 * @return List<PropertyAddressCsvRecord>
	 * 
	 *         To Generate List of PropertyAddressCsvRecords from List of
	 *         PropertyAddressDTO
	 */
	private List<PropertyAddressCsvRecord> convertPropertyAddressDtoToCsv(
			List<PropertyAddressDTO> propertyAddress) {
		if (propertyAddress == null)
			return null;

		return propertyAddress
				.stream()
				.map(propertyAddres -> new PropertyAddressCsvRecord(
						propertyAddres.getId(), propertyAddres.getAddress1(),
						propertyAddres.getAddress2(), propertyAddres
								.getCountry(), propertyAddres.getCity(),
						propertyAddres.getState(), propertyAddres.getZip()))
				.collect(Collectors.toList());

	}
}
